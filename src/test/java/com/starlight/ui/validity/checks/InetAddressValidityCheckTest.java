package com.starlight.ui.validity.checks;

import com.starlight.thread.ObjectSlot;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


/**
 *
 */
@RunWith( Parameterized.class )
public class InetAddressValidityCheckTest extends TestCase {
	@Parameterized.Parameters( name = "{0}" )
	public static Collection<TestArgs> args() {
		return Arrays.asList(
			new TestArgs( "vfbkvb", Type.ILLEGAL ),
			new TestArgs( "1", Type.ILLEGAL ),
			new TestArgs( "192.168.1.1", Type.IP4 ),
			new TestArgs( "192.168.1.1.", Type.ILLEGAL ), // trailing dot (PR 1)
			new TestArgs( "1080:0:0:0:8:800:200C:417A", Type.IP6 ),

			// IP6 shortening examples from http://www.gestioip.net/docu/ipv6_address_examples.html
//			new TestArgs( "2001:0db8::0001", Type.ILLEGAL ),        // leading zeros
			new TestArgs( "2001:db8::1", Type.IP6 ),

//			new TestArgs( "2001:db8:0:0:0:0:2:1", Type.ILLEGAL ),   // un-shortened
			new TestArgs( "2001:db8::2:1", Type.IP6 ),

//			new TestArgs( "2001:db8::1:1:1:1:1", Type.ILLEGAL ),    // shorten single 0
			new TestArgs( "2001:db8:0:1:1:1:1:1", Type.IP6 )
		);
	}


	private final ObjectSlot<String> text_slot = new ObjectSlot<>();
	private final Type text_type;


	private final InetAddressValidityCheck ip4_check =
		new InetAddressValidityCheck( text_slot, InetAddressValidityCheck.Mode.IP4_ONLY );

	private final InetAddressValidityCheck ip6_check =
		new InetAddressValidityCheck( text_slot, InetAddressValidityCheck.Mode.IP6_ONLY );

	private final InetAddressValidityCheck either_check =
		new InetAddressValidityCheck( text_slot, InetAddressValidityCheck.Mode.EITHER );

	private final InetAddressValidityCheck default_check =
		new InetAddressValidityCheck( text_slot );





	public InetAddressValidityCheckTest( TestArgs args ) {
		this.text_type = args.type;
		text_slot.set( args.text );
	}


	@Test
	public void testDefaultMode() {     // either
		assertEquals( text_type != Type.ILLEGAL, default_check.check() );
	}

	@Test
	public void testIP4and6() {
		assertEquals( text_type != Type.ILLEGAL, either_check.check() );
	}

	@Test
	public void testIP4Only() {
		assertEquals( text_type == Type.IP4, ip4_check.check() );
	}

	@Test
	public void testIP6Only() {
		assertEquals( text_type == Type.IP6, ip6_check.check() );
	}





	private enum Type {
		ILLEGAL,
		IP4,
		IP6
	}


	private static class TestArgs {
		final String text;
		final Type type;

		TestArgs( String text, Type type ) {
			this.text = text;
			this.type = type;
		}



		@Override public String toString() {
			return "text=" + text +
				", type=" + type;
		}
	}
}
