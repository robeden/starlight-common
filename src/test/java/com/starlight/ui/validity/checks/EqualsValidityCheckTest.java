package com.starlight.ui.validity.checks;

import com.starlight.thread.ObjectSlot;
import junit.framework.TestCase;


/**
 *
 */
public class EqualsValidityCheckTest extends TestCase {
	public void testCheckValidIfEqual() {
		ObjectSlot<String> value = new ObjectSlot<>();
		ObjectSlot<String> match = new ObjectSlot<>();

		EqualsValidityCheck<String> check = new EqualsValidityCheck<>( value, match, true );

		assertTrue( check.check() );    // value null is always equal

		value.set( "Something" );
		assertFalse( check.check() );

		value.set( null );              // value null is always equal
		match.set( "Something" );
		assertTrue( check.check() );

		value.set( "Something" );
		assertTrue( check.check() );
	}

	public void testCheckValidIfNotEqual() {
		ObjectSlot<String> value = new ObjectSlot<>();
		ObjectSlot<String> match = new ObjectSlot<>();

		EqualsValidityCheck<String> check = new EqualsValidityCheck<>( value, match, false );

		assertTrue( check.check() );    // value null is always equal

		value.set( "Something" );
		assertTrue( check.check() );

		value.set( null );              // value null is always equal
		match.set( "Something" );
		assertTrue( check.check() );

		value.set( "Something" );
		assertFalse( check.check() );
	}
}
