package com.starlight.ui.validity.checks;

import junit.framework.TestCase;

import javax.swing.*;

/**
 *
 */
public class URLValidityCheckTest extends TestCase {
    public void testCheck() {
        JTextField field = new JTextField();

        URLValidityCheck check = new URLValidityCheck( field, true );

        // No text
        assertTrue( check.check() );

        // Whitespace
        field.setText( " " );
        assertTrue( check.check() );

        // Junk, invalid
        field.setText( "blah" );
        assertFalse( check.check() );

        // Host without protocol
        field.setText( "www.starlight-systems.com" );
        assertFalse( check.check() );

        // HTTP
        field.setText( "http://www.starlight-systems.com" );
        assertTrue( check.check() );

        // HTTPS
        field.setText( "https://www.starlight-systems.com" );
        assertTrue( check.check() );
    }
}
