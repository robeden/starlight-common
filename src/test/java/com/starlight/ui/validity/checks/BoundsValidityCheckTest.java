package com.starlight.ui.validity.checks;

import junit.framework.TestCase;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;


/**
 *
 */
public class BoundsValidityCheckTest extends TestCase {
	public void testCheck() throws Exception {
		final AtomicInteger source = new AtomicInteger(  10 );
		Supplier<Integer> supplier = () -> {
			int value = source.get();
			if ( value == Integer.MIN_VALUE ) return null;
			return Integer.valueOf( value );
		};

		BoundsValidityCheck<Integer> int_check = new BoundsValidityCheck<>( supplier,
			Integer.valueOf( 0 ), Integer.valueOf( 100 ) );

		assertTrue( int_check.check() );

		source.set( -10 );
		assertFalse( int_check.check() );
		source.set( -1 );
		assertFalse( int_check.check() );

		source.set( 0 );
		assertTrue( int_check.check() );
		source.set( 1 );
		assertTrue( int_check.check() );
		source.set( 50 );
		assertTrue( int_check.check() );
		source.set( 99 );
		assertTrue( int_check.check() );
		source.set( 100 );
		assertTrue( int_check.check() );

		source.set( 101 );
		assertFalse( int_check.check() );

		source.set( Integer.MIN_VALUE );       // returns null
		assertTrue( int_check.check() );
	}
}
