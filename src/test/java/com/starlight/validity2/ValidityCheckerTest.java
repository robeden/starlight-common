package com.starlight.validity2;

import com.starlight.locale.UnlocalizableTextResourceKey;
import junit.framework.TestCase;

import java.util.function.Consumer;

import static org.easymock.EasyMock.*;


/**
 *
 */
public class ValidityCheckerTest extends TestCase {
	public void testSingleCheckPass() {
		ValidityCheck mock_check = createMock( ValidityCheck.class );
		expect( mock_check.check( anyObject( Consumer.class ) ) )
			.andReturn( Boolean.TRUE );

		CheckHandler mock_handler = createMock( CheckHandler.class );
		mock_handler.checkPass();

		replay( mock_check, mock_handler );


		ValidityChecker checker = new ValidityChecker();
		checker.addCheck( mock_check );
		checker.addHandler( mock_handler );

		assertTrue( checker.check() );

		verify( mock_check, mock_handler );
	}

	public void testSingleCheckFailNoMessage() {
		ValidityCheck mock_check = createMock( ValidityCheck.class );
		expect( mock_check.check( anyObject( Consumer.class ) ) )
			.andReturn( Boolean.FALSE );

		CheckHandler mock_handler = createMock( CheckHandler.class );
		mock_handler.checkFail( new FailureInfo( mock_check, null ) );

		replay( mock_check, mock_handler );


		ValidityChecker checker = new ValidityChecker();
		checker.addCheck( mock_check );
		checker.addHandler( mock_handler );

		assertFalse( checker.check() );

		verify( mock_check, mock_handler );
	}

	public void testSingleCheckFailWithMessage() {
		ValidityCheck check = fail_message_consumer -> {

			fail_message_consumer.accept(
				new UnlocalizableTextResourceKey( "This is a test failure" ) );

			return false;
		};

		CheckHandler mock_handler = createMock( CheckHandler.class );
		mock_handler.checkFail( new FailureInfo( check,
			new UnlocalizableTextResourceKey( "This is a test failure" ) ) );

		replay( mock_handler );


		ValidityChecker checker = new ValidityChecker();
		checker.addCheck( check );
		checker.addHandler( mock_handler );

		assertFalse( checker.check() );

		verify( mock_handler );
	}

	// TODO: figure out what's wrong with this test
//	public void testMultipleCheckFail() {
//		ValidityCheck check_fail_with_message = new ValidityCheck() {
//			@Override
//			public boolean check(
//				@NotNull Consumer<ResourceKey<String>> fail_message_consumer ) {
//
//				fail_message_consumer.consume(
//					new UnlocalizableTextResourceKey( "This is a test failure" ) );
//
//				return false;
//			}
//
//			@Override
//			public String toString() {
//				return "fail_with_message";
//			}
//		};
//
//		ValidityCheck mock_check_fail_no_message =
//			createMock( "fail_no_message", ValidityCheck.class );
//		expect( mock_check_fail_no_message.check( anyObject( Consumer.class ) ) )
//			.andReturn( Boolean.FALSE );
//
//		ValidityCheck mock_check_pass = createMock( "pass", ValidityCheck.class );
//		expect( mock_check_pass.check( anyObject( Consumer.class ) ) )
//			.andReturn( Boolean.TRUE );
//
//
//		CheckHandler mock_handler = createMock( CheckHandler.class );
//		FailureInfo failure = new FailureInfo( check_fail_with_message,
//			new UnlocalizableTextResourceKey( "This is a test failure" ) );
//		failure.add( mock_check_fail_no_message, null );
//		mock_handler.checkFail( failure );
//
//		replay( mock_handler, mock_check_fail_no_message, mock_check_pass );
//
//
//		ValidityChecker checker = new ValidityChecker();
//		checker.addCheck( check_fail_with_message );
//		checker.addCheck( mock_check_fail_no_message );
//		checker.addCheck( mock_check_pass );
//		checker.addHandler( mock_handler );
//
//		assertFalse( checker.check() );
//
//		verify( mock_handler, mock_check_fail_no_message, mock_check_pass );
//	}
}
