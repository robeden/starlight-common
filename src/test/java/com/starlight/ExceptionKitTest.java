package com.starlight;

import junit.framework.TestCase;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.ObjectOutput;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


/**
 *
 */
public class ExceptionKitTest extends TestCase {
	public void testCanThrow() throws Exception {
		Class clazz = ObjectOutput.class;
		Method method = clazz.getMethod( "writeObject", Object.class );

		assertTrue( ExceptionKit.canThrow( method, IOException.class ) );
		assertTrue( ExceptionKit.canThrow( method, InterruptedIOException.class ) );
		assertTrue( ExceptionKit.canThrow( method, NullPointerException.class ) );
		assertTrue( ExceptionKit.canThrow( method, NoClassDefFoundError.class ) );
		assertFalse( ExceptionKit.canThrow( method, InvocationTargetException.class ) );
	}


	public void testContainsFrame() {
		try {
			Test.foo();
		}
		catch ( RuntimeException e ) {
			e.printStackTrace( System.out );
			containsCorrect( e, "foo", 0, -1, true );
			containsCorrect( e, "foo", 0, 0, true );
			containsCorrect( e, "Test.foo", 0, 0, true );
			containsCorrect( e, "Test.foo", 0, -1, true );
			containsCorrect( e, "bar", 0, -1, false );
			containsCorrect( e, "ExceptionKitTest.testContainsFrame", 0, -1, true );
			containsCorrect( e, "ExceptionKitTest.testContainsFrame", 1, 1, true );
			containsCorrect( e, "ExceptionKitTest.testContainsFrame", 0, 0, false );
		}
		try {
			Test.foo4();
		}
		catch ( RuntimeException e ) {
			System.out.println(); // blank line
			e.printStackTrace( System.out );
			containsCorrect( e, "foo", 0, 0, true );
			containsCorrect( e, "foo", 0, -1, true );
			containsCorrect( e, "foo2", 0, -1, true );
			containsCorrect( e, "foo2", 0, 2, true );
			containsCorrect( e, "foo2", 2, 2, true );
			containsCorrect( e, "foo2", 3, -1, false );
			containsCorrect( e, "foo2", 3, 8, false );
		}
	}


	@SuppressWarnings( { "ThrowableInstanceNeverThrown" } )
	public void testEquals() throws Exception {
		Exception e1 = new Exception();
		Exception e2 = new IllegalArgumentException();
		Exception e3 = new Exception();

		assertTrue( ExceptionKit.equals( null, null ) );
		assertFalse( ExceptionKit.equals( e1, null ) );
		assertFalse( ExceptionKit.equals( null, e1 ) );
		assertTrue( ExceptionKit.equals( e1, e1 ) );

		assertFalse( ExceptionKit.equals( e1, e2 ) );
		assertFalse( ExceptionKit.equals( e1, e3 ) );
		assertFalse( ExceptionKit.equals( e2, e3 ) );
	}


	private void containsCorrect( Throwable t, String frameString, int minDistFromRoot,
		int maxDistFromRoot, boolean shouldContain ) {

		boolean contains = ExceptionKit.containsFrame( t, frameString, minDistFromRoot,
			maxDistFromRoot );
		assertEquals( shouldContain, contains );
	}

	private static class Test {
		public static void foo() {
			throw new RuntimeException( "Testing" );
		}

		public static void foo1() {
			foo();
		}

		public static void foo2() {
			foo1();
		}

		public static void foo3() {
			foo2();
		}

		public static void foo4() {
			foo3();
		}
	}
}
