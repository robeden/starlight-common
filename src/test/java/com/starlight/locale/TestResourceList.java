/*
 * Copyright(c) 2007, StarLight Systems
 * All rights reserved.
 */
package com.starlight.locale;

/**
 *
 */
public class TestResourceList extends ResourceList {
	public static final TextResourceKey ONE = new TextResourceKey( "One" );
	static final TextResourceKey TWO = new TextResourceKey( "Two" );
}
