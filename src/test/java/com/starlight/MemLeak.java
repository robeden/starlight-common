package com.starlight;

import java.util.LinkedList;
import java.util.List;


/**
 *
 */
public class MemLeak {
	public static void main( String[] args ) {
//		List<String> list = new LinkedList<String>();
//
//		int counter = 0;
//		while( true ) {
//			list.add( Integer.toString( counter ) );
//		}

//		byte[] toobig = new byte[ Integer.MAX_VALUE - 1 ];

//		method1();

		new MemLeak();
	}


	public MemLeak() {
		method2();
	}


	private static void method1() {
		method2();
	}

	private static void method2() {
		List<String> list = new LinkedList<String>();

		int counter = 0;
		while( true ) {
			list.add( Integer.toString( counter ) );
		}
	}
}
