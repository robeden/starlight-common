package com.starlight;

import junit.framework.TestCase;


/**
 *
 */
public class ValidationKitTest extends TestCase {
	public void testCheckNonnull() throws Exception {
		ValidationKit.checkNonnull( new Object(), "object" );

		try {
			//noinspection ConstantConditions
			ValidationKit.checkNonnull( null, "whojy-whatzit" );
		}
		catch( IllegalArgumentException ex ) {
			assertEquals( "whojy-whatzit cannot be null", ex.getMessage() );
			// this is good
		}

	}

	public void testCheckGreaterThanInt() throws Exception {
		ValidationKit.checkGreaterThan( 5, 2, "something" );

		try {
			ValidationKit.checkGreaterThan( 5, 5, "something" );
		}
		catch( IllegalArgumentException ex ) {
			assertEquals( "something must be greater than 5", ex.getMessage() );
		}

		try {
			ValidationKit.checkGreaterThan( 3, 5, "something" );
		}
		catch( IllegalArgumentException ex ) {
			assertEquals( "something must be greater than 5", ex.getMessage() );
		}
	}

	public void testCheckGreaterThanLong() throws Exception {
		ValidationKit.checkGreaterThan( 5L, 2L, "something" );

		try {
			ValidationKit.checkGreaterThan( 5L, 5L, "something" );
		}
		catch( IllegalArgumentException ex ) {
			assertEquals( "something must be greater than 5", ex.getMessage() );
		}

		try {
			ValidationKit.checkGreaterThan( 3L, 5L, "something" );
		}
		catch( IllegalArgumentException ex ) {
			assertEquals( "something must be greater than 5", ex.getMessage() );
		}

	}

	public void testCheckLessThanInt() throws Exception {
		ValidationKit.checkLessThan( 3, 5, "something" );

		try {
			ValidationKit.checkLessThan( 5, 5, "something" );
		}
		catch( IllegalArgumentException ex ) {
			assertEquals( "something must be less than 5", ex.getMessage() );
		}

		try {
			ValidationKit.checkLessThan( 7, 5, "something" );
		}
		catch( IllegalArgumentException ex ) {
			assertEquals( "something must be less than 5", ex.getMessage() );
		}
	}

	public void testCheckLessThanLong() throws Exception {
		ValidationKit.checkLessThan( 3L, 5L, "something" );

		try {
			ValidationKit.checkLessThan( 5L, 5L, "something" );
		}
		catch( IllegalArgumentException ex ) {
			assertEquals( "something must be less than 5", ex.getMessage() );
		}

		try {
			ValidationKit.checkLessThan( 7L, 5L, "something" );
		}
		catch( IllegalArgumentException ex ) {
			assertEquals( "something must be less than 5", ex.getMessage() );
		}
	}
}
