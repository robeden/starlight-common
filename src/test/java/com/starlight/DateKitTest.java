/*
 * Copyright (c) 2009 Rob Eden.
 * All Rights Reserved.
 */
package com.starlight;

import junit.framework.TestCase;

import java.text.DateFormat;
import java.util.Date;


/**
 *
 */
public class DateKitTest extends TestCase {
	public void testStripTime() {
		Date orig = new Date();

		DateFormat format = DateFormat.getDateInstance( DateFormat.SHORT );

//		System.out.println( "Orig: " + orig );
		String before = format.format( orig );
//		System.out.println( "  Formatted: " + before );

		Date modified = DateKit.stripTime( orig );
//		System.out.println( "Modified: " + modified );
		String after = format.format( modified );
//		System.out.println( "  Formatted: " + after );

		assertEquals( before, after );
		assertFalse( orig.equals( modified ) );
	}
}
