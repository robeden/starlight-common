package com.starlight;

import junit.framework.TestCase;

import java.util.Arrays;


/**
 *
 */
public class StringKitTest extends TestCase {
	public void testSplit() throws Exception {
		assertEquals( "a.b.c".split( "\\." ), StringKit.split( "a.b.c", '.' ) );
		assertEquals( "a.b.c.".split( "\\." ), StringKit.split( "a.b.c.", '.' ) );
		assertEquals( ".a.b.c".split( "\\." ), StringKit.split( ".a.b.c", '.' ) );
		assertEquals( "a..b.c".split( "\\." ), StringKit.split( "a..b.c", '.' ) );
		assertEquals( "abc".split( "\\." ), StringKit.split( "abc", '.' ) );
		assertEquals( ".a.b.c.".split( "\\." ), StringKit.split( ".a.b.c.", '.' ) );
	}


	public void testTrimToNull() {
		assertEquals( "foo", StringKit.trimToNull( "foo" ) );
		assertEquals( "foo", StringKit.trimToNull( "foo " ) );
		assertEquals( "foo", StringKit.trimToNull( "\tfoo " ) );
		assertNull( StringKit.trimToNull( "" ) );
		assertNull( StringKit.trimToNull( "   " ) );
		assertNull( StringKit.trimToNull( " \t  " ) );
		assertNull( StringKit.trimToNull( null ) );
	}


	public void testEqualIgnoreCase() {
		assertTrue( StringKit.equalIgnoreCase( null, null ) );
		assertFalse( StringKit.equalIgnoreCase( "hi", null ) );
		assertFalse( StringKit.equalIgnoreCase( null, "hi" ) );
		assertFalse( StringKit.equalIgnoreCase( "hello", "hi" ) );
		assertFalse( StringKit.equalIgnoreCase( "hi", "hello" ) );
		assertTrue( StringKit.equalIgnoreCase( "HELLO", "hello" ) );
		assertTrue( StringKit.equalIgnoreCase( "hello", "HELLO" ) );
	}



	public void testFormat() {
		assertEquals( "This is a test",
			StringKit.format( "This is a test" ) );
		assertEquals( "This is a test",
			StringKit.format( "This {} a test", "is" ) );
		assertEquals( "This is a test",
			StringKit.format( "This {} a {}", "is", "test" ) );
		assertEquals( "This is a test",
			StringKit.format( "This {} {} {}", "is", "a", "test" ) );
		assertEquals( "This is a test",
			StringKit.format( "{} {} {} {}", "This", "is", "a", "test" ) );

		assertEquals( "All for one and 1 for all",
			StringKit.format( "All for {} and {} {} all", "one", Integer.valueOf( 1 ), "for" ) );
	}


	private void assertEquals( String[] expected, String[] test ) {
		if ( !Arrays.equals( expected, test ) ) {
			fail( Arrays.toString( test ) + " does not match the expected " +
				Arrays.toString( expected ) );
		}
	}
}
