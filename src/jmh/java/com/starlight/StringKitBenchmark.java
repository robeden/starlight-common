package com.starlight;

import org.openjdk.jmh.annotations.Benchmark;

/**
 *
 */
public class StringKitBenchmark {
	private static final String TO_SPLIT =
		"..acbdfjkbc.dcbsdjkb.weehf.cdfjksbc.dskjbdj..cdsjkbc." +
		"cdsjkcbds.a.s.d.f.g.h.w.r.r.t.y.y.c.c.s.s.f.f.f.g.g.w.w.";


	@Benchmark
	public String[] starlightSplit() {
		return StringKit.split( TO_SPLIT, '.' );
	}


	@Benchmark
	public String[] stringSplit() {
		return TO_SPLIT.split( "\\." );
	}
}