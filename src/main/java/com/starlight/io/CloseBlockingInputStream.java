package com.starlight.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * An InputStream implementation that doesn't pass the close() to the delegate stream.
 */
public class CloseBlockingInputStream extends FilterInputStream {
	public CloseBlockingInputStream( InputStream in ) {
		super( in );
	}

	@Override
	public void close() throws IOException {
		// Don't pass on the close.
	}
}
