package com.starlight.io;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;


/**
 * Reader implementation that doesn't pass the close() to the delegate stream.
 */
public class CloseBlockingWriter extends FilterWriter {
	public CloseBlockingWriter( Writer out ) {
		super( out );    // TODO: implement
	}

	@Override
	public void close() throws IOException {
		// Don't pass on the close.
	}
}