package com.starlight.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;


/**
 * An OutputStream implementation that doesn't pass the close() to the delegate stream.
 */
public class CloseBlockingOutputStream extends FilterOutputStream {
	public CloseBlockingOutputStream( OutputStream out ) {
		super( out );    // TODO: implement
	}

	@Override
	public void close() throws IOException {
		// Don't pass on the close.
	}
}
