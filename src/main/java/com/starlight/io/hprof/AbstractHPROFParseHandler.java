/*
 * Copyright (c) 2012 Rob Eden.
 * All Rights Reserved.
 */

package com.starlight.io.hprof;

/**
 *
 */
public abstract class AbstractHPROFParseHandler implements HPROFParseHandler {
	@Override
	public void beginRecord( byte tag_type, long micros_since_header, long length ) {}

	@Override
	public void end() {}

	@Override
	public void endRecord( byte tag_type ) {}

	@Override
	public void headDumpPrimitiveArrayDump( ID object_id, int stack_trace_serial_number,
		int number_of_elements, byte entry_type ) {}

	@Override
	public void header( String format_name, int identifier_size, long time ) {}

	@Override
	public void heapDumpArrayDump( ID object_id, int stack_trace_serial_number,
		int number_of_elements, ID array_class_object_id ) {}

	@Override
	public void heapDumpClassDump( ID object_id, int stack_trace_serial_number,
		ID super_class_id, ID class_loader_id, ID signers_object_id,
		ID protection_domain_object_id, long instance_size, int[] constant_pool_indexes,
		BasicType[] constant_pool_types, Object[] constant_pool_values,
		ID[] static_field_names, BasicType[] static_field_types,
		Object[] static_field_values, ID[] instance_field_names,
		BasicType[] instance_field_types ) {}

	@Override
	public void heapDumpInstanceDump( ID object_id, int stack_trace_serial_number,
		ID class_object_id, long data_length ) {}

	@Override
	public void heapDumpRootJavaFrame( ID object_id, int thread_serial_number,
		int frame_number_in_trace ) {}

	@Override
	public void heapDumpRootJNIGlobal( ID object_id, ID jni_global_ref_id ) {}

	@Override
	public void heapDumpRootJNILocal( ID object_id, int thread_serial_number,
		int frame_number_in_trace ) {}

	@Override
	public void heapDumpRootMonitorUsed( ID object_id ) {}

	@Override
	public void heapDumpRootNativeStack( ID object_id, int thread_serial_number ) {}

	@Override
	public void heapDumpRootStickyClass( ID object_id ) {}

	@Override
	public void heapDumpRootThreadBlock( ID object_id, int thread_serial_number ) {}

	@Override
	public void heapDumpRootThreadObject( ID object_id, int thread_serial_number,
		int stack_trace_serial_number ) {}

	@Override
	public void heapDumpRootUnknown( ID object_id ) {}

	@Override
	public void recordControlSettings( int bit_mask_flags, int stack_trace_depth ) {}

	@Override
	public void recordCPUSamples( long total_number_of_samples, int[] number_of_samples,
		int[] stack_trace_serial_number ) {}

	@Override
	public void recordHeapDump() {}

	@Override
	public void recordHeapDumpEnd() {}

	@Override
	public void recordHeapDumpSegment() {}

	@Override
	public void recordLoadClass( int class_serial_number, ID object_id,
		int stack_trace_serial, ID class_name_string_id ) {}

	@Override
	public void recordStackFrame( ID frame_id, ID method_name_id, ID method_signature_id,
		ID source_file_name_id, int class_serial_number, int line ) {}

	@Override
	public void recordStackTrace( int trace_serial_number, int thread_serial_number,
		ID[] frame_ids ) {}

	@Override
	public void recordStringInUTF8( ID id, String string ) {}

	@Override
	public void recordUnloadClass( int class_serial_number ) {}
}
