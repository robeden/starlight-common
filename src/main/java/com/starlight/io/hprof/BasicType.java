/*
 * Copyright (c) 2012 Rob Eden.
 * All Rights Reserved.
 */

package com.starlight.io.hprof;

/**
 *
 */
public enum BasicType {
	OBJECT( ( byte ) 2 ),
	BOOLEAN( ( byte ) 4 ),
	CHAR( ( byte ) 5 ),
	FLOAT( ( byte ) 6 ),
	DOUBLE( ( byte ) 7 ),
	BYTE( ( byte ) 8 ),
	SHORT( ( byte ) 9 ),
	INT( ( byte ) 10 ),
	LONG( ( byte ) 11 );

	// Reference to enums for use by fromID()
	private static final BasicType[] TYPE_ACCESS = new BasicType[ 12 ];
	static {
		for( BasicType type : values() ) {
			TYPE_ACCESS[ type.id & 0xFF ] = type;
		}
	}

	private final byte id;

	private BasicType( byte id ) {
		this.id = id;
	}

	byte getID() {
		return id;
	}


	static BasicType fromID( byte id ) {
		return TYPE_ACCESS[ id & 0xFF ];
	}
}
