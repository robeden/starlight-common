/*
 * Copyright (c) 2012 Rob Eden.
 * All Rights Reserved.
 */

package com.starlight.io.hprof;

import java.util.Arrays;


/**
*
*/
public class ID {
	private final byte[] data;

	ID( byte[] data ) {
		this.data = data;
	}

	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( o == null || getClass() != o.getClass() ) return false;

		ID id = ( ID ) o;

		if ( !Arrays.equals( data, id.data ) ) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return data != null ? Arrays.hashCode( data ) : 0;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		for( byte b : data ) {
			String hex = Integer.toHexString( b & 0xff );
			if ( hex.length() == 1 ) buf.append( '0' );
			buf.append( hex );
		}
		return buf.toString();
	}
}
