package com.starlight.io;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;


/**
 * Reader implementation that doesn't pass the close() to the delegate stream.
 */
public class CloseBlockingReader extends FilterReader {
	public CloseBlockingReader( Reader in ) {
		super( in );
	}

	@Override
	public void close() throws IOException {
		// Don't pass on the close.
	}
}
