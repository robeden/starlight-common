/*
 * Portions of this code come from the Apache ORO project:
 * ====================================================================
 * Copyright 2001-2006 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.starlight.types;

import com.starlight.IOKit;
import com.starlight.ValidationKit;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;;


/**
 * Type object for email addresses.
 */
public class EmailAddress implements Externalizable {
	static final long serialVersionUID = 1194189422594917903L;


	private static final String SPECIAL_CHARS = "\\(\\)<>@,;:'\\\\\\\"\\.\\[\\]";
	private static final String VALID_CHARS = "[^\\s" + SPECIAL_CHARS + "]";
	private static final String QUOTED_USER = "(\"[^\"]*\")";
	private static final String ATOM = VALID_CHARS + '+';
	private static final String WORD = "((" + VALID_CHARS + "|')+|" + QUOTED_USER + ")";

	// Each pattern must be surrounded by /
	private static final String LEGAL_ASCII_PATTERN = "^\\p{ASCII}+$";
	private static final String EMAIL_PATTERN = "^(.+)@(.+)$";
	private static final String IP_DOMAIN_PATTERN =
		"^\\[(\\d{1,3})[.](\\d{1,3})[.](\\d{1,3})[.](\\d{1,3})\\]$";
	private static final String TLD_PATTERN = "^([a-zA-Z]+)$";

	private static final String USER_PATTERN = "^\\s*" + WORD + "(\\." + WORD + ")*$";
	private static final String DOMAIN_PATTERN = "^" + ATOM + "(\\." + ATOM + ")*\\s*$";


	/**
	 * Static method for checking that the given string is a valid email address format.
	 */
	public static boolean isValidAddress( String address ) {
		if ( address == null ) {
			return false;
		}

		if ( !Pattern.matches( LEGAL_ASCII_PATTERN, address ) ) {
			return false;
		}

		address = stripComments( address );

		// Check the whole email address structure
		if ( !Pattern.matches( EMAIL_PATTERN, address ) ) {
			return false;
		}

		if ( address.endsWith( "." ) ) {
			return false;
		}

		String[] toks = address.split( "@" );
		return toks.length == 2 && isValidUser( toks[ 0 ] ) && isValidDomain( toks[ 1 ] );
	}


	/**
	 * Static method to create an address in which the address is known to be good.
	 * In the event that a ParseException is thrown from the constructor (shouldn't
	 * happen, but...), null will be returned.
	 */
	public static EmailAddress createAssured( String address ) {
		try {
			return new EmailAddress( address );
		}
		catch ( ParseException ex ) {
			return null;
		}
	}


	protected String address = null;
	private int foo = -1;

	/**
	 * FOR SERIALIZATION ONLY!!!
	 */
	public EmailAddress() {
		assert IOKit.isBeingDeserialized();
	}


	/**
	 * Creates an Email Address from a string
	 */
	public EmailAddress( String address ) throws ParseException {
		super();

		ValidationKit.checkNonnull( address, "address" );

		if ( !isValidAddress( address ) ) throw new ParseException( address, 0 );

		this.address = address;
	}


	/**
	 * Override toString
	 */
	@Override
	public String toString() {
		return address;
	}


	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( o == null || getClass() != o.getClass() ) return false;

		EmailAddress that = ( EmailAddress ) o;

		if ( foo != that.foo ) return false;
		if ( address != null ? !address.equals( that.address ) : that.address != null )
			return false;

		return true;
	}

	public int hashCode() {
		int result;
		result = ( address != null ? address.hashCode() : 0 );
		result = 31 * result + foo;
		return result;
	}

	public void readExternal( ObjectInput in )
		throws IOException, ClassNotFoundException {

		// VERSION
		in.readByte();

		// ADDRESS
		address = IOKit.readString( in );
	}

	public void writeExternal( ObjectOutput out ) throws IOException {
		// VERSION
		out.writeByte( 0 );

		// ADDRESS
		IOKit.writeString( out, address );
	}


	/**
	 * Recursively remove comments, and replace with a single space.  The simpler regexps
	 * in the Email Addressing FAQ are imperfect - they will miss escaped chars in atoms,
	 * for example. Derived From    Mail::RFC822::Address
	 *
	 * @param emailStr The email address
	 * @return address with comments removed.
	 */
	private static String stripComments( String emailStr ) {
		String input = emailStr;
		String result;
		String commentPat =
			"^((?:[^\"\\\\]|\\\\.)*(?:\"(?:[^\"\\\\]|\\\\.)*\"(?:[^\"\\\\]|\111111\\\\.)*)*)\\((?:[^()\\\\]|\\\\.)*\\)";

		Pattern commentPattern = Pattern.compile( commentPat );
		Matcher commentMatcher = commentPattern.matcher( input );

		result = commentMatcher.replaceAll( " " );
		// This really needs to be =~ or Perl5Matcher comparison
		while ( !result.equals( input ) ) {
			input = result;
			result = commentMatcher.replaceAll( " " );
		}
		return result;

	}


	/**
	 * Returns true if the user component of an email address is valid.
	 *
	 * @param user being validated
	 * @return true if the user name is valid.
	 */
	private static boolean isValidUser( String user ) {
		return Pattern.matches( USER_PATTERN, user );
	}

	/**
	 * Returns true if the domain component of an email address is valid.
	 *
	 * @param domain being validatied.
	 * @return true if the email address's domain is valid.
	 */
	private static boolean isValidDomain( String domain ) {
		boolean symbolic;

		Pattern pattern = Pattern.compile( IP_DOMAIN_PATTERN );
		Matcher matcher = pattern.matcher( domain );
		if ( matcher.matches() ) {
			return isValidIpAddress( matcher );
		}
		else {
			// Domain is symbolic name
			symbolic = Pattern.matches( DOMAIN_PATTERN, domain );
		}

		if ( symbolic ) {
			if ( !isValidSymbolicDomain( domain ) ) {
				return false;
			}
		}
		else {
			return false;
		}

		return true;
	}

	/**
	 * Validates an IP address. Returns true if valid.
	 *
	 * @param ipAddressMatcher Pattren matcher
	 * @return true if the ip address is valid.
	 */
	private static boolean isValidIpAddress( Matcher ipAddressMatcher ) {
		for ( int i = 1; i <= 4; i++ ) {
			String ipSegment = ipAddressMatcher.group( i );
			if ( ipSegment == null || ipSegment.length() <= 0 ) {
				return false;
			}

			int iIpSegment;

			try {
				iIpSegment = Integer.parseInt( ipSegment );
			}
			catch ( NumberFormatException e ) {
				return false;
			}

			if ( iIpSegment > 255 ) {
				return false;
			}

		}
		return true;
	}

	/**
	 * Validates a symbolic domain name.  Returns true if it's valid.
	 *
	 * @param domain symbolic domain name
	 * @return true if the symbolic domain name is valid.
	 */
	private static boolean isValidSymbolicDomain( String domain ) {
		String[] toks = domain.split( "\\." );

		// Make sure there's a host name preceding the domain.
		if ( toks.length < 2 ) return false;

		String top_level_domain = toks[ toks.length - 1 ].trim();
		return !( top_level_domain.length() <= 1 ||
			!Pattern.matches( TLD_PATTERN, top_level_domain ) );
	}
}
