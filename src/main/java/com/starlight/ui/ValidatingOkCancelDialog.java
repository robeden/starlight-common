/*
 * Copyright (c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.starlight.ValidationKit;
import com.starlight.locale.ResourceKey;
import com.starlight.ui.validity.FieldValidityChecker;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * This is a dialog that displays a component allong with ok/cancel buttons. The OK button
 * will not light up until the component has "valid" data. It used by simply passing in
 * a component with the real content of the panel to the constructor. When the component
 * contains valid data, call {@link #setContentValid setContentValid(true)} to enable the
 * OK button. When the dialog is no longer visible, you can see if it was canceled by
 * calling {@link #wasCanceled}.
 *
 * @author reden
 */
@SuppressWarnings( "UnusedDeclaration" )
public class ValidatingOkCancelDialog extends EscapeDialog implements ActionListener {
	private JButton ok_button;

	private boolean was_canceled = true;

	private JPanel user_content_wrapper_panel;


	public static ValidatingOkCancelDialog create( Component parent,
		ResourceKey<String> title, boolean modal, JComponent content, boolean resizable,
		FieldValidityChecker validator, JComponent... additional_buttons ) {

		Window parent_window;
		if ( parent == null ) parent_window = null;
		else if ( parent instanceof Window ) parent_window = ( Window ) parent;
		else parent_window = SwingUtilities.getWindowAncestor( parent );

		final ValidatingOkCancelDialog dialog;
		if ( parent_window == null || parent_window instanceof JFrame ) {
			dialog = new ValidatingOkCancelDialog( ( JFrame ) parent_window,
				title.getValue(), modal, content, validator.check(), resizable,
				additional_buttons );
		}
		else dialog = new ValidatingOkCancelDialog( ( JDialog ) parent_window,
			title.getValue(), modal, content, validator.check(), resizable,
			additional_buttons );

		validator.addListener( new FieldValidityChecker.Listener() {
			@Override
			public void validityChanged( boolean is_valid ) {
				dialog.setContentValid( is_valid );
			}
		} );

		dialog.setContentValid( validator.check() );

		return dialog;
	}


	private ValidatingOkCancelDialog( JFrame owner, String title, boolean modal,
		JComponent content, boolean initially_valid, boolean resizable,
		JComponent... additional_buttons ) {

		super( owner, title, modal );

		init( content, initially_valid, resizable, additional_buttons );
	}

	private ValidatingOkCancelDialog( JDialog owner, String title, boolean modal,
		JComponent content, boolean initially_valid, boolean resizable,
		JComponent... additional_buttons ) {

		super( owner, title, modal );

		init( content, initially_valid, resizable, additional_buttons );
	}


	/**
	 * Should be called when the contents of the dialog become valid or invalid. This will
	 * cause the OK/cancel buttons to reflect the change appropriately.
	 */
	public void setContentValid( final boolean is_valid ) {
		if ( !SwingUtilities.isEventDispatchThread() ) {
			SwingUtilities.invokeLater( new Runnable() {
				@Override
				public void run() {
					setContentValid( is_valid );
				}
			} );
			return;
		}

		getRootPane().setDefaultButton( is_valid ? ok_button : null );
		ok_button.setEnabled( is_valid );
	}

	/**
	 * Check when the dialog is no longer visible to see if it was canceled.
	 */
	public boolean wasCanceled() {
		return was_canceled;
	}


	public void actionPerformed( ActionEvent e ) {
		if ( e.getActionCommand().equals( "OK" ) ) was_canceled = false;
		setVisible( false );
	}


	public void setVisible( boolean b ) {
		if ( b ) was_canceled = true;

		super.setVisible( b );
	}


	public void setContentBorderEnabled( boolean enabled ) {
		Border border;
		if ( enabled ) border = new EmptyBorder( 15, 15, 0, 15 );
		else border = null;

		user_content_wrapper_panel.setBorder( border );
	}


	private void init( JComponent user_content, boolean initally_valid,
		boolean resizable, JComponent... additional_buttons ) {

		ValidationKit.checkNonnull( user_content, "Content" );

		user_content_wrapper_panel = new JPanel( new BorderLayout( 0, 0 ) );
		user_content_wrapper_panel.add( user_content, BorderLayout.CENTER );
		setContentBorderEnabled( true );

		JPanel content = new JPanel( new BorderLayout() );

		content.add( user_content_wrapper_panel, BorderLayout.CENTER );

		ok_button = new JButton( "OK" );
		ok_button.setActionCommand( "OK" );
		ok_button.addActionListener( this );

		JButton cancel_button = new JButton( "Cancel" );
		cancel_button.setActionCommand( "CANCEL" );
		cancel_button.addActionListener( this );

		ButtonBarBuilder builder = new ButtonBarBuilder();
		if ( additional_buttons != null && additional_buttons.length > 0 ) {
			builder.addButton( additional_buttons );
		}
		builder.addGlue();
		builder.addButton( ok_button, cancel_button );

		builder.getPanel().setBorder(
			BorderFactory.createEmptyBorder( 15, 15, 15, 15 ) );
		content.add( builder.getContainer(), BorderLayout.PAGE_END );

		super.setContentPane( content );

		setResizable( resizable );

		pack();

		setContentValid( initally_valid );
	}


//	public static void main( String[] args ) {
//		JPanel panel = new JPanel( new BorderLayout() );
//		panel.add( new JLabel( "Enter a number" ), BorderLayout.NORTH );
//
//		final JTextField field = new JTextField();
//		panel.add( field, BorderLayout.CENTER );
//
//		final ValidatingOkCancelDialog dialog = new ValidatingOkCancelDialog(
//			( JFrame ) null, "Test", true, panel, false );
//
//		field.getDocument().addDocumentListener(
//			new javax.swing.event.DocumentListener() {
//				public void insertUpdate( DocumentEvent e ) {
//					check();
//				}
//				public void removeUpdate( DocumentEvent e ) {
//					check();
//				}
//				public void changedUpdate( DocumentEvent e ) {
//					check();
//				}
//
//				private void check() {
//					String text = field.getText();
//					if ( text == null ) dialog.setContentValid( false );
//					else {
//						try {
//							Integer.parseInt( text );
//							dialog.setContentValid( true );
//						}
//						catch( Exception ex ) {
//							dialog.setContentValid( false );
//						}
//					}
//				}
//			} );
//
//		dialog.setVisible( true );
//		System.exit( 0 );
//	}
}
