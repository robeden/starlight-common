/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;


/**
 * Adding this as a listener to a text (or password) field will cause the contents of the
 * field to be selected when focus is gained.
 *
 * @author reden
 */
public class SelectOnFocusListener implements FocusListener {
	public static void install( JTextComponent component ) {
		new SelectOnFocusListener( component );
	}
	

	/**
	 * Create the focus listener and add it to the component.
	 */
	public SelectOnFocusListener( JTextComponent target_component ) {
		target_component.addFocusListener( this );
	}

	public void focusGained( FocusEvent e ) {
		final JTextComponent tc = ( JTextComponent ) e.getSource();

		final int length;
		if ( tc instanceof JPasswordField ) {
			char[] pass = ( ( JPasswordField ) tc ).getPassword();
			length = pass == null ? 0 : pass.length;
		}
		else {
			String text = tc.getText();

			length = text == null ? 0 : text.length();
		}

		tc.setSelectionStart( 0 );
		tc.setSelectionEnd( length );

		// Clear the selection on the other component
		Component other = e.getOppositeComponent();
		if ( other instanceof JTextComponent ) {
			JTextComponent jtcother = ( JTextComponent ) other;
			jtcother.setSelectionStart( 0 );
			jtcother.setSelectionEnd( 0 );
		}

		// Formatted text fields are stupid, so have to fire this twice. HACK!!
		if ( tc instanceof JFormattedTextField ) {
			SwingUtilities.invokeLater( new Runnable() {
				public void run() {
					tc.setSelectionStart( 0 );
					tc.setSelectionEnd( length );
				}
			} );
		}
	}

	public void focusLost( FocusEvent e ) {
		// do nothing
	}
}
