/*
 * Copyright (c) 2010 Rob Eden.
 * All Rights Reserved.
 */

package com.starlight.ui.debug;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import javax.swing.*;
import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;


/**
 *
 */
public class UIDebugger {
	static DetailsWindow window = new DetailsWindow();

	private static final List<GlassPainter> painters = new ArrayList<GlassPainter>();

	static {
		window.addWindowListener( new WindowAdapter() {
			@Override
			public void windowClosing( WindowEvent e ) {
				setVisible( false );
			}

			@Override
			public void windowOpened( WindowEvent e ) {
				setVisible( true );
			}

			@Override
			public void windowActivated( WindowEvent e ) {
				setVisible( true );
			}


			private void setVisible( boolean visible ) {
				for( GlassPainter painter : painters ) {
					painter.setVisible( visible );
				}
			}
		} );
	}

	/**
	 * Install the debugger on a JFrame, JDialog or JWindow.
	 *
	 * @param window		The window to install on to. Must be a JFrame, JDialog or
	 * 						JWindow.
	 */
	public static void install( Window window ) {
		assert SwingUtilities.isEventDispatchThread();

		GlassPainter painter = new GlassPainter( window );

		if ( window instanceof JDialog ) {
			( ( JDialog ) window ).setGlassPane( painter );
		}
		else if ( window instanceof JFrame ) {
			( ( JFrame ) window ).setGlassPane( painter );
		}
		else if ( window instanceof JWindow ) {
			( ( JWindow ) window ).setGlassPane( painter );
		}
		else {
			throw new IllegalArgumentException(
				"Window must be a JDialog, JFrame or JWindow" );
		}

		painter.setVisible( UIDebugger.window.isVisible() );

		Toolkit.getDefaultToolkit().addAWTEventListener( painter,
			AWTEvent.MOUSE_MOTION_EVENT_MASK );
		painters.add( painter );
	}


	public static void uninstall( Window window ) {
		assert SwingUtilities.isEventDispatchThread();

		Component glass_pane;
		if ( window instanceof JDialog ) {
			glass_pane = ( ( JDialog ) window ).getGlassPane();
		}
		else if ( window instanceof JFrame ) {
			glass_pane = ( ( JFrame ) window ).getGlassPane();
		}
		else if ( window instanceof JWindow ) {
			glass_pane = ( ( JWindow ) window ).getGlassPane();
		}
		else {
			throw new IllegalArgumentException(
				"Window must be a JDialog, JFrame or JWindow" );
		}

		if ( !( glass_pane instanceof GlassPainter ) ) return;

		GlassPainter painter = ( GlassPainter ) glass_pane;

		Toolkit.getDefaultToolkit().removeAWTEventListener( painter );
		painters.remove( painter );
	}


	public static void setVisible( boolean visible ) {
		window.setVisible( visible );
	}


	static void setActiveComponent( Component c ) {
		window.setComponent( c );
	}


	static boolean isEnabled() {
		return window.isVisible();
	}


	private UIDebugger() {} // hidden constructor
	

	public static void main( String[] args ) {
		JPanel panel = new JPanel( new FormLayout( "10px, fill:pref, 10px, fill:pref, 10px",
			"10px, fill:pref, 10px, fill:pref, 10px" ) );
		CellConstraints cc = new CellConstraints();

		panel.add( new JButton( "Test Button" ), cc.xy( 2, 2 ) );
		panel.add( new JCheckBox( "Test Check" ), cc.xy( 4, 2 ) );
		panel.add( new JLabel( "Text Label" ), cc.xy( 2, 4 ) );

		JFrame frame = new JFrame( "Test" );
		frame.setContentPane( panel );
		frame.pack();
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setLocationRelativeTo( null );

		UIDebugger.install( frame );

		frame.setVisible( true );

		UIDebugger.setVisible( true );
	}
}