package com.starlight.ui.debug;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.lang.reflect.Field;


/**
 *
 */
class DetailsWindow extends JDialog {
	private static final Object UI_CLIENT_PROPERTY_KEY;
	static {
		Object key = null;
		try {
			Class clazz = Class.forName( "sun.swing.SwingUtilities2" );
			Field field = clazz.getField( "COMPONENT_UI_PROPERTY_KEY" );
			key = field.get( clazz );
		}
		catch( Throwable t ) {
			// ignore
		}
		UI_CLIENT_PROPERTY_KEY = key;
	}


	private final DetailsPanel panel1 = new DetailsPanel( Color.RED );
	private final DetailsPanel panel2 = new DetailsPanel( Color.GREEN );


	DetailsWindow() {
		super( ( Frame ) null, "UI Debugger", false );

		setLayout( new GridLayout( 1, 2 ) );
		( ( JComponent ) getContentPane() ).setBorder(
			BorderFactory.createEmptyBorder( 10, 10, 10, 10 ) );

		add( panel1 );
		add( panel2 );

		pack();

		setSize( getWidth() + 100, getHeight() );

		setDefaultCloseOperation( JDialog.HIDE_ON_CLOSE );
	}


	void setComponent( Component c ) {
		assert SwingUtilities.isEventDispatchThread();

		panel1.fill( c );
		if ( c == null || c.getParent() == null ) panel2.fill( null );
		else panel2.fill( c.getParent() );
	}



	private class DetailsPanel extends JPanel {
		private JLabel type_label = new JLabel();
		private JLabel name_label = new JLabel();
		private JLabel id_label = new JLabel();
		private JLabel ui_label = new JLabel();
		private JLabel size_label = new JLabel();
		private JLabel pref_label = new JLabel();
		private JLabel min_label = new JLabel();
		private JLabel max_label = new JLabel();
		private JLabel layout_label = new JLabel();
		private JLabel border_label = new JLabel();
		private JLabel constraint_in_parent_label = new JLabel();

		private Dimension reused_dim = new Dimension();
		private StringBuilder reused_buf = new StringBuilder();

		DetailsPanel( Color color ) {
			setLayout( new FormLayout(
				"fill:pref, 1dlu, fill:pref:grow, 8dlu, fill:pref, 1dlu, fill:pref:grow",
				"2px, 3px, fill:pref, 1dlu, fill:pref, 3dlu, fill:pref, 3dlu, " +
				"fill:pref, 1dlu, fill:pref, 3dlu, fill:pref, 1dlu, fill:pref, 1dlu, " +
				"fill:pref" ) );
			CellConstraints cc = new CellConstraints();

			JPanel color_panel = new JPanel();
			color_panel.setBackground( color );
			color_panel.setOpaque( true );
			add( color_panel, cc.xyw( 1, 1, 7 ) );
			
			add( new JLabel( "Type:" ), cc.xy( 1, 3 ) );
			add( type_label, cc.xyw( 3, 3, 5 ) );

			add( new JLabel( "Name:" ), cc.xy( 1, 5 ) );
			add( name_label, cc.xy( 3, 5 ) );

			add( new JLabel( "ID:" ), cc.xy( 5, 5 ) );
			add( id_label, cc.xy( 7, 5 ) );

			add( new JLabel( "UI:" ), cc.xy( 1, 7 ) );
			add( ui_label, cc.xyw( 3, 7, 5 ) );

			add( new JLabel( "Size:" ), cc.xy( 1, 9  ) );
			add( size_label, cc.xy( 3, 9 ) );

			add( new JLabel( "Max:" ), cc.xy( 5, 9  ) );
			add( max_label, cc.xy( 7, 9 ) );

			add( new JLabel( "Pref:" ), cc.xy( 1, 11 ) );
			add( pref_label, cc.xy( 3, 11 ) );

			add( new JLabel( "Min:" ), cc.xy( 5, 11  ) );
			add( min_label, cc.xy( 7, 11 ) );

			add( new JLabel( "Layout:" ), cc.xy( 1, 13 ) );
			add( layout_label, cc.xyw( 3, 13, 5 ) );

			add( new JLabel( "Border:" ), cc.xy( 1, 15 ) );
			add( border_label, cc.xyw( 3, 15, 5 ) );

			JPanel parent_constraint_panel = new JPanel(
				new FormLayout( "fill:pref, 1dlu, fill:pref:grow", "fill:pref" ) );
			parent_constraint_panel.add( new JLabel( "Constraint:"),
				cc.xy( 1, 1 ) );
			parent_constraint_panel.add( constraint_in_parent_label, cc.xy( 3, 1 ) );
			add( parent_constraint_panel, cc.xyw( 1, 17, 7 ) );

			max_label.setText( "0000x0000" );
			max_label.setPreferredSize( max_label.getPreferredSize() );
			max_label.setText( null );
			min_label.setPreferredSize( max_label.getPreferredSize() );
			pref_label.setPreferredSize( max_label.getPreferredSize() );
			size_label.setPreferredSize( max_label.getPreferredSize() );
		}


		void fill( Component c ) {
			if ( c == null ) {
				type_label.setText( null );
				name_label.setText( null );
				id_label.setText( null );
				ui_label.setText( null );
				size_label.setText( null );
				pref_label.setText( null );
				min_label.setText( null );
				max_label.setText( null );
				layout_label.setText( null );
				border_label.setText( null );
				constraint_in_parent_label.setText( null );
				return;
			}

			type_label.setText( c.getClass().getSimpleName() );
			name_label.setText( c.getName() == null ? "-" : c.getName() );
			id_label.setText( String.valueOf( System.identityHashCode( c ) ) );
			if ( c instanceof JComponent && UI_CLIENT_PROPERTY_KEY != null ) {
				Object ui =
					( ( JComponent ) c ).getClientProperty( UI_CLIENT_PROPERTY_KEY );
				if ( ui == null ) ui_label.setText( "-" );
				else ui_label.setText( ui.getClass().getSimpleName() );
			}
			else ui_label.setText( "-" );

			size_label.setText( getSizeText( c.getSize( reused_dim ) ) );
			min_label.setText( getSizeText( c.getMinimumSize() ) );
			max_label.setText( getSizeText( c.getMaximumSize() ) );
			pref_label.setText( getSizeText( c.getPreferredSize() ) );

			if ( c instanceof Container ) {
				LayoutManager layout_mgr = ( ( Container ) c ).getLayout();
				if ( layout_mgr == null ) layout_label.setText( "-" );
				else layout_label.setText( layout_mgr.getClass().getSimpleName() );
			}
			else layout_label.setText( "-" );

			Border border = null;
			if ( c instanceof JComponent ) {
				border = ( ( JComponent ) c ).getBorder();
			}
			border_label.setText(
				border == null ? "null" : border.getClass().getSimpleName() );

			Container parent = c.getParent();
			if ( parent == null ) constraint_in_parent_label.setText( "-" );
			else {
				LayoutManager parent_layout = parent.getLayout();
				constraint_in_parent_label.setText(
					getConstraintText( parent_layout, c ) );
			}
		}


		private String getSizeText( Dimension dim ) {
			reused_buf.setLength( 0 );

			if ( dim == null ) return "-";

			if ( dim.width > 20000 ) {
				reused_buf.append( "MAX" );
			}
			else reused_buf.append( dim.width );

			reused_buf.append( "x" );

			if ( dim.height > 20000 ) {
				reused_buf.append( "MAX" );
			}
			else reused_buf.append( dim.height );

			return reused_buf.toString();
		}


		private String getConstraintText( LayoutManager layout, Component child ) {
			if ( layout == null || child == null ) return "-";

			if ( layout instanceof FormLayout ) {
				FormLayout form_layout = ( FormLayout ) layout;
				CellConstraints cc = form_layout.getConstraints( child );
				if ( cc == null ) return "-";

				String string = cc.toShortString( form_layout );
				if ( string.startsWith( "( " ) && string.endsWith( ")" ) ) {
					return string.substring( 2, string.length() - 1 );
				}
				else return string;
			}
			else if ( layout instanceof BorderLayout ) {
				Object constraint = ( ( BorderLayout ) layout ).getConstraints( child );
				if ( constraint == null ) return "-";
				else return constraint.toString();
			}
			else return "n/a (" + layout.getClass().getSimpleName() + ")";
		}
	}
}
