/*
 * Copyright (c) 2010 Rob Eden.
 * All Rights Reserved.
 */

package com.starlight.ui.debug;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;


/**
 *
 */
class GlassPainter extends JComponent implements AWTEventListener {
	private final Container content;

	private final Rectangle reused_rect = new Rectangle();
	private final Point reused_point = new Point();

	private Component active_component = null;


	GlassPainter( Window parent ) {
		if ( parent instanceof JDialog ) {
			content = ( ( JDialog ) parent ).getContentPane();
		}
		else if ( parent instanceof JFrame ) {
			content = ( ( JFrame ) parent ).getContentPane();
		}
		else if ( parent instanceof JWindow ) {
			content = ( ( JWindow ) parent ).getContentPane();
		}
		else throw new IllegalArgumentException();

		setOpaque( false );
	}


	@Override
	protected void paintComponent( Graphics g ) {
		if ( !UIDebugger.isEnabled() ) return;


		if ( active_component == null ) return;

		// Parent component
		if ( active_component.getParent() != null ) {
			drawBounds( g, Color.GREEN, active_component.getParent() );
		}

		// Active component
		drawBounds( g, Color.RED, active_component );
	}


	public void eventDispatched( AWTEvent event ) {
		if ( !UIDebugger.isEnabled() ) return;

		MouseEvent evt = ( MouseEvent ) event;

		reused_point.setLocation( evt.getXOnScreen(), evt.getYOnScreen() );
		SwingUtilities.convertPointFromScreen( reused_point, content );

		Component c = content.findComponentAt( reused_point.x, reused_point.y );
		if ( c != active_component ) {
			active_component = c;
			UIDebugger.setActiveComponent( c );
			repaint();
		}
	}


	private void drawBounds( Graphics g, Color color, Component c ) {
		if ( c == null ) return;

		g.setColor( color );
		c.getBounds( reused_rect );
		reused_point.setLocation( reused_rect.x, reused_rect.y );

		SwingUtilities.convertPointToScreen( reused_point, c.getParent() );
		SwingUtilities.convertPointFromScreen( reused_point, this );

		g.drawRect( reused_point.x, reused_point.y, reused_rect.width - 1,
			reused_rect.height - 1 );
		g.drawRect( reused_point.x - 1, reused_point.y - 1, reused_rect.width + 1,
			reused_rect.height + 1 );
	}
}
