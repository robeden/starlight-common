/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */

package com.starlight.ui;

import javax.swing.*;
import java.awt.*;


/**
 * Simple Icon implement to fill the area given a Paint object (could be a Color).
 */
public class PaintIcon implements Icon {
	private final int width;
	private final int height;

	private Paint paint;
	private Color border_color = null;

	public PaintIcon( Paint paint, int width, int height ) {
		this.paint = paint;
		this.width = width;
		this.height = height;
	}


	public void setPaint( Paint paint ) {
		this.paint = paint;
	}

	public Paint getPaint() {
		return paint;
	}


	public void setBorderColor( Color border_color ) {
		this.border_color = border_color;
	}

	public Color getBorderColor() {
		return border_color;
	}


	@Override
	public int getIconWidth() {
		return width;
	}

	@Override
	public int getIconHeight() {
		return height;
	}

	@Override
	public void paintIcon( Component c, Graphics g, int x, int y ) {
		( ( Graphics2D ) g ).setPaint( paint );
		g.fillRect( x, y, width - 1, height - 1 );

		if ( border_color != null ) {
			g.setColor( border_color );
			g.drawRect( x, y, width - 1, height - 1 );
		}
	}
}
