/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */

package com.starlight.ui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Iterator;
import java.util.LinkedList;


/**
 * This is a panel that allows easy setup of the components with labels
 * that conform to the Java Look & Feel Guidelines. The tricky part is that
 * the labels and the "real" components must both be left justified.
 * For example:
 * <P><PRE>
 *     Label 1:            XXXXXXXXXXXXXXXX
 *     Some Other Label 2: YYYYYYYYYYY
 *     3:                  ZZZZZZZZZZZZZZZZZZZZZZ
 *                         ZZZZZZZZZZZZZZZZZZZZZZ
 *                         ZZZZZZZZZZZZZZZZZZZZZZ
 * </PRE></P>
 * In this example, the labels are all left justified, so the right edges don't
 * line up. The same holds true with the components on the right. The other
 * thing to notices is that the components are no the same size (which is what
 * would happen if a GridLayout was used).
 *
 * @author  $Author: reden $
 * @version $Revision: #4 $ - $DateTime: 2003/03/24 20:49:30 $
 */
public class LabeledComponentPanel extends JPanel {
    private static final int LABEL_HORIZONTAL_PADDING = 5;
    private Box box = null;
    private java.util.List sections = new LinkedList();

    private int max_label_width = -1;

	private int hgap;
	private int vgap;

	private boolean default_stretch_horizontal;

    public LabeledComponentPanel() {
		this( 2, 7, true );
    }

    public LabeledComponentPanel( boolean default_stretch_horizontal ) {
		this( 2, 7, default_stretch_horizontal );
    }

    public LabeledComponentPanel( int vgap, int hgap ) {
		this( vgap, hgap, true );
    }

	public LabeledComponentPanel( int vgap, int hgap, boolean default_stretch_horizontal ) {
		setLayout( new GridBagLayout() );
        box = Box.createVerticalBox();

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridy = 0;
		gbc.weightx = 100;
		gbc.weighty = 100;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		super.add( box, gbc );

		gbc.fill = GridBagConstraints.BOTH;
		super.add( UIKit.createFiller( 0, 0 ), gbc );

		this.vgap = vgap;
		this.hgap = hgap;
		this.default_stretch_horizontal = default_stretch_horizontal;
	}

    public synchronized void addLabelAndComponent( JComponent label, JComponent component ) {
		addLabelAndComponent( label, component, true );
	}

    public synchronized void addLabelAndComponent( JComponent label, JComponent component,
		boolean center_vertically ) {

		addLabelAndComponent( label, component, center_vertically,
			default_stretch_horizontal );
	}

	/**
	 * Add the given label and component combo.
	 *
	 * @param label				The label to add.
	 * @param component			The component to add.
	 * @param center_vertically	If true, the label will be centered vertically if
	 * 							the component is taller than the label. If false,
	 * 							the label will be aligned at the top. Note that
	 * 							the component is always centered on the label if
	 * 							the component is smaller than the label.
	 * @param stretch_component	Whether or not the component should be streatched
	 * 							horizontally.
	 */
    public synchronized void addLabelAndComponent( JComponent label, JComponent component,
		boolean center_vertically, boolean stretch_component ) {

		if ( label == null ) label = new JPanel();
		if ( component == null ) component = new JPanel();

        PanelSection section = new PanelSection( label, component,
			center_vertically, stretch_component );

		checkLabelSize( section );

		int vgap = this.vgap;
		// Don't add a gap on the first item
		if ( sections.size() == 0 ) vgap = 0;

	    section.setBorder(new EmptyBorder(vgap, 0, 0, 0));
        sections.add( section );
        box.add( section );

		validate();
    }


	public void removeLabelAndComponent( JComponent label, JComponent component ) {
		PanelSection section = findPanelSection( label );
		if ( section == null ) section = findPanelSection( component );

		if ( section != null ) {
			sections.remove( section );
			box.remove( section );
		}
	}


	/**
	 * Add a component directly to the panel inline with everything else, but without the
	 * normal spacing. This allows components to be intermingled that aren't labels and
	 * components
	 */
	public void addDirect( JComponent component ) {
		box.add( component );
	}


	/**
	 * Add a space to separate components.
	 */
	public void addSpace( int height ) {
		box.add( Box.createVerticalStrut( height ) );
	}


	/**
	 * Looks for the component specified as a label or a component and set the entire
	 * row visible or invisible
	 */
	public synchronized void setVisible(JComponent comp, boolean visible) {
		PanelSection section = findPanelSection( comp );
		if ( section != null ) {
			section.setVisible( visible );
			setBorders();
		}
	}


	/**
	 * Check the label size for the given label or component.
	 */
	public synchronized void checkLabelSize( JComponent comp ) {
		PanelSection section = findPanelSection( comp );
		if ( section != null ) checkLabelSize( section );
	}

	/**
	 * Check the label size for the given label or component.
	 */
	public synchronized void checkLabelSize( PanelSection section ) {
		if ( section == null ) return;

		section.label.setPreferredSize( null );
        int width = section.getLabelSize().width;
		if ( width > 1000 ) {
			System.out.println( "Width " + width + " - " + section.getLabelSize() );
			System.out.println( "Break here" );
		}
		if ( width > max_label_width ) {
            max_label_width = width;
            Iterator it = sections.iterator();
            while( it.hasNext() ) {
                PanelSection tmp_section = ( PanelSection ) it.next();
                tmp_section.setLabelSize( new Dimension(
                    max_label_width + LABEL_HORIZONTAL_PADDING,
                    tmp_section.getLabelSize().height ) );
            }
        }

        section.setLabelSize( new Dimension(
            max_label_width + LABEL_HORIZONTAL_PADDING,
            section.getLabelSize().height ) );
	}


	public Component add( Component comp ) {
		throw new UnsupportedOperationException( "Use addLabelAndComponent" );
	}

	public Component add( String name, Component comp ) {
		throw new UnsupportedOperationException( "Use addLabelAndComponent" );
	}

	public Component add( Component comp, int index ) {
		throw new UnsupportedOperationException( "Use addLabelAndComponent" );
	}

	public void add( Component comp, Object constraints ) {
		throw new UnsupportedOperationException( "Use addLabelAndComponent" );
	}

	public void add( Component comp, Object constraints, int index ) {
		throw new UnsupportedOperationException( "Use addLabelAndComponent" );
	}

	public void remove( int index ) {
		throw new UnsupportedOperationException( "Use removeLabelAndComponent" );
	}

	public void remove( Component comp ) {
		throw new UnsupportedOperationException( "Use removeLabelAndComponent" );
	}

	public void removeAll() {
		box.removeAll();
		sections.clear();
		revalidate();
	}


	private PanelSection findPanelSection( JComponent comp ) {
		Iterator it = sections.iterator();
		while( it.hasNext() ) {
			PanelSection section = ( PanelSection ) it.next();

			if ( section.label == comp || section.component == comp ) {
				return section;
			}
		}

		return null;
	}

    private void setBorders() {
	    if (sections.size() > 1) {
		    Iterator it = sections.iterator();
		    it.next();
			while (it.hasNext()) {
				PanelSection sec = (PanelSection) it.next();
				sec.setBorder(new EmptyBorder(hgap, 0, 0, 0));
			}
	    }
    }

    public void setOpaque( boolean value ) {
        super.setOpaque( value );
        if ( sections != null ) {
            Iterator it = sections.iterator();
            while( it.hasNext() ) {
                ( ( JPanel ) it.next() ).setOpaque( value );
            }
        }
    }


    public void setBackground( Color value ) {
        super.setBackground( value );
        if ( box != null ) box.setBackground( value );
        if ( sections != null ) {
            Iterator it = sections.iterator();
            while( it.hasNext() ) {
                ( ( JPanel ) it.next() ).setBackground( value );
            }
        }
    }


    private class PanelSection extends JPanel {
        private JComponent label;
        private JComponent component;

        PanelSection( JComponent label, JComponent component,
			boolean center_vertically, boolean stretch_component ) {

			super( new GridBagLayout() );

	        setOpaque( false );

            this.label = label;
            this.component = component;

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
			gbc.weightx = 1;
			gbc.weighty = 1;
			if ( center_vertically ) {
				gbc.anchor = GridBagConstraints.WEST;
			}
			else {
				gbc.anchor = GridBagConstraints.NORTHWEST;
				Insets i = gbc.insets;
				i.top += 3;
			}

            add( label, gbc );

			gbc = new GridBagConstraints();
			gbc.gridx = 1;
			gbc.weightx = 100.0;
			gbc.weighty = 1.0;
			gbc.anchor = GridBagConstraints.NORTHWEST;
			if ( stretch_component ) gbc.fill = GridBagConstraints.HORIZONTAL;
			else gbc.fill = GridBagConstraints.NONE;
			add( component, gbc );
        }

        public void setOpaque( boolean value ) {
            super.setOpaque( value );
            if ( label != null ) label.setOpaque( value );
            if ( component != null ) component.setOpaque( value );
        }

        public void setBackground( Color value ) {
            super.setBackground( value );
            if ( label != null ) label.setBackground( value );
            if ( component != null ) component.setBackground( value );
        }

        Dimension getLabelSize() {
            return label.getPreferredSize();
        }

        void setLabelSize( Dimension size ) {
			if ( size.width > 1000 ) {
				new Throwable( "Set label size: " + size ).printStackTrace(  );
			}
			label.setPreferredSize( size );
        }


        Dimension getComponentSize() {
            return component.getPreferredSize();
        }

        void setComponentSize( Dimension size ) {
			System.out.println( "Set component size: " + size );
            component.setPreferredSize( size );
        }
    }


	public static void main( String[] args ) {
		LabeledComponentPanel lcp = new LabeledComponentPanel( false );
		lcp.addLabelAndComponent( new JLabel( "Normal 1: " ), new JTextField( 10 ) );
		lcp.addLabelAndComponent( new JLabel( "Normal 2: " ), new JTextField( 5 ) );
		lcp.addLabelAndComponent( new JLabel( "Normal 3: " ), new JTextField( 20 ) );
		lcp.addLabelAndComponent( new JLabel( "Stretched: " ), new JTextField( 5 ), true, true );
		lcp.addLabelAndComponent( new JLabel( "Normal Tall: " ), new JTextArea( 5, 10 ) );
		lcp.addLabelAndComponent( new JLabel( "Not Centered Tall: " ), new JTextArea( 5, 10 ), false );

		UIKit.testComponent( lcp );
	}
}