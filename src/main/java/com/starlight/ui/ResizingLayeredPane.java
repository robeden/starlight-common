package com.starlight.ui;

import javax.swing.*;


/**
 * A {@link javax.swing.JLayeredPane} that automatically keeps the bounds of all its
 * children in sync.
 */
public class ResizingLayeredPane extends JLayeredPane {
	@SuppressWarnings( "deprecation" )
	@Override
	public void resize( int width, int height ) {
		super.resize( width, height );
		for( int i = 0; i < getComponentCount(); i++ ) {
			getComponent( i ).setSize( width, height );
		}
	}

	@SuppressWarnings( "deprecation" )
	@Override
	public void reshape( int x, int y, int w, int h ) {
		super.reshape( x, y, w, h );
		for( int i = 0; i < getComponentCount(); i++ ) {
			getComponent( i ).setBounds( x, y, w, h );
		}
	}
}
