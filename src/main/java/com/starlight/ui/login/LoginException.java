/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.login;


/**
 *
 * @author reden
 */
public class LoginException extends Exception {
	public LoginException( Throwable cause ) {
		super( cause );
	}

	public LoginException( String message, Throwable cause ) {
		super( message, cause );
	}

	public LoginException( String message ) {
		super( message );
	}
}
