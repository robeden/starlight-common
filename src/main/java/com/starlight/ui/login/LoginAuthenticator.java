/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.login;

import java.util.Map;


/**
 *
 * @author reden
 */
public interface LoginAuthenticator<T> {
	public T authenticate( String username, char[] password, Map options,
		boolean dev_mode ) throws LoginException;
}
