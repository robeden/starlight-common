/*
 * Copyright(c) 2007, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui;

import com.starlight.StringKit;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.*;
import java.net.URL;


/**
 *
 */
public class SearchTextBox extends JLayeredPane {
	private final ImageIcon clear_icon;

	private final JLabel search_label;
	private final JLabel clear_label;
	private final JTextField text_field;

	private final int search_icon_height;
	private final int search_icon_width;
	private final int clear_icon_width;
	private final int clear_icon_height;

	public SearchTextBox() {
		URL url = getClass().getResource( "images/search.png" );
		if ( url != null ) {
			ImageIcon search_icon = new ImageIcon( url );
			search_icon_height = search_icon.getIconHeight();
			search_icon_width = search_icon.getIconWidth();
			search_label = new JLabel( search_icon );
			add( search_label, Integer.valueOf( 100 ) );
		}
		else {
			search_label = new JLabel();
			search_label.setPreferredSize( new Dimension( 16, 16 ) );
			search_icon_height = 0;
			search_icon_width = 0;
		}

		url = getClass().getResource( "images/search_clear.png" );
		if ( url != null ) {
			clear_icon = new ImageIcon( url );
			clear_icon_width = clear_icon.getIconWidth();
			clear_icon_height = clear_icon.getIconHeight();
			clear_label = new JLabel( clear_icon );
	//		clear_button.setOpaque( false );
	//		clear_button.setBorderPainted( false );
			clear_label.setPreferredSize( clear_label.getPreferredSize() );
			clear_label.setMinimumSize( clear_label.getPreferredSize() );
			clear_label.setMaximumSize( clear_label.getPreferredSize() );
			add( clear_label, Integer.valueOf( 100 ) );
		}
		else {
			clear_icon = null;
			clear_label = new JLabel();
			clear_label.setPreferredSize( new Dimension( 16, 16 ) );
			clear_icon_height = 0;
			clear_icon_width = 0;
		}

		text_field = new JTextField();
		text_field.setMargin( new Insets( 0, 6 + search_label.getPreferredSize().width,
			0, 6 + clear_label.getPreferredSize().width ) );
		add( text_field, Integer.valueOf( 0 ) );

		clear_label.setIcon( null );

		addComponentListener( new ComponentAdapter() {
			@Override
			public void componentResized( ComponentEvent e ) {
				positionComponents();
			}

			@Override
			public void componentMoved( ComponentEvent e ) {
				positionComponents();
			}

			@Override
			public void componentShown( ComponentEvent e ) {
				positionComponents();
			}
		});
		positionComponents();


		// When the text field has text, show the clear icon
		text_field.getDocument().addDocumentListener( new DocumentListener() {
			public void insertUpdate( DocumentEvent e ) {
				recheck();
			}

			public void removeUpdate( DocumentEvent e ) {
				recheck();
			}

			public void changedUpdate( DocumentEvent e ) {
				recheck();
			}

			private void recheck() {
				if ( StringKit.isEmpty( text_field.getText() ) ) {
					clear_label.setIcon( null );
				}
				else if ( clear_icon != null ) clear_label.setIcon( clear_icon );
			}
		} );

		clear_label.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent e ) {
				text_field.setText( null );
			}
		} );

		KeyStroke stroke = KeyStroke.getKeyStroke( KeyEvent.VK_ESCAPE, 0 );
		text_field.registerKeyboardAction( new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent e ) {
				text_field.setText( null );
			}
		}, stroke, JComponent.WHEN_FOCUSED );
	}

	public JTextField getTextField() {
		return text_field;
	}


	@Override
	public Dimension getPreferredSize() {
		return text_field.getPreferredSize();
	}

	@Override
	public Dimension getMaximumSize() {
		return text_field.getMaximumSize();
	}

	@Override
	public Dimension getMinimumSize() {
		return text_field.getMinimumSize();
	}

	private void positionComponents() {
		text_field.setBounds( 0, 0, getWidth(), getHeight() );

		int y = ( int )
			( ( ( double ) ( getHeight() - search_icon_height ) ) / 2.0 );
		search_label.setBounds( 6, y, search_icon_width, search_icon_height );

		y = ( int ) ( ( ( double ) ( getHeight() - clear_icon_height ) ) / 2.0 );
		clear_label.setBounds( getWidth() - ( 6 + clear_icon_width ), y, clear_icon_width,
			clear_icon_height );
	}


	public static void main( String[] args ) {
		UIKit.testComponent( new SearchTextBox() );
	}
}
