/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity;

/**
 * Base class for checking the validity of a field.
 */
public interface ValidityCheck {//extends Predicate<FieldValidityChecker> {
	public boolean check();
}