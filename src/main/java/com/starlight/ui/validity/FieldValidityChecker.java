/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity;

import com.starlight.listeners.ListenerSupport;
import com.starlight.listeners.ListenerSupportFactory;
import com.starlight.ui.ValueChangeRelay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Used to check the validity of fields and take action if they're valid or not.
 */
public class FieldValidityChecker {
	static final ThreadLocal<Boolean> PRINTING_STATE = new ThreadLocal<>();

	private static final Logger LOG =
		LoggerFactory.getLogger( FieldValidityChecker.class );

	private static final int STATE_NOT_YET_RUN = -1;
	private static final int STATE_VALID = 1;
	private static final int STATE_INVALID = 0;

	private final ListenerSupport<Listener,?> listeners =
		ListenerSupportFactory.create( Listener.class, false );

	private final List<CheckInfoHolder> checks = new ArrayList<CheckInfoHolder>();

	private final Runnable valid_action;
	private final Runnable invalid_action;
	private final boolean only_fire_when_state_change;

	private boolean stop_checks_on_failure = false;

	private int previous_state = STATE_NOT_YET_RUN;


	public FieldValidityChecker() {
		this( null, null, false );
	}

	public FieldValidityChecker( Runnable valid_action, Runnable invalid_action,
		boolean only_fire_when_state_change ) {

		this.valid_action = valid_action;
		this.invalid_action = invalid_action;
		this.only_fire_when_state_change = only_fire_when_state_change;
	}


	/**
	 * Add a check that will be run when {@link #check()} is called.
	 *
	 * @param check				The check to be run.
	 */
	public void addCheck( ValidityCheck check ) {
		addCheck( check, null, null );
	}

	/**
	 * Add a check that will be run when {@link #check()} is called.
	 *
	 * @param check				The check to be run.
	 * @param valid_action		Action that is take if <b>this check</b> (not all checks)
	 * 							is valid.
	 * @param invalid_action	Action that is take if <b>this check</b> (not all checks)
	 * 							is invalid.
	 */
	public void addCheck( ValidityCheck check, Runnable valid_action,
		Runnable invalid_action ) {

		synchronized( checks ) {
			checks.add( new CheckInfoHolder( check, valid_action, invalid_action ) );
		}
	}

	/**
	 * Remove a check.
	 *
	 * @see #addCheck(ValidityCheck, Runnable, Runnable)
	 */
	public void removeCheck( ValidityCheck check ) {
		synchronized( checks ) {
			Iterator<CheckInfoHolder> it = checks.iterator();
			while( it.hasNext() ) {
				if ( check.equals( it.next().check ) ) {
					it.remove();
					return;
				}
			}
		}
	}

	/**
	 * Clear all checks from the validator.
	 */
	public void clearChecks() {
		synchronized( checks ) {
			checks.clear();
		}
	}


	/**
	 * Controls whether or not all checks are run when a check fails. Normally no further
	 * checks are run when a failure is found, but setting this to false will cause all
	 * checks to be run regardless.
	 */
	public void setStopChecksOnFailure( boolean state ) {
		this.stop_checks_on_failure = state;
	}

	public boolean getStopChecksOnFailure() {
		return stop_checks_on_failure;
	}


	/**
	 * Check all the managed fields.
	 *
	 * @return		True if everything is valid.
	 */
	public boolean check() {
		boolean all_valid = true;
		synchronized( checks ) {
			for ( CheckInfoHolder info : checks ) {
				ValidityCheck check = info.check;

				if ( check.check() ) {
					if ( LOG.isTraceEnabled() ) {
						printState( true, true, check );
					}

					if ( info.state != STATE_VALID ) {
						info.state = STATE_VALID;
						if ( info.valid_action != null && only_fire_when_state_change ) {
							info.valid_action.run();
						}
					}
					if ( info.valid_action != null && !only_fire_when_state_change ) {
						info.valid_action.run();
					}
				}
				else {
					if ( LOG.isDebugEnabled() ) {
						printState( false, false, check );
					}

					all_valid = false;
					if ( info.state != STATE_INVALID ) {
						info.state = STATE_INVALID;
						if ( info.invalid_action != null && only_fire_when_state_change ) {
							info.invalid_action.run();
						}
					}

					if ( info.invalid_action != null && !only_fire_when_state_change ) {
						info.invalid_action.run();
					}

					if ( stop_checks_on_failure ) break;
				}
			}
		}

		if ( all_valid ) {
			// If desired, make sure the state changed
			if ( !only_fire_when_state_change || previous_state != STATE_VALID ) {
				if ( valid_action != null ) valid_action.run();
				listeners.dispatch().validityChanged( true );
			}
		}
		else {
			// If desired, make sure the state changed
			if ( !only_fire_when_state_change || previous_state != STATE_INVALID ) {
				if ( invalid_action != null ) invalid_action.run();
				listeners.dispatch().validityChanged( false );
			}
		}

		previous_state = all_valid ? STATE_VALID : STATE_INVALID;

		return all_valid;
	}


	/**
	 * Return a {@link com.starlight.ui.ValueChangeRelay.ValueChangeListener listener}
	 * that forces a validity check every time something changes.
	 */
	public ValueChangeRelay.ValueChangeListener<?> createVCRListener() {
		return new ValueChangeRelay.ValueChangeListener() {
			public void valueChanged( JComponent component, Object key,
				Object new_value ) {

				check();
			}
		};
	}


	/**
	 * Add a listener that will be notified when the validity state changes.
	 */
	public void addListener( Listener listener ) {
		listeners.add( listener );
	}

	/**
	 * See {@link #addListener(com.starlight.ui.validity.FieldValidityChecker.Listener)}
	 */
	public void removeListener( Listener listener ) {
		listeners.remove( listener );
	}


	private void printState( boolean passed, boolean trace, ValidityCheck check ) {
		Boolean printing_state = PRINTING_STATE.get();
		if ( printing_state != null ) return;

		PRINTING_STATE.set( Boolean.TRUE );
		try {
			String check_tostring = check.toString();
			String message;
			if ( check_tostring.contains( "\n" ) ) {
				message = "Check " + ( passed ? "passed" : "failed" ) + ":\n{}";
			}
			else message = "Check " + ( passed ? "passed" : "failed" ) + ": {}";

			if ( trace ) LOG.trace( message, check );
			else LOG.debug( message, check );
		}
		finally {
			PRINTING_STATE.remove();
		}
	}


	private class CheckInfoHolder {
		final ValidityCheck check;
		final Runnable valid_action;
		final Runnable invalid_action;
		int state = STATE_NOT_YET_RUN;

		CheckInfoHolder( ValidityCheck check, Runnable valid_action,
			Runnable invalid_action ) {

			this.check = check;
			this.valid_action = valid_action;
			this.invalid_action = invalid_action;
		}
	}


	public interface Listener {
		public void validityChanged( boolean is_valid );
	}
}
