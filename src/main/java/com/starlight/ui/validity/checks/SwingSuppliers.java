package com.starlight.ui.validity.checks;

import com.starlight.ValidationKit;
import com.starlight.ui.UIKit;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.Component;
import java.awt.ItemSelectable;
import java.text.ParseException;
import java.util.Date;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;


/**
 *
 */
public class SwingSuppliers {
	private SwingSuppliers() {}


	/**
	 * Returns a supplier indicating the
	 * {@link java.awt.Component#isEnabled() enabled state}.
	 */
	public static BooleanSupplier enabledSupplier( final Component component ) {
		return new ComponentSourceBooleanSupplier<Component>( component ) {
			@Override
			public boolean getAsBoolean() {
				return component.isEnabled();
			}
		};
	}

	/**
	 * Returns a supplier indicating the
	 * {@link javax.swing.AbstractButton#isSelected() button selection state}.
	 */
	public static BooleanSupplier stateSupplier( final AbstractButton button ) {
		return new ComponentSourceBooleanSupplier<AbstractButton>( button ) {
			@Override
			public boolean getAsBoolean() {
				return component.isSelected();
			}
		};
	}

	/**
	 * Returns a supplier with the selection.
	 */
	public static <T> Supplier<T> selectionSupplier( final JComboBox<T> combo ) {
		return new ComponentSourceSupplier<T,JComboBox<T>>( combo ) {
			@Override
			public T get() {
				//noinspection unchecked
				return ( T ) component.getSelectedItem();
			}
		};
	}

	/**
	 * Returns a supplier with the selection.
	 */
	public static Supplier<Object[]> selectionSupplier( final ItemSelectable selectable ) {
		return new Supplier<Object[]>() {
			@Override
			public Object[] get() {
				return selectable.getSelectedObjects();
			}

			@Override
			public String toString() {
				if ( selectable instanceof Component ) {
					return UIKit.describeComponent( ( Component ) selectable );
				}
				else return selectable.getClass().getSimpleName();
			}
		};
	}

	/**
	 * Return a supplier with the selected node count.
	 */
	public static Supplier<Integer> selectionCountSupplier( final JTree tree ) {
		return new ComponentSourceSupplier<Integer,JTree>( tree ) {
			@Override
			public Integer get() {
				return Integer.valueOf( component.getSelectionCount() );
			}
		};
	}


	/**
	 * Returns a supplier with the text of the given {@link java.awt.TextComponent}
	 */
	public static Supplier<String> textSupplier( final JTextComponent component ) {
		return new ComponentSourceSupplier<String,JTextComponent>( component ) {
			@Override
			public String get() {
				if ( component instanceof JFormattedTextField ) {
					JFormattedTextField field = ( JFormattedTextField ) component;

					Object value = field.getValue();

					String text;
					if ( value instanceof String ) {
						text = ( String ) value;
					}
					else text = component.getText();

					String empty_text = null;
					JFormattedTextField.AbstractFormatter formatter = field.getFormatter();
					try {
						empty_text = formatter.valueToString( null );
					}
					catch( ParseException ex ) {
						// ignore
					}

					// Make sure the value isn't the mask. If it is, return null.
					if ( empty_text != null && empty_text.equals( text ) ) {
						return null;
					}

					return text;
				}
				else return component.getText();
			}
		};
	}

	/**
	 * Returns a supplier with the Date of the given {@link javax.swing.JFormattedTextField}
	 */
	public static Supplier<Date> dateSupplier( final JFormattedTextField component ) {
		return new ComponentSourceSupplier<Date,JFormattedTextField>( component ) {
			@Override
			public Date get() {
				return ( Date ) component.getValue();
			}
		};
	}

	/**
	 * Returns a supplier with the password (char[]) of the given
	 * {@link javax.swing.JPasswordField}.
	 */
	public static Supplier<char[]> passwordSupplier( final JPasswordField component ) {
		return new ComponentSourceSupplier<char[],JPasswordField>( component ) {
			@Override
			public char[] get() {
				return component.getPassword();
			}
		};
	}


	static abstract class ComponentSourceSupplier<T,C extends Component>
		implements Supplier<T> {

		protected final C component;

		ComponentSourceSupplier( C component ) {
			ValidationKit.checkNonnull( component, "component" );

			this.component = component;
		}

		@Override
		public final String toString() {
			return UIKit.describeComponent( component );
		}
	}

	static abstract class ComponentSourceBooleanSupplier<C extends Component>
		implements BooleanSupplier {

		protected final C component;

		ComponentSourceBooleanSupplier( C component ) {
			this.component = component;
		}

		@Override
		public final String toString() {
			return UIKit.describeComponent( component );
		}
	}
}
