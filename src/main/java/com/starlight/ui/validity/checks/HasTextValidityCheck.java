/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.StringKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.text.JTextComponent;
import java.util.function.Supplier;


/**
 * Makes sure a text field has (non-empty) text.
 */
public class HasTextValidityCheck implements ValidityCheck{
	private final Supplier<String> text_supplier;
	private final boolean valid_if_empty;


	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public HasTextValidityCheck( JTextComponent field ) {
		this( SwingSuppliers.textSupplier( field ) );
	}

	public HasTextValidityCheck( Supplier<String> text_supplier ) {
		this( text_supplier, false );
	}

	public HasTextValidityCheck( Supplier<String> text_supplier, boolean valid_if_empty ) {
		this.text_supplier = text_supplier;
		this.valid_if_empty = valid_if_empty;
	}


	public boolean check() {
		String text = text_supplier.get();

		if ( StringKit.isEmpty( text ) ) return valid_if_empty;
		else return !valid_if_empty;

	}

	@Override
	public String toString() {
		return "HasText: " + text_supplier;
	}
}
