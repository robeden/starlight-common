package com.starlight.ui.validity.checks;

import com.starlight.StringKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.text.JTextComponent;
import java.util.function.Supplier;


/**
 * Validates if a component contains nothing or an integer in the specified range.
 */
public class IntegerBoundsValidityCheck implements ValidityCheck {
	private final Supplier<String> text_supplier;

	private final int min;
	private final int max;



	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public IntegerBoundsValidityCheck( JTextComponent component, int min, int max ) {
		this( SwingSuppliers.textSupplier( component ), min, max );
	}

	public IntegerBoundsValidityCheck( Supplier<String> text_supplier, int min, int max ) {
		this.text_supplier = text_supplier;
		this.min = min;
		this.max = max;
	}


	@Override
	public boolean check() {
		String text = text_supplier.get();
		if ( StringKit.isEmpty( text ) ) return true;

		int value;
		try {
			value = Integer.parseInt( text );
		}
		catch( NumberFormatException ex ) {
			return false;
		}

		return value >= min && value <= max;
	}

	@Override
	public String toString() {
		return "Integer: " + text_supplier + "  " + min + " - " + max;
	}
}
