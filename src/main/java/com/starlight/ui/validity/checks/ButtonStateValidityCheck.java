/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.ui.UIKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.*;


/**
 * Checks that a toggle button has the desired state.
 *
 * @deprecated Use {@link com.starlight.ui.validity.checks.BooleanValidityCheck} and {@link SwingSuppliers#stateSupplier(javax.swing.AbstractButton)}
 */
@Deprecated
public class ButtonStateValidityCheck implements ValidityCheck {
	private JToggleButton button;
	private boolean desired_state;

	public ButtonStateValidityCheck( JToggleButton button, boolean desired_state ) {
		this.button = button;
		this.desired_state = desired_state;
	}

	public boolean check() {
		return button.isSelected() == desired_state;
	}

	@Override
	public String toString() {
		return "State: " + UIKit.describeComponent( button );
	}
}
