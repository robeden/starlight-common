package com.starlight.ui.validity.checks;

import com.starlight.StringKit;
import com.starlight.ValidationKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.text.JTextComponent;
import java.util.function.Supplier;


/**
 * Validates if a component contains nothing or an integer in the specified range.
 */
public class DoubleBoundsValidityCheck implements ValidityCheck {
	private final Supplier<String> text_supplier;

	private final double min;
	private final double max;



	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public DoubleBoundsValidityCheck( JTextComponent component, double min, double max ) {
		this( SwingSuppliers.textSupplier( component ), min, max );
	}

	public DoubleBoundsValidityCheck( Supplier<String> text_supplier, double min,
		double max ) {

		ValidationKit.checkNonnull( text_supplier, "text_supplier" );

		this.text_supplier = text_supplier;
		this.min = min;
		this.max = max;
	}


	@Override
	public boolean check() {
		String text = text_supplier.get();

		if ( StringKit.isEmpty( text ) ) return true;

		double value;
		try {
			value = Double.parseDouble( text );
		}
		catch( NumberFormatException ex ) {
			return false;
		}

		return value >= min && value <= max;
	}

	@Override
	public String toString() {
		return "Double: " + text_supplier + "  " + min + " - " + max;
	}
}
