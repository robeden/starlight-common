package com.starlight.ui.validity.checks;

import com.starlight.ui.validity.FieldValidityChecker;
import com.starlight.ui.validity.ValidityCheck;


/**
 * This check is valid if a delegate {@link com.starlight.ui.validity.FieldValidityChecker}
 * is valid.
 */
public class DelegateCheckerValidityCheck implements ValidityCheck {
	private final FieldValidityChecker delegate;

	public DelegateCheckerValidityCheck( FieldValidityChecker delegate ) {
		this.delegate = delegate;
	}

	@Override
	public boolean check() {
		return delegate.check();
	}
}
