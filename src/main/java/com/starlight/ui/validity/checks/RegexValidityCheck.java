/*
 * Copyright(c) 2005, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.StringKit;
import com.starlight.ValidationKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.*;
import java.util.function.Supplier;
import java.util.regex.Pattern;


/**
 * Check that a field contains a valid regular expression (or no text).
 */
public class RegexValidityCheck implements ValidityCheck {
	private Supplier<String> text_supplier;

	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public RegexValidityCheck( JTextField field ) {
		this( SwingSuppliers.textSupplier( field ) );
	}

	public RegexValidityCheck( Supplier<String> text_supplier ) {
		ValidationKit.checkNonnull( text_supplier, "text_supplier" );

		this.text_supplier = text_supplier;
	}

	public boolean check() {
		String text = text_supplier.get();

		if ( StringKit.isEmpty( text ) ) return true;

		try {
			Pattern.compile( text );
			return true;
		}
		catch( Exception ex ) {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Regex: " + text_supplier;
	}
}

