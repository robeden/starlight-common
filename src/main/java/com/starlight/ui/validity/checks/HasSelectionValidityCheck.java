/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.ui.UIKit;
import com.starlight.ui.validity.ValidityCheck;

import java.awt.ItemSelectable;


/**
 * Make sure a selectable item is selected.
 *
 * @deprecated Use {@link com.starlight.ui.validity.checks.ArrayLengthValidityCheck} with {@link SwingSuppliers#selectionSupplier(java.awt.ItemSelectable)}
 */
@Deprecated
public class HasSelectionValidityCheck implements ValidityCheck{
	private ItemSelectable selectable;

	public HasSelectionValidityCheck( ItemSelectable selectable ) {
		this.selectable = selectable;
	}

	public boolean check() {
		if ( selectable == null ) return true;

		Object[] objs = selectable.getSelectedObjects();
		return !( objs == null || objs.length == 0 );
	}

	@Override
	public String toString() {
		if ( selectable instanceof java.awt.Component ) {
			return "Selection: " +
				UIKit.describeComponent( ( java.awt.Component ) selectable );
		}
		else return "Selection: " + selectable;
	}
}
