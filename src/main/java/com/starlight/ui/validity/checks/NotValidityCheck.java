package com.starlight.ui.validity.checks;

import com.starlight.ValidationKit;
import com.starlight.ui.validity.ValidityCheck;


/**
 * A ValidityCheck that inverts the state of another ValidityCheck.
 */
public class NotValidityCheck implements ValidityCheck {
	private final ValidityCheck delegate;

	public NotValidityCheck( ValidityCheck delegate ) {
		ValidationKit.checkNonnull( delegate, "delegate" );
		this.delegate = delegate;
	}

	@Override
	public boolean check() {
		return !delegate.check();
	}

	@Override
	public String toString() {
		return "not " + delegate;
	}
}
