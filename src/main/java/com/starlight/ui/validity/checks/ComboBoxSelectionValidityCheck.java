/*
 * Copyright(c) 2005, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.MiscKit;
import com.starlight.ui.UIKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.*;


/**
 * Checks to see if a combo box selection matches (or does not match) a specifed value.
 *
 * @deprecated Use {@link com.starlight.ui.validity.checks.EqualsValidityCheck} with {@link SwingSuppliers#selectionSupplier(javax.swing.JComboBox)}
 */
@Deprecated
public class ComboBoxSelectionValidityCheck implements ValidityCheck {
	private final JComboBox combo_box;
	private final Object selection;
	private final boolean valid_if_equal;

	/**
	 * @param combo_box
	 * @param selection			The desired (or undesired) selection.
	 * @param valid_if_equal	If true, the check will be valid if the combo box's
	 * 							selection is equal to the given value. Otherwise, the
	 * 							check will be valid if they are not equal.
	 */
	public ComboBoxSelectionValidityCheck( JComboBox combo_box, Object selection,
		boolean valid_if_equal ) {

		this.combo_box = combo_box;
		this.selection = selection;
		this.valid_if_equal = valid_if_equal;
	}

	public boolean check() {
		if ( combo_box == null ) return true;

		Object current_selection = combo_box.getSelectedItem();

		boolean equals = MiscKit.equal( selection, current_selection );

		return valid_if_equal ? equals : !equals;
	}

	@Override
	public String toString() {
		return "Selection: " + UIKit.describeComponent( combo_box );
	}
}
