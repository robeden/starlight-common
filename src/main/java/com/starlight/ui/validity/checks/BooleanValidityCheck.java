package com.starlight.ui.validity.checks;

import com.starlight.ui.validity.ValidityCheck;

import java.util.function.BooleanSupplier;


/**
 * Checks the result of a {@link BooleanSupplier} matches a desired value.
 */
public class BooleanValidityCheck implements ValidityCheck {
	private final BooleanSupplier supplier;
	private final boolean desired_value;


	public BooleanValidityCheck( BooleanSupplier supplier ) {
		this( supplier, true );
	}

	public BooleanValidityCheck( BooleanSupplier supplier, boolean desired_value ) {
		this.supplier = supplier;
		this.desired_value = desired_value;
	}


	@Override
	public boolean check() {
		return supplier.getAsBoolean() == desired_value;
	}
}
