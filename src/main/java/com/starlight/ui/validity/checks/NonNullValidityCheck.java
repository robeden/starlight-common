package com.starlight.ui.validity.checks;

import com.starlight.ValidationKit;
import com.starlight.ui.validity.ValidityCheck;

import java.util.function.Supplier;


/**
 * Check that validates a source has a non-null value.
 */
public class NonNullValidityCheck implements ValidityCheck {
	private final Supplier<?> value_supplier;

	public NonNullValidityCheck( Supplier<?> value_supplier ) {
		ValidationKit.checkNonnull( value_supplier, "value_supplier" );

		this.value_supplier = value_supplier;
	}

	@Override
	public boolean check() {
		return value_supplier.get() != null;
	}


	@Override
	public String toString() {
		return "NonNull: " + value_supplier;
	}
}
