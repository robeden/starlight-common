/*
 * Copyright (c) 2009 Rob Eden.
 * All Rights Reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.ui.UIKit;
import com.starlight.ui.validity.ValidityCheck;
import org.jdesktop.swingx.JXDatePicker;

import java.util.Date;


/**
 * Checks to make sure a JXDatePicker has a date selected.
 *
 * @deprecated Use {@link com.starlight.ui.validity.checks.DateBoundsValidityCheck} with {@link SwingXSuppliers#dateSupplier}
 */
@Deprecated
public class DatePickerValidityCheck implements ValidityCheck {
	private final JXDatePicker picker;
	private final Date floor;
	private final Date ceil;

	/**
	 * Checks to make sure the picker has a valid (non-null) date selected.
	 */
	public DatePickerValidityCheck( JXDatePicker picker ) {
		this( picker, null, null );
	}

	/**
	 * Checks to make sure the picker has a value (non-null) date selected and that the
	 * date is between the given dates.
	 *
	 * @param floor		Lowest valid date (null for anything)
	 * @param ceil		Highest valid dat (null for anything)
	 */
	public DatePickerValidityCheck( JXDatePicker picker, Date floor, Date ceil ) {
		this.picker = picker;
		this.floor = floor;
		this.ceil = ceil;
	}


	public boolean check() {
		Date date_picked = picker.getDate();
		if ( date_picked == null ) return false;

		if ( floor != null && floor.after( date_picked ) ) return false;
		if ( ceil != null && ceil.before( date_picked ) ) return false;

		return true;
	}

	@Override
	public String toString() {
		return "DatePicker: " + UIKit.describeComponent( picker ) + "  " + floor +
			" - " + ceil;
	}
}
