/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.ui.UIKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.*;
import java.util.Date;


/**
 * Make sure a formatted text field contains a date that is either before or after another
 * date (or it's empty).
 *
 * @deprecated Use {@link com.starlight.ui.validity.checks.BoundsValidityCheck} with {@link SwingSuppliers#dateSupplier(javax.swing.JFormattedTextField)}
 */
@Deprecated
public class DateValidityCheck implements ValidityCheck {
	private JFormattedTextField field;
	private boolean before;
	private Date other_date;

	public DateValidityCheck( JFormattedTextField field, boolean before ) {
		this( field, before, new Date() );
	}

	public DateValidityCheck( JFormattedTextField field, boolean before, Date other_date ) {
		this.field = field;
		this.before = before;
		this.other_date = other_date;
	}


	public boolean check() {
		Date date = ( Date ) field.getValue();

		if ( date == null ) return true;

		if ( before ) {
			return date.before( other_date );
		}
		else {
			return date.after( other_date );
		}
	}

	@Override
	public String toString() {
		return "Date: " + UIKit.describeComponent( field );
	}
}
