package com.starlight.ui.validity.checks;

import com.starlight.ValidationKit;
import com.starlight.ui.validity.ValidityCheck;

import java.util.concurrent.Callable;


/**
 * A ValidityCheck that takes its validity from the result of a Callable. Note that a
 * null result or thrown exception from the Callable will be treated as a failure (false).
 *
 * @deprecated Use {@link com.starlight.ui.validity.checks.BooleanValidityCheck}
 */
@Deprecated
public class CallableResultValidityCheck implements ValidityCheck {
	private final Callable<Boolean> delegate;

	public CallableResultValidityCheck( Callable<Boolean> delegate ) {
		ValidationKit.checkNonnull( delegate, "delegate" );
		this.delegate = delegate;
	}


	@Override
	public boolean check() {
		try {
			Boolean result = delegate.call();
			return result != null && result.booleanValue();
		}
		catch ( Exception e ) {
			return false;
		}
	}

	@Override
	public String toString() {
		return "CallableResult: " + delegate;
	}
}
