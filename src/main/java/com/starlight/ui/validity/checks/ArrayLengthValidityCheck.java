package com.starlight.ui.validity.checks;

import com.starlight.ValidationKit;
import com.starlight.ui.validity.ValidityCheck;

import java.util.function.Supplier;


/**
 * Validates whether or not an array has entries.
 */
public class ArrayLengthValidityCheck implements ValidityCheck {
	private final Supplier<Object[]> array_supplier;
	private final boolean valid_if_non_empty;

	public ArrayLengthValidityCheck( Supplier<Object[]> array_supplier ) {
		this( array_supplier, true );
	}

	public ArrayLengthValidityCheck( Supplier<Object[]> array_supplier,
		boolean valid_if_non_empty ) {

		ValidationKit.checkNonnull( array_supplier, "array_supplier" );

		this.array_supplier = array_supplier;
		this.valid_if_non_empty = valid_if_non_empty;
	}



	@Override
	public boolean check() {
		Object[] array = array_supplier.get();

		if ( array == null || array.length == 0 ) return valid_if_non_empty;
		else return !valid_if_non_empty;
	}

	@Override
	public String toString() {
		return "ArrayLength: " + array_supplier;
	}
}
