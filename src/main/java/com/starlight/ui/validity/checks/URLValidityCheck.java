/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.StringKit;
import com.starlight.ValidationKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.text.JTextComponent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Supplier;


/**
 * Make sure a text field contains a valid URL (or nothing).
 */
public class URLValidityCheck implements ValidityCheck {
	private final Supplier<String> text_supplier;
    private final boolean require_protocol;
	
	private final String[] required_protocols;


	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public URLValidityCheck( JTextComponent field, boolean require_protocol,
		String... required_protocols ) {

		this( SwingSuppliers.textSupplier( field ), require_protocol, required_protocols );
	}

	public URLValidityCheck( Supplier<String> text_supplier, boolean require_protocol,
		String... required_protocols ) {

		ValidationKit.checkNonnull( text_supplier, "text_supplier" );

		this.text_supplier = text_supplier;
        this.require_protocol = require_protocol;
		this.required_protocols = required_protocols;
	}


	public boolean check() {
		String text = text_supplier.get();
		if ( StringKit.isEmpty( text ) ) return true;

		return createURL( text, require_protocol, required_protocols ) != null;
	}

	@Override
	public String toString() {
		return "URL: " + text_supplier;
	}

	/**
	 * Creates a URL from the contents of a text field. This uses the same logic as
	 * validation, so a URL is guaranteed to be returned if the validation check passes
	 * (and the text is non-null).
	 */
	public static URL createURL( String text, boolean require_protocol,
		String... required_protocols ) {

		if ( StringKit.isEmpty(text) ) return null;

		try {
			return checkForRequiredProtocol( new URL( text ), required_protocols );
		}
		catch( MalformedURLException ex ) {
			// Try again with "http://" on the front.
			if ( !require_protocol && !text.contains( "://" ) ) {
				text = "http://" + text;
				try {
					return checkForRequiredProtocol( new URL( text ), required_protocols );
				}
				catch( MalformedURLException exc ) {
					return null;
				}
			}

			return null;
		}
	}

	private static URL checkForRequiredProtocol( URL url, String... required_protocols ) {
		if ( required_protocols == null || required_protocols.length == 0 ) return url;

		String protocol = url.getProtocol();

		for( String check_protocol : required_protocols ) {
			// If the protocol matches, return immediately
			if ( protocol.equalsIgnoreCase( check_protocol ) ) return url;
		}

		// If it didn't match and protocols, null it out
		return null;
	} 
}
