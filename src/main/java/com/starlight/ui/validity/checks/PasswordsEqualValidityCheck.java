/*
 * Copyright(c) 2005, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.ui.validity.ValidityCheck;

import javax.swing.*;
import java.util.Arrays;
import java.util.function.Supplier;


/**
 * Makes sure two password fields contain the same value and are not empty.
 */
public class PasswordsEqualValidityCheck implements ValidityCheck {
	private Supplier<char[]> password_supplier1;
	private Supplier<char[]> password_supplier2;


	/**
	 * @deprecated Use {@link SwingSuppliers#passwordSupplier(javax.swing.JPasswordField)}
	 */
	@Deprecated
	public PasswordsEqualValidityCheck( JPasswordField field1, JPasswordField field2 ) {
		this( SwingSuppliers.passwordSupplier( field1 ),
			SwingSuppliers.passwordSupplier( field2 ) );
	}

	public PasswordsEqualValidityCheck( Supplier<char[]> password_supplier1,
		Supplier<char[]> password_supplier2 ) {

		this.password_supplier1 = password_supplier1;
		this.password_supplier2 = password_supplier2;
	}

	public boolean check() {
		char[] pass_one = password_supplier1.get();
		char[] pass_two = password_supplier2.get();

		if ( pass_two == null || pass_one == null ) return false;
		else {
			return Arrays.equals( pass_one, pass_two );
		}
	}

	@Override
	public String toString() {
		return "PasswordsEqual: " + password_supplier1 + " - " + password_supplier2;
	}
}
