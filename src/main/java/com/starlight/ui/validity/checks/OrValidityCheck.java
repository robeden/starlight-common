/*
 * Copyright(c) 2005, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.ui.validity.ValidityCheck;


/**
 * A ValidityCheck that will pass if any of the passed in checks pass.
 */
public class OrValidityCheck implements ValidityCheck {
	private ValidityCheck[] checks;

	public OrValidityCheck( ValidityCheck... checks ) {
		this.checks = checks;
	}


	public boolean check() {
		for( int i = 0; i < checks.length; i++ ) {
			if ( checks[ i ].check() ) return true;
		}

		return false;
	}


	public static ValidityCheck create( ValidityCheck... checks ) {
		return new OrValidityCheck( checks );
	}

	@Override
	public String toString() {
		String our_indent = CompoundValidityCheck.TOSTRING_INDENT.get();
		if ( our_indent == null ) our_indent = "";

		StringBuilder buf = new StringBuilder( "or\n" );

		CompoundValidityCheck.TOSTRING_INDENT.set( our_indent + "    " );
		boolean first = true;
		for( ValidityCheck check : checks ) {
			if ( first ) first = false;
			else buf.append( '\n' );

			if ( our_indent.length() != 0 ) buf.append( our_indent );
			buf.append( "  - " );

			boolean passes = check.check();
			buf.append( passes ? "(PASS) " : "(FAIL) " );

			buf.append( check );
		}

		CompoundValidityCheck.TOSTRING_INDENT.set( our_indent );

		return buf.toString();
	}
}