/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.StringKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.*;
import java.text.Format;
import java.text.ParseException;
import java.util.function.Supplier;


/**
 * Make sure a text field contains a string that can be parsed by a {@link java.text.Format}
 * object (or nothing).
 */
public class FormatValidityCheck implements ValidityCheck {
	private Supplier<String> text_supplier;
	private Format formatter;



	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public FormatValidityCheck( JTextField field, Format formatter ) {
		this( SwingSuppliers.textSupplier( field ), formatter );
	}

	public FormatValidityCheck( Supplier<String> text_supplier, Format formatter ) {
		this.text_supplier = text_supplier;
		this.formatter = formatter;
	}


	public boolean check() {
		String text = text_supplier.get();

		if ( StringKit.isEmpty( text ) ) return true;

		try {
			formatter.parseObject( text );
			return true;
		}
		catch( ParseException ex ) {
//			System.err.println( "Text: " + text );
//			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public String toString() {
		return "Format: " + text_supplier;
	}
}
