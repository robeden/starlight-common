package com.starlight.ui.validity.checks;

import com.starlight.MiscKit;
import com.starlight.ui.validity.ValidityCheck;

import java.util.function.Supplier;


/**
 * Validates that a value is equal (or not equal, if preferred) to a particular value
 * supplied by the "match_supplier".
 */
public class EqualsValidityCheck<T> implements ValidityCheck {
	private final Supplier<T> value_supplier;
	private final Supplier<T> match_supplier;
	private final boolean valid_if_equals;

	public EqualsValidityCheck( Supplier<T> value_supplier, Supplier<T> match_supplier ) {
		this( value_supplier, match_supplier, true );
	}

	public EqualsValidityCheck( Supplier<T> value_supplier, Supplier<T> match_supplier,
		boolean valid_if_equals ) {

		this.value_supplier = value_supplier;
		this.match_supplier = match_supplier;
		this.valid_if_equals = valid_if_equals;
	}


	@Override
	public boolean check() {
		T value = value_supplier.get();
		if ( value == null ) return true;

		if ( MiscKit.equal( value, match_supplier.get() ) ) return valid_if_equals;
		else return !valid_if_equals;
	}
}
