/*
 * Copyright(c) 2005, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.ui.validity.ValidityCheck;


/**
 * A ValidityCheck that combine multiple. They will be checked in order and any failure
 * will stop the checking and return invalid.
 */
public class CompoundValidityCheck implements ValidityCheck {
	static ThreadLocal<String> TOSTRING_INDENT = new ThreadLocal<>();

	private ValidityCheck[] checks;

	public CompoundValidityCheck( ValidityCheck... checks ) {
		this.checks = checks;
	}


	public boolean check() {
		for ( ValidityCheck check : checks ) {
			if ( !check.check() ) {
				return false;
			}
		}

		return true;
	}


	public static ValidityCheck create( ValidityCheck... checks ) {
		return new CompoundValidityCheck( checks );
	}

	@Override
	public String toString() {
		String our_indent = TOSTRING_INDENT.get();
		if ( our_indent == null ) our_indent = "";

		StringBuilder buf = new StringBuilder( "and\n" );

		TOSTRING_INDENT.set( our_indent + "    " );
		boolean first = true;
		for( ValidityCheck check : checks ) {
			if ( first ) first = false;
			else buf.append( '\n' );

			if ( our_indent.length() != 0 ) buf.append( our_indent );
			buf.append( "  - " );

			boolean passes = check.check();
			buf.append( passes ? "(PASS) " : "(FAIL) " );

			buf.append( check );
		}

		TOSTRING_INDENT.set( our_indent );

		return buf.toString();
	}
}