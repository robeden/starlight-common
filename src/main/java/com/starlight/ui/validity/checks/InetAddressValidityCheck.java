package com.starlight.ui.validity.checks;


import com.starlight.StringKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.text.JTextComponent;
import java.util.function.Supplier;


/**
 * Verifies the contents of a text supplier are either empty or a valid IP address. Note
 * that this does not support host names lookups (because that requires a more
 * sophisticated threading scheme due to the amount of time necessary for the lookup).
 */
public class InetAddressValidityCheck implements ValidityCheck {
	public enum Mode{ IP4_ONLY, IP6_ONLY, EITHER }


	public static Mode IP4_ONLY = Mode.IP4_ONLY;
	public static Mode IP6_ONLY = Mode.IP6_ONLY;
	public static Mode EITHER = Mode.EITHER;


	private final Supplier<String> text_supplier;
	private final Mode mode;

	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public InetAddressValidityCheck( JTextComponent component ) {
		this( component, EITHER );
	}

	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public InetAddressValidityCheck( JTextComponent component, Mode mode ) {
		this( SwingSuppliers.textSupplier( component ), mode );
	}


	public InetAddressValidityCheck( Supplier<String> text_supplier ) {
		this( text_supplier, EITHER );
	}

	public InetAddressValidityCheck( Supplier<String> text_supplier, Mode mode ) {
		this.text_supplier = text_supplier;
		this.mode = mode;
	}


	@Override
	public boolean check() {
		String text = text_supplier.get();
		return StringKit.isEmpty( text ) || isValidAddress( text, mode );
	}

	@Override
	public String toString() {
		return "InetAddress: " + text_supplier;
	}

	public static boolean isValidAddress( String host, Mode mode ) {
		if ( mode == EITHER || mode == IP4_ONLY ) {
			if ( Inet6Util.isValidIPV4Address( host ) ) return true;
		}

		if ( mode == EITHER || mode == IP6_ONLY ) {
			if ( Inet6Util.isValidIP6Address( host ) ) return true;
		}

		return false;
	}
}
