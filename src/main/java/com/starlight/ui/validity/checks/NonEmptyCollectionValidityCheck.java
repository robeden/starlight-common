package com.starlight.ui.validity.checks;

import com.starlight.ValidationKit;
import com.starlight.ui.validity.ValidityCheck;

import java.util.Collection;


/**
 * A check that verifies that a collection is not empty.
 */
public class NonEmptyCollectionValidityCheck implements ValidityCheck {
	private final Collection collection;

	public NonEmptyCollectionValidityCheck( Collection collection ) {
		ValidationKit.checkNonnull( collection, "collection" );
		this.collection = collection;
	}

	@Override
	public boolean check() {
		return !collection.isEmpty();
	}

	@Override
	public String toString() {
		return "NonEmpty: " + collection.getClass().getSimpleName();
	}
}
