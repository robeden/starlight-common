/*
 * Copyright(c) 2005, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.ui.UIKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.*;


/**
 * Checks to see if a JComponent is enabled.
 *
 * @deprecated Use {@link com.starlight.ui.validity.checks.BooleanValidityCheck} with {@link SwingSuppliers#enabledSupplier(javax.swing.JComponent)}
 */
@Deprecated
public class ComponentEnabledValidityCheck implements ValidityCheck {
	private final JComponent component;
	private final boolean valid_if_enabled;

	public ComponentEnabledValidityCheck( JComponent component, boolean valid_if_enabled ) {
		this.component = component;
		this.valid_if_enabled = valid_if_enabled;
	}

	public boolean check() {
		return valid_if_enabled == component.isEnabled();
	}

	@Override
	public String toString() {
		return ( valid_if_enabled ? "Enabled: " : "Disabled: " ) +
			UIKit.describeComponent( component );
	}
}
