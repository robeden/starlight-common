/*
 * Copyright(c) 2005, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity.checks;

import com.starlight.ValidationKit;
import com.starlight.types.EmailAddress;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.*;
import java.util.StringTokenizer;
import java.util.function.Supplier;


/**
 * Make sure a text field contains a valid e-mail address (or that it's empty).
 */
public class EmailAddressValidityCheck implements ValidityCheck {
	private final Supplier<String> text_supplier;
	private final boolean allow_multiple;



	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public EmailAddressValidityCheck( JTextField field ) {
		this( field, false );
	}

	/**
	 * @deprecated Use {@link SwingSuppliers#textSupplier(javax.swing.text.JTextComponent)}
	 */
	@Deprecated
	public EmailAddressValidityCheck( JTextField field, boolean allow_multiple ) {
		this( SwingSuppliers.textSupplier( field ), allow_multiple );
	}


	public EmailAddressValidityCheck( Supplier<String> text_supplier ) {
		this ( text_supplier, false );
	}

	public EmailAddressValidityCheck( Supplier<String> text_supplier,
		boolean allow_multiple ) {

		ValidationKit.checkNonnull( text_supplier, "text_supplier" );

		this.text_supplier = text_supplier;
		this.allow_multiple = allow_multiple;
	}


	public boolean check() {
		String text = text_supplier.get();
		if ( text == null || text.trim().equals( "" ) ) return true;

		if ( allow_multiple &&
			( text.indexOf( ';' ) >= 0 || text.indexOf( ' ' ) >= 0 ||
			text.indexOf( ',' ) >= 0 ) ) {

			StringTokenizer tok = new StringTokenizer( text, " ;," );
			while( tok.hasMoreTokens() ) {
				if ( !addressMatches( tok.nextToken() ) ) return false;
			}
			return true;
		}
		else return addressMatches( text );
	}


	@Override
	public String toString() {
		return "EmailAddress: " + text_supplier;
	}

	private boolean addressMatches( String text ) {
		return EmailAddress.isValidAddress( text );
	}
}
