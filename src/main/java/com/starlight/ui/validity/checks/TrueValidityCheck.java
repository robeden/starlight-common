package com.starlight.ui.validity.checks;

import com.starlight.ui.validity.ValidityCheck;


/**
 * A {@link com.starlight.ui.validity.ValidityCheck} that always passes.
 */
public class TrueValidityCheck implements ValidityCheck {
	@Override
	public boolean check() {
		return true;
	}
}
