package com.starlight.ui.validity.checks;

import com.starlight.ValidationKit;
import com.starlight.ui.UIKit;
import com.starlight.ui.validity.ValidityCheck;

import javax.swing.*;


/**
 * {@link ValidityCheck} to ensure a node in a tree is selected.
 *
 * @deprecated Use {@link com.starlight.ui.validity.checks.BoundsValidityCheck} with {@link SwingSuppliers#selectionCountSupplier(javax.swing.JTree)}
 */
@Deprecated
public class TreeHasSelectionValidityCheck implements ValidityCheck {
	private final JTree tree;

	public TreeHasSelectionValidityCheck( JTree tree ) {
		ValidationKit.checkNonnull( tree, "tree" );
		this.tree = tree;
	}

	@Override
	public boolean check() {
		return tree.getSelectionCount() > 0;
	}

	@Override
	public String toString() {
		return "Selection: " + UIKit.describeComponent( tree );
	}
}
