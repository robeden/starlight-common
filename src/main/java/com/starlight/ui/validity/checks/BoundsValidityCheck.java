/*
 * Copyright (c) 2009 Rob Eden.
 * All Rights Reserved.
 */
package com.starlight.ui.validity.checks;

import ca.odell.glazedlists.GlazedLists;
import com.starlight.ValidationKit;
import com.starlight.ui.validity.ValidityCheck;

import java.util.Comparator;
import java.util.function.Supplier;


/**
 * Checks to make sure a JXDatePicker has a date selected.
 */
public class BoundsValidityCheck<T> implements ValidityCheck {
	private final Supplier<T> value_supplier;
	private final T min;
	private final T max;
	private final Comparator<T> comparator;

	/**
	 * Checks to make sure the picker has a valid (non-null) date selected.
	 */
	public BoundsValidityCheck( Supplier<T> value_supplier ) {
		this( value_supplier, null, null );
	}

	/**
	 * Checks to make sure the supplier has a value between the given min and max. Null
	 * values are treated as valid.
	 *
	 * @param min		Lowest valid value (null for anything)
	 * @param max		Highest valid value (null for anything)
	 */
	public BoundsValidityCheck( Supplier<T> value_supplier, T min, T max ) {
		this( value_supplier, min, max, null );
	}

	/**
	 * Checks to make sure the supplier has a value between the given min and max. Null
	 * values are treated as valid.
	 *
	 * @param min		Lowest valid value (null for anything)
	 * @param max		Highest valid value (null for anything)
	 */
	public BoundsValidityCheck( Supplier<T> value_supplier, T min, T max,
		Comparator<T> comparator ) {

		ValidationKit.checkNonnull( value_supplier, "value_supplier" );

		if ( comparator == null ) {
			//noinspection unchecked,RedundantCast
			comparator = ( Comparator<T> ) GlazedLists.comparableComparator();
		}

		this.value_supplier = value_supplier;
		this.min = min;
		this.max = max;
		this.comparator = comparator;
	}


	public boolean check() {
		T value = value_supplier.get();
		if ( value == null ) return true;

		if ( min != null && comparator.compare( value, min ) < 0 ) return false;
		if ( max != null && comparator.compare( value, max ) > 0 ) return false;

		return true;
	}

	@Override
	public String toString() {
		return "Bounds: " + value_supplier + "  " + min + " - " + max;
	}
}
