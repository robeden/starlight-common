/*
 * Copyright(c) 2007, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity;

import com.starlight.ValidationKit;

import javax.swing.*;


/**
 * Disable a component when run.
 */
public class DisableComponentAction implements Runnable {
	private final JComponent component;

	public DisableComponentAction( JComponent component ) {
		this.component = component;

		ValidationKit.checkNonnull( component, "Component" );
	}


	public void run() {
		component.setEnabled( false );
	}
}