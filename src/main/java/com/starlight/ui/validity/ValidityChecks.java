package com.starlight.ui.validity;

import com.starlight.ui.validity.checks.*;

import java.util.Comparator;
import java.util.function.Supplier;


/**
 * Static utility functions for building {@link ValidityCheck} chains.
 */
public class ValidityChecks {
	/**
	 * Return a single check that requires all delegate checks to be valid.
	 * Equivalent to <code>CompoundValidityCheck.create( checks )</code>.
	 *
	 * @param checks    Delegate checks.
	 */
	public static ValidityCheck and( ValidityCheck... checks ) {
		return CompoundValidityCheck.create( checks );
	}

	/**
	 * Return a single check that requires any delegate check to be valid.
	 * Equivalent to <code>OrValidityCheck.create( checks )</code>.
	 *
	 * @param checks    Delegate checks.
	 */
	public static ValidityCheck or( ValidityCheck... checks ) {
		return OrValidityCheck.create( checks );
	}

	/**
	 * Return a check with inverted logic from the delegate check.
	 * Equivalent to <code>new NotValidityCheck( check )</code>.
	 *
	 * @param check     Delegate check.
	 */
	public static ValidityCheck not( ValidityCheck check ) {
		return new NotValidityCheck( check );
	}

	/**
	 * Return a check that the contents of the given supplier are equal or not equal
	 * to the value supplied by a "match_supplier". The check also passes if the value
	 * supplied by "value_supplier" is null.
	 */
	public static <T> ValidityCheck equal( Supplier<T> value_supplier,
		Supplier<T> match_supplier, boolean valid_if_equal ) {

		return new EqualsValidityCheck<>( value_supplier, match_supplier,
			valid_if_equal );
	}

	/**
	 * Return a check that validates the value of a supplier is not null.
	 */
	public static ValidityCheck nonNull( Supplier<?> value_supplier ) {
		return new NonNullValidityCheck( value_supplier );
	}


	/**
	 * Returns a check that ensure a value is within the given bounds.
	 *
	 * @param min       The min bounds, or null for none.
	 * @param max       The max bounds, or null for none.
	 */
	public static <T extends Comparable> ValidityCheck bounds( Supplier<T> value_supplier,
		T min, T max ) {

		return new BoundsValidityCheck<>( value_supplier, min, max );
	}

	/**
	 * Returns a check that ensure a value is within the given bounds.
	 *
	 * @param min       The min bounds, or null for none.
	 * @param max       The max bounds, or null for none.
	 */
	public static <T> ValidityCheck bounds( Supplier<T> value_supplier,
		T min, T max, Comparator<T> comparator ) {

		return new BoundsValidityCheck<>( value_supplier, min, max, comparator );
	}


	/**
	 * Returns a check that ensures the given supplier has non-empty text.
	 */
	public static ValidityCheck hasText( Supplier<String> text_supplier ) {
		return new HasTextValidityCheck( text_supplier, false );
	}

	/**
	 * Returns a check that ensures the given supplier has empty text.
	 */
	public static ValidityCheck emptyText( Supplier<String> text_supplier ) {
		return new HasTextValidityCheck( text_supplier, true );
	}


	/**
	 * Returns a check that ensure the give value supplier contains a parseable
	 * InetAddress.
	 */
	public static ValidityCheck inetAddress( Supplier<String> text_supplier ) {
		return new InetAddressValidityCheck( text_supplier );
	}

	/**
	 * Returns a check that ensure the give value supplier contains a parseable
	 * email address.
	 */
	public static ValidityCheck emailAddress( Supplier<String> text_supplier ) {
		return new EmailAddressValidityCheck( text_supplier );
	}


	/**
	 * Returns a check that always fails.
	 */
	public static ValidityCheck fail() {
		return new BooleanValidityCheck( () -> false );
	}

	/**
	 * Returns a check that always fails.
	 */
	public static ValidityCheck pass() {
		return new BooleanValidityCheck( () -> true );
	}
}
