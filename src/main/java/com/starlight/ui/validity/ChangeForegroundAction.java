package com.starlight.ui.validity;

import com.starlight.ValidationKit;
import com.starlight.ui.UIKit;

import javax.swing.*;
import java.awt.Color;


/**
 *
 */
public class ChangeForegroundAction implements Runnable {
	/**
	 * Create and add a check that changes the label color when there is an error.
	 */
	public static void createStandardErrorAction( FieldValidityChecker checker,
		JComponent label, ValidityCheck check ) {

		checker.addCheck( check,
			new ChangeForegroundAction( label, UIKit.getNormalLabelColor() ),
			new ChangeForegroundAction( label, UIKit.getErrorLabelColor() ) );

		if ( ChangeLabelAction.AVOID_SUBSTANCE_COLOR_CORRECTION ) {
			label.putClientProperty( "substancelaf.internal.textUtilities.enforceFgColor",
				Boolean.TRUE );
		}
	}


	private final JComponent component;
	private final Color color;


	public ChangeForegroundAction( JComponent component, Color color ) {
		ValidationKit.checkNonnull( component, "component" );
		ValidationKit.checkNonnull( color, "color" );

		this.component = component;
		this.color = color;
	}

	public void run() {
		component.setForeground( color );
	}
}
