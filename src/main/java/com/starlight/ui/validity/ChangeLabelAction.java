/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui.validity;

import com.starlight.ui.UIKit;

import javax.swing.*;
import java.awt.Color;


public class ChangeLabelAction implements Runnable {
	static final boolean AVOID_SUBSTANCE_COLOR_CORRECTION = System.getProperty(
		"starlight.validity.avoid_substance_color_correction" ) != null;


	/**
	 * Create and add a check that changes the label color when there is an error.
	 */
	public static void createStandardErrorLabel( FieldValidityChecker checker,
		JLabel label, ValidityCheck check ) {

		checker.addCheck( check,
			new ChangeLabelAction( label, UIKit.getNormalLabelColor() ),
			new ChangeLabelAction( label, UIKit.getErrorLabelColor() ) );

		if ( AVOID_SUBSTANCE_COLOR_CORRECTION ) {
			label.putClientProperty( "substancelaf.internal.textUtilities.enforceFgColor",
				Boolean.TRUE );
		}
	}


	private final JLabel label;
	private final String text;
	private final Color color;
	private final String tooltip;


	public ChangeLabelAction( JLabel label, Color color ) {
		this( label, null, color, null );
	}

	public ChangeLabelAction( JLabel label, String text, Color color, String tooltip ) {
		if ( label == null ) throw new IllegalArgumentException( "Label cannot be null." );

		this.label = label;
		this.text = text;
		this.color = color;
		this.tooltip = tooltip;
	}

	public void run() {
		if ( text != null ) label.setText( text.equals( "" ) ? null : text );
		if ( color != null ) label.setForeground( color );
		if ( tooltip != null ) label.setToolTipText( tooltip.equals( "" ) ? null : tooltip );
	}
}
