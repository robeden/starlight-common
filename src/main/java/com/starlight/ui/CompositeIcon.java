/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */

package com.starlight.ui;

import javax.swing.*;
import java.awt.*;


/**
 * This will create an Icon that is made of two icons layered over top of each
 * other.
 */
public class CompositeIcon implements Icon {

	public static final int LEFT = 0;
	public static final int RIGHT = 1;
	public static final int TOP = 2;
	public static final int BOTTOM = 3;
	public static final int CENTER = 4;

	private Icon fore;
	private Icon back;

	private int halign = LEFT;
	private int valign = TOP;

	public CompositeIcon( Icon foreground, Icon background ) {
		this( foreground, background, LEFT, TOP );
	}

	public CompositeIcon( Icon foreground, Icon background, int halign, int valign ) {
		this.fore = foreground;
		this.back = background;

		setHorizontalAlignment( halign );
		setVerticalAlignment( valign );
	}

	/**
	 * Sets the horizontal position of the foreground icon on the background icon.
	 * <p/>
	 * NOTE: Changes don't take affect until the icon is repainted.
	 *
	 * @param align LEFT, CENTER, or RIGHT
	 */
	public void setHorizontalAlignment( int align ) {
		if ( align != LEFT && align != CENTER && align != RIGHT )
			throw new IllegalArgumentException(
				"Horizontal Alignment must be LEFT, CENTER, or RIGHT" );

		this.halign = align;
	}

	/**
	 * Sets the vertical position of the foreground icon on the background icon.
	 * <p/>
	 * NOTE: Changes don't take affect until the icon is repainted.
	 *
	 * @param align TOP, CENTER, or BOTTOM
	 */
	public void setVerticalAlignment( int align ) {
		if ( align != TOP && align != CENTER && align != BOTTOM )
			throw new IllegalArgumentException(
				"Vertical Alignment must be TOP, CENTER, or BOTTOM" );

		this.valign = align;
	}

	public int getIconHeight() {
		return Math.max( fore.getIconHeight(), back.getIconHeight() );
	}

	public int getIconWidth() {
		return Math.max( fore.getIconWidth(), back.getIconWidth() );
	}

	public void paintIcon( Component c, Graphics g, int x, int y ) {
		int forex = x;
		int forey = y;
		int backx = x;
		int backy = y;

		if ( back.getIconWidth() > fore.getIconWidth() ) {
			if ( halign == CENTER ) {
				float bmid = ( ( float ) back.getIconWidth() ) / ( 2f );
				float fhalf = ( ( float ) fore.getIconWidth() ) / ( 2f );
				forex = ( int ) ( bmid - fhalf );
			}
			else if ( halign == RIGHT ) {
				forex = back.getIconWidth() - fore.getIconWidth();
			}
		}
		else if ( back.getIconWidth() < fore.getIconWidth() ) {
			if ( halign == CENTER ) {
				float bmid = ( ( float ) fore.getIconWidth() ) / ( 2f );
				float fhalf = ( ( float ) back.getIconWidth() ) / ( 2f );
				backx = ( int ) ( bmid - fhalf );
			}
			else if ( halign == RIGHT ) {
				backx = fore.getIconWidth() - back.getIconWidth();
			}
		}

		if ( back.getIconHeight() > fore.getIconHeight() ) {
			if ( valign == CENTER ) {
				float bmid = ( ( float ) back.getIconHeight() ) / ( 2f );
				float fhalf = ( ( float ) fore.getIconHeight() ) / ( 2f );
				forey = ( int ) ( bmid - fhalf );
			}
			else if ( valign == BOTTOM ) {
				forey = back.getIconHeight() - fore.getIconHeight();
			}
		}
		else if ( back.getIconHeight() < fore.getIconHeight() ) {
			if ( valign == CENTER ) {
				float bmid = ( ( float ) fore.getIconHeight() ) / ( 2f );
				float fhalf = ( ( float ) back.getIconHeight() ) / ( 2f );
				backy = ( int ) ( bmid - fhalf );
			}
			else if ( valign == BOTTOM ) {
				backy = fore.getIconHeight() - back.getIconHeight();
			}
		}

		back.paintIcon( c, g, backx, backy );
		fore.paintIcon( c, g, forex, forey );
	}
}
