/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui;

import javax.swing.*;
import java.awt.*;


/**
 * <pre>
 * This JLabel wraps the set Text in html tags makes some decisions based on the
 * text set.  It will...
 *
 * 1.  Replace \n with &lt;br&gt;
 * 2.  Figure out the preferred height (after wrapping)
 *
 * </pre>
 */
public class MultiLineLabel extends JLabel {
	public MultiLineLabel() {};

	public MultiLineLabel(String text) {
		super();
		setText(text);
	}

	public String getOriginalText() {
		String text = super.getText();
		text = text.replaceAll("<html>", "");
		text = text.replaceAll("</html>", "");
		text = text.replaceAll("<br>", "\n");
		return text;
	}

	public void setSize(int width, int height) {
		super.setSize(width, height);
		//System.out.println("SIZING: " + width);
		redoPreferredHeight(width);
	}

	public void redoPreferredHeight(int width) {
		if (getFont() != null) {
			FontMetrics fm = getFontMetrics(getFont());
			String text = getOriginalText();
			int lines = 1;
			int tot_width = 0;
			int word_width = 0;
			int line_width = 0;
			int max_line_width = -1;
			for (int i = 0; i < text.length(); i++) {
				char c = text.charAt(i);
				if (c == '\n') {
					lines++;
					if (line_width >= max_line_width) {
						max_line_width = line_width;
					}
					line_width = 0;
					tot_width = 0;
					word_width = 0;
				} else {
					tot_width += fm.charWidth(c);
					line_width += fm.charWidth(c);

					if (c != ' ') word_width += fm.charWidth(c);
					else word_width = 0;

					if (tot_width > width && width != -1) {
						lines++;
						tot_width = word_width;
					}
				}
			}
			if (max_line_width == -1) max_line_width = line_width;
			if (width == -1) width = max_line_width;

//			Insets border_insets = getBorder().getBorderInsets( )

			Dimension d = new Dimension(width, ( lines * fm.getHeight() ) + fm.getDescent() +
				fm.getAscent() );
			setPreferredSize(d);
		}
	}

	public void setText(String text) {
		StringBuffer b = new StringBuffer();
		b.append("<html>").append(text).append("</html>");
		super.setText(b.toString().replaceAll("\\n", "<br>"));

		redoPreferredHeight((int)super.getPreferredSize().getWidth());
	}

	public Dimension getPreferredSize() {
		redoPreferredHeight((int)super.getPreferredSize().getWidth());
		return super.getPreferredSize();
	}

	public final static void main(String[] args) {
		JFrame test = new JFrame("Test");
		test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel label = new MultiLineLabel();

		label.setText( "ok, that one worked, this then is \n1\n2\n longer\n, " +
			"yhahahah , this is an even longer test to see if the darn multilinelabel " +
			"will wrap correctly" );
//		label.setText( "test sing line" );

		test.getContentPane().setLayout(new BorderLayout(0, 0));
		test.getContentPane().add(label, BorderLayout.NORTH);
		test.pack();
		test.setVisible(true);
	}
}
