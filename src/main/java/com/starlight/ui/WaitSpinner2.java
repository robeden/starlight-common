/*
 * Copyright(c) 2007, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.icon.EmptyIcon;
import org.jdesktop.swingx.painter.BusyPainter;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;


/**
 *
 */
public class WaitSpinner2 extends JComponent {
	private final JXBusyLabel label;

	private final BusyPainter painter;


	public WaitSpinner2() {
		this( true );
	}

	public WaitSpinner2( boolean initally_running ) {
		Dimension size = getPreferredSize();
		label = new JXBusyLabel( size );
		painter = createBusyPainter();
		label.setPreferredSize( size );
		label.setIcon( new EmptyIcon( size.width, size.height ) );
		label.setBusyPainter( painter );
		label.setBusy( initally_running );

		setLayout( new BorderLayout( 0, 0 ) );

		add( label, BorderLayout.CENTER );
	}


	protected BusyPainter createBusyPainter() {
		BusyPainter painter = new BusyPainter(
			new RoundRectangle2D.Float( 0, 0, 2.0f, 4.5f, 10.0f, 10.0f ),
			new Ellipse2D.Float( 3.0f, 3.0f, 14.0f, 14.0f ) );
		painter.setTrailLength( 4 );
		painter.setPoints( 8 );
		painter.setFrame( 4 );
		painter.setHighlightColor( new Color( 0, 112, 192 ) );
		return painter;
	}


	@Override
	public Dimension getMinimumSize() {
		return label.getMinimumSize();
	}

	@Override
	public void setPreferredSize( Dimension preferredSize ) {
		label.setPreferredSize( preferredSize );
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension( 20, 20 );
	}

	@Override
	public void setMaximumSize( Dimension maximumSize ) {
		label.setMaximumSize( maximumSize );
	}

	@Override
	public Dimension getMaximumSize() {
		return label.getMaximumSize();
	}

	@Override
	public void setMinimumSize( Dimension minimumSize ) {
		label.setMinimumSize( minimumSize );
	}


	public void start() {
		painter.setVisible( true );
		label.setBusy( true );
	}

	public void stop() {
		label.setBusy( false );
		painter.setVisible( false );
	}

	public boolean isRunning() {
		return label.isBusy();
	}


	public static void main( String args[] ) throws Exception {
		JFrame frame = new JFrame( "Test" );
		frame.getContentPane().setBackground( Color.white );
		frame.getContentPane().setLayout( new FlowLayout( FlowLayout.LEFT ) );
		final WaitSpinner2 spinner = new WaitSpinner2();
		frame.getContentPane().add( spinner );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

		JButton but = new JButton( "toggle" );
		but.addActionListener( new ActionListener() {
				public void actionPerformed( ActionEvent e ) {
					if ( spinner.isRunning() ) spinner.stop();
					else spinner.start();
				}
			} );
		frame.getContentPane().add( but );

		frame.pack();
		frame.setVisible( true );
	}
}
