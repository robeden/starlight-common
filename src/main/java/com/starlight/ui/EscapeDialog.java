/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * A dialog that is closed when the user hits the "escape" key when the dialog is in focus.
 */
public class EscapeDialog extends JDialog {
	public EscapeDialog() throws HeadlessException {
	}

	public EscapeDialog( Dialog owner ) throws HeadlessException {
		super( owner );
	}

	public EscapeDialog( Dialog owner, boolean modal ) throws HeadlessException {
		super( owner, modal );
	}

	public EscapeDialog( Dialog owner, String title ) throws HeadlessException {
		super( owner, title );
	}

	public EscapeDialog( Dialog owner, String title, boolean modal )
		throws HeadlessException {
		super( owner, title, modal );
	}

	public EscapeDialog( Dialog owner, String title, boolean modal,
		GraphicsConfiguration gc ) throws HeadlessException {
		super( owner, title, modal, gc );
	}

	public EscapeDialog( Frame owner ) throws HeadlessException {
		super( owner );
	}

	public EscapeDialog( Frame owner, boolean modal ) throws HeadlessException {
		super( owner, modal );
	}

	public EscapeDialog( Frame owner, String title ) throws HeadlessException {
		super( owner, title );
	}

	public EscapeDialog( Frame owner, String title, boolean modal )
		throws HeadlessException {
		super( owner, title, modal );
	}

	public EscapeDialog( Frame owner, String title, boolean modal,
		GraphicsConfiguration gc ) {
		super( owner, title, modal, gc );
	}


	protected JRootPane createRootPane() {
		assert SwingUtilities.isEventDispatchThread();
		
		KeyStroke stroke = KeyStroke.getKeyStroke( KeyEvent.VK_ESCAPE, 0 );

		JRootPane rootPane = new JRootPane();

		ActionListener actionListener = new ActionListener() {
			public void actionPerformed( ActionEvent actionEvent ) {
				setVisible( false );
			}
		};

		rootPane.registerKeyboardAction( actionListener, stroke,
			JComponent.WHEN_IN_FOCUSED_WINDOW );

		return rootPane;
	}
}
