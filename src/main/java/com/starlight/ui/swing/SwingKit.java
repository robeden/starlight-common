/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */

package com.starlight.ui.swing;

import javax.swing.*;
import java.awt.BorderLayout;


/**
 * Static functions for working with Swing.
 */
public final class SwingKit {
	/**
	 * Hidden constructor
	 */
	private SwingKit() {
	}


	/**
	 * This is used in testing of new components to display a frame with the component
	 * inside. The frame will cause the VM to exit when closed. Basically it does
	 * everything you need in the main method to display the component.
	 */
	public static void testComponent( JComponent component ) {
		JFrame test = new JFrame( "Test: " + component.getClass().getName() );
		test.getContentPane().setLayout( new BorderLayout() );
		test.getContentPane().add( component, BorderLayout.CENTER );
		test.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		test.pack();
		System.out.println( "Frame size: " + test.getSize() );
		test.setVisible( true );
	}
}
