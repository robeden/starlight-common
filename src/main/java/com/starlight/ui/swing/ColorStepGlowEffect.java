package com.starlight.ui.swing;

import org.jdesktop.swingx.painter.effects.AbstractAreaEffect;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.Point2D;


/**
 *
 */
public class ColorStepGlowEffect extends AbstractAreaEffect {
	private final Color[] color_steps;

	public ColorStepGlowEffect( Color[] color_steps ) {
		setBrushColor( Color.WHITE );
		setBrushSteps( color_steps.length );
		setEffectWidth( 10 );
		setShouldFillShape( false );
		setOffset( new Point( 0, 0 ) );

		this.color_steps = color_steps;
	}

	// This method is mostly from AbstractAreaEffect, which is copyright Sun Microsystems.
	// Changes were made to tie the number of steps to a color array. Original license is:
	//
	// Copyright 2006 Sun Microsystems, Inc., 4150 Network Circle,
	// Santa Clara, California 95054, U.S.A. All rights reserved.
	//
	// This library is free software; you can redistribute it and/or
	// modify it under the terms of the GNU Lesser General Public
	// License as published by the Free Software Foundation; either
	// version 2.1 of the License, or (at your option) any later version.
	//
	// This library is distributed in the hope that it will be useful,
	// but WITHOUT ANY WARRANTY; without even the implied warranty of
	// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	// Lesser General Public License for more details.
	//
	// You should have received a copy of the GNU Lesser General Public
	// License along with this library; if not, write to the Free Software
	// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	@Override
	protected void paintBorderGlow( Graphics2D g2, Shape clipShape, int width,
		int height ) {

		int steps = color_steps.length;
		float brushAlpha = 1f / steps;

		boolean inside = isRenderInsideShape();

		g2.setPaint( getBrushColor() );

		Point2D offset = getOffset();
		g2.translate( offset.getX(), offset.getY() );

		int effectWidth = getEffectWidth();

		if ( isShouldFillShape() ) {
			// set the inside/outside mode
			if ( inside ) {
				g2.setComposite(
					AlphaComposite.getInstance( AlphaComposite.SRC_ATOP, 1f ) );
				Area a1 = new Area( new Rectangle(
					( int ) -offset.getX() - 20,
					( int ) -offset.getY() - 20,
					width + 40, height + 40 ) );
				Area a2 = new Area( clipShape );
				a1.subtract( a2 );
				g2.fill( a1 );
			}
			else {
				g2.setComposite(
					AlphaComposite.getInstance( AlphaComposite.DST_OVER, 1f ) );
				g2.fill( clipShape );
			}

		}

		g2.setComposite(
			AlphaComposite.getInstance( AlphaComposite.DST_OVER, brushAlpha ) );

		// draw the effect
		int index = 0;
		for ( float i = 0; i < steps; i = i + 1f ) {
			float brushWidth = i * effectWidth / steps;
			g2.setPaint( color_steps[ index++ ] );
			g2.setStroke( new BasicStroke( brushWidth,
				BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND ) );
			g2.draw( clipShape );
		}
		g2.translate( -offset.getX(), -offset.getY() );
	}
}
