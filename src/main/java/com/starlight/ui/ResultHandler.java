/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */

package com.starlight.ui;

/**
* Simple interface for handling a normal result or an error.
*/
public interface ResultHandler<E> {
	public void run( E result );
	public void error( Throwable t );
}
