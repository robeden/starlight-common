/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */
package com.starlight.ui;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListDataListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.ArrayList;


/**
 * This is a general component to display a list with "Add", "Remove" and possibly "Edit",
 * "Move Up" and "Move Down" buttons.
 *
 * @author reden
 */
public class AddRemovePanel extends JPanel implements ActionListener {
	private AddRemovePanelListener listener;

	private JList list;
	private MyListModel model;


	/**
	 * Create the component.
	 *
	 * @param items				The initial items in the list.
	 * @param allow_edit		Determines whether the "Edit" button is displayed.
	 * @param position_matters	Determines whether the "Move Up" and "Move Down" buttons
	 * 							are displayed.
	 * @param list_renderer		Renders the components in the list.
	 * @param listener			Notified of actions taken by the user.
	 */
	public AddRemovePanel( Object[] items, boolean allow_edit, boolean position_matters,
		ListCellRenderer list_renderer, AddRemovePanelListener listener ) {

		this( items, allow_edit, position_matters, list_renderer, listener, true );
	}

	/**
	 * Create the component.
	 *
	 * @param items				The initial items in the list.
	 * @param allow_edit		Determines whether the "Edit" button is displayed.
	 * @param position_matters	Determines whether the "Move Up" and "Move Down" buttons
	 * 							are displayed.
	 * @param list_renderer		Renders the components in the list.
	 * @param listener			Notified of actions taken by the user.
	 * @param use_small_buttons	If true, the buttons will be smaller than normal.
	 */
	public AddRemovePanel( Object[] items, boolean allow_edit, boolean position_matters,
		ListCellRenderer list_renderer, AddRemovePanelListener listener,
		boolean use_small_buttons ) {

		this.listener = listener;

		if ( listener == null ) throw new IllegalArgumentException(
			"Action listener cannot be null." );

		init( items, allow_edit, position_matters, list_renderer, use_small_buttons );
	}


	/**
	 * Remove all data from list.
	 */
	public void clear() {
		model.clear();
	}


	/**
	 * Add the given data to the list.
	 */
	public void addAll( Object[] data ) {
		if ( data == null || data.length == 0 ) return;

		model.addAll( data );
	}


	/**
	 * Get all the items in the list.
	 */
	public java.util.List getItems() {
		int size = model.getSize();

		if ( size == 0 ) return new LinkedList();

		java.util.List list = new ArrayList( size );
		for( int i = 0; i < size; i++ ) {
			list.add( model.getElementAt( i ) );
		}
		return list;
	}


	public void addListDataListener( ListDataListener listener ) {
		model.addListDataListener( listener );
	}

	public void removeListDataListener( ListDataListener listener ) {
		model.removeListDataListener( listener );
	}


	private void init( Object[] items, boolean allow_edit, boolean position_matters,
		ListCellRenderer list_renderer, boolean use_small_buttons ) {

		setLayout( new BorderLayout() );

		model = new MyListModel( items );

		list = new JList( model );
		list.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
		if ( list_renderer != null ) {
			list.setCellRenderer( list_renderer );
		}
		JScrollPane scroller = new JScrollPane( list );
		add( scroller, BorderLayout.CENTER );

		JPanel button_panel = new JPanel( new GridBagLayout() );

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridy = 0;

		ArrayList enable_on_select = new ArrayList( 4 );

		JButton button = new JButton( "Add" );
		if ( use_small_buttons ) createSmallBorder( button );
		button.setActionCommand( "ADD" );
		button.addActionListener( this );
		button_panel.add( button, gbc );
		gbc.gridy++;

		final JButton add_button = button;

		if ( allow_edit ) {
			button = new JButton( "Edit" );
			if ( use_small_buttons ) createSmallBorder( button );
			button.setActionCommand( "EDIT" );
			button.addActionListener( this );
			button_panel.add( button, gbc );
			enable_on_select.add( button );
			gbc.gridy++;
		}

		button = new JButton( "Remove" );
		if ( use_small_buttons ) createSmallBorder( button );
		button.setActionCommand( "REMOVE" );
		button.addActionListener( this );
		button_panel.add( button, gbc );
		enable_on_select.add( button );
		gbc.gridy++;

		if ( position_matters ) {
			button_panel.add( UIKit.createFiller( 1, 7 ), gbc );
			gbc.gridy++;

			button = new JButton( "Move Up" );
			if ( use_small_buttons ) createSmallBorder( button );
			button.setActionCommand( "MOVE UP" );
			button.addActionListener( this );
			button_panel.add( button, gbc );
			enable_on_select.add( button );
			gbc.gridy++;

			button = new JButton( "Move Down" );
			if ( use_small_buttons ) createSmallBorder( button );
			button.setActionCommand( "MOVE DOWN" );
			button.addActionListener( this );
			button_panel.add( button, gbc );
			enable_on_select.add( button );
			gbc.gridy++;
		}

		JPanel filler = new JPanel();
		gbc.fill = GridBagConstraints.VERTICAL;
		gbc.weighty = 100.0;
		button_panel.add( filler, gbc );
		gbc.gridy++;

		final JButton[] enable_on_select_buttons =
			( JButton[] ) enable_on_select.toArray( new JButton[ enable_on_select.size() ] );
		list.addListSelectionListener( new ListSelectionListener() {
			public void valueChanged( ListSelectionEvent e ) {
				int index = list.getSelectedIndex();

				for( int i = 0; i < enable_on_select_buttons.length; i++ ) {
					enable_on_select_buttons[ i ].setEnabled( index >= 0 );
				}

				if ( index < 0 ) add_button.requestFocus();
			}
		} );

		add( button_panel, BorderLayout.LINE_END );

		button_panel.doLayout();

		int button_panel_height = button_panel.getPreferredSize().height;

		Dimension size = new Dimension( list.getPreferredSize().width,
			button_panel_height );
//		list.setPreferredSize( size );
		scroller.setPreferredSize( size );

		for( int i = 0; i < enable_on_select_buttons.length; i++ ) {
			enable_on_select_buttons[ i ].setEnabled( false );
		}
	}

	public void actionPerformed( ActionEvent e ) {
		String command = e.getActionCommand();

		int index = list.getSelectedIndex();

		if ( command.equals( "ADD" ) ) {
			Object new_item = listener.itemAdd();
			if ( new_item != null ) model.add( new_item );
		}
		else if ( command.equals( "REMOVE" ) ) {
			if ( index < 0 ) return;

			Object item = model.getElementAt( index );
			if ( listener.itemRemoved( item ) ) {
				model.remove( index );

				// Do the right thing with the selection
				if ( model.getSize() == 0 ) {
					// do nothing with the selection
				}
				else if ( index + 1 <= model.getSize() ) {
					list.setSelectedIndex( index );
				}
				else {
					list.setSelectedIndex( index - 1 );
				}
			}
		}
		else if ( command.equals( "EDIT" ) ) {
			if ( index < 0 ) return;

			Object item = model.getElementAt( index );
			if ( listener.itemEdit( item ) ) {
				model.edit( index );
			}

		}
		else if ( command.equals( "MOVE UP" ) ) {
			if ( index < 0 ) return;

			if ( listener.orderChange( model.getData(), model.getElementAt( index ),
				index, index - 1 ) ) {

				model.moveUp( index );
				list.setSelectedIndex( index - 1 );
			}

		}
		else if ( command.equals( "MOVE DOWN" ) ) {
			if ( index >= model.getSize() - 1 ) return;

			if ( listener.orderChange( model.getData(), model.getElementAt( index ),
				index, index + 1 ) ) {

				model.moveDown( index );
				list.setSelectedIndex( index + 1 );
			}
		}
	}


	private void createSmallBorder( JButton button ) {
		// TODO: implement?
//		button.setBorder(
//			com.incors.plaf.alloy.AlloyCommonBorderFactory.createSmallButtonBorder() );
	}


	public static interface AddRemovePanelListener {
		/**
		 * Indicates that the user has requested to remove the given item.
		 *
		 * @return		True if the item should be removed.
		 */
		public boolean itemRemoved( Object obj );

		/**
		 * Indicates that the user has requested to add an item.
		 *
		 * @return		The item to add, or null to add nothing.
		 */
		public Object itemAdd();

		/**
		 * Indicates that user has requested to edit the given item.
		 *
		 * @return		True if the item was edited.
		 */
		public boolean itemEdit( Object obj );

		/**
		 * Indicates that the user has requested that the order be changed.
		 *
		 * @param data		The data BEFORE the move.
		 * @param item		The item that would be moved.
		 * @param old_index	The old index of the item.
		 * @param new_index	The new index of the item.
		 *
		 * @return		True if the move should be allowed.
		 */
		public boolean orderChange( Object[] data, Object item, int old_index,
			int new_index );
	}


	private class MyListModel extends AbstractListModel {
		java.util.List data_store;

		MyListModel( Object[] data ) {
			data_store = new LinkedList();

			addAll( data );
		}

		public void addAll( Object[] data ) {
			if ( data == null || data.length == 0 ) return;

			int size = data_store.size();
			data_store.addAll( Arrays.asList( data ) );
			fireIntervalAdded( this, size, data_store.size() - size );
		}

		public int getSize() {
			return data_store.size();
		}

		public Object getElementAt( int index ) {
			return data_store.get( index );
		}


		public void moveUp( int index ) {
			if ( index <= 0 ) return;

			Object obj = data_store.remove( index );
			data_store.add( index - 1, obj );

			fireContentsChanged( this, index, index - 1 );
		}

		public void moveDown( int index ) {

			Object obj = data_store.remove( index );
			data_store.add( index + 1, obj );

			fireContentsChanged( this, index + 1, index );
		}

		public void add( Object obj ) {
			if ( obj == null ) return;

			data_store.add( obj );
			fireIntervalAdded( this, data_store.size() - 1, data_store.size() - 1 );
		}

		public void remove( int index ) {
			data_store.remove( index );
			fireIntervalRemoved( this, index, index );
		}

		public void edit( int index ) {
			fireContentsChanged( this, index, index );
		}

		public Object[] getData() {
			return data_store.toArray();
		}


		public void clear() {
			int size = data_store.size();

			data_store.clear();

			if ( size > 0 ) fireIntervalRemoved( this, 0, size - 1 );
		}
	}


	public static void main( String[] args ) {
		AddRemovePanelListener listener = new AddRemovePanelListener() {
			int counter = 0;

			public boolean itemEdit( Object obj ) {
				StringBuffer buf = ( StringBuffer ) obj;

				buf.reverse();

				return true;
			}

			public boolean orderChange( Object[] data, Object item, int old_index, int new_index ) {
				return true;
			}

			public boolean itemRemoved( Object obj ) {
				return true;
			}

			public Object itemAdd() {
				return new StringBuffer( String.valueOf( counter++ ) );
			}
		};

		AddRemovePanel panel = new AddRemovePanel(
			new StringBuffer[] { new StringBuffer( "lib/test" ), new StringBuffer( "test2" ) },
			true, true, null, listener );

		UIKit.testComponent( panel );
	}
}
