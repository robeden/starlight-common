/*
 * Copyright(c) 2002-2004, StarLight Systems
 * All rights reserved.
 */

package com.starlight.ui;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.prefs.Preferences;


/**
 *
 */
public class WindowKit {

	/**
	 * Centers a window on the screen
	 */
	static public void centerWindow( Window win ) {
		Point center = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
		Dimension win_size = win.getSize();
		Point window_point = new Point(
			center.x - ( win_size.width / 2 ),
			center.y - ( win_size.height / 2 ) );
		win.setLocation( window_point );
	}


	/**
	 * Centers a window on another window and make the child smaller than the
	 * parent.
	 *
	 * @param child  the window to center
	 * @param parent the window to center on
	 */
	public static void centerWindowOnWindow( Window child, Window parent ) {
		centerWindowOnWindow( child, parent, true );
	}

	/**
	 * Centers a window on another window
	 *
	 * @param child        the window to center
	 * @param parent       the window to center on
	 * @param resize_child Set to true if the child should be made smaller
	 *                     than the parent.
	 */
	public static void centerWindowOnWindow( Window child, Window parent,
		boolean resize_child ) {

		Dimension parent_size = parent.getSize();

		if ( resize_child ) {
			// If larger than screen, reduce window width or height
			if ( parent_size.width < child.getSize().width ) {
				child.setSize( parent_size.width, child.getSize().height );
			}
			if ( parent_size.height < child.getSize().height ) {
				child.setSize( child.getSize().width, parent_size.height );
			}
		}

		// Center Frame, Dialogue or Window on window
		int x = ( parent_size.width - child.getSize().width ) / 2;
		int y = ( parent_size.height - child.getSize().height ) / 2;

		Point ploc = parent.getLocation();

		child.setLocation( ploc.x + x, ploc.y + y );
	}

	/**
	 * This will add a listener to save changes when the given window is moved or resized,
	 * AND set the position & size of the window to the values currently saved,
	 * if applicable.
	 *
	 * @param window    Window to watch.
	 * @param pref_node Preferences node to save data in.
	 */
	public static void addWindowBoundsSaveListener( final Window window,
		final Preferences pref_node ) {

		addWindowBoundsSaveListener( window, pref_node, "" );
	}


	/**
	 * This will add a listener to save changes when the given window is moved or resized,
	 * AND set the position & size of the window to the values currently saved,
	 * if applicable.
	 *
	 * @param window    Window to watch.
	 * @param pref_node Preferences node to save data in.
	 */
	public static void addWindowBoundsSaveListener( final Window window,
		final Preferences pref_node, final String key_header ) {

		addWindowBoundsSaveListener( window, pref_node, key_header, false );
	}

	/**
	 * This will add a listener to save changes when the given window is moved or resized,
	 * AND set the position & size of the window to the values currently saved,
	 * if applicable.
	 *
	 * @param window    Window to watch.
	 * @param pref_node Preferences node to save data in.
	 */
	public static void addWindowBoundsSaveListener( final Window window,
		final Preferences pref_node, final String key_header,
		final boolean position_only ) {

		window.addComponentListener( new ComponentAdapter() {
			@Override
			public void componentMoved( ComponentEvent e ) {
				pref_node.putInt( key_header + "window_x", window.getX() );
				pref_node.putInt( key_header + "window_y", window.getY() );
			}

			@Override
			public void componentResized( ComponentEvent e ) {
				if ( position_only ) return;

				pref_node.putInt( key_header + "window_width", window.getWidth() );
				pref_node.putInt( key_header + "window_height", window.getHeight() );
			}
		} );

		int x = pref_node.getInt( key_header + "window_x", -1 );
		int y = pref_node.getInt( key_header + "window_y", -1 );
		if ( x >= 0 && y >= 0 ) window.setLocation( x, y );

		if ( position_only ) return;
		
		int w = pref_node.getInt( key_header + "window_width", -1 );
		int h = pref_node.getInt( key_header + "window_height", -1 );
		Dimension min = window.getMinimumSize();
		if ( w > 0 && h > 0 ) {
			window.setSize( Math.max( w, min.width ), Math.max( h, min.height ) );
		}
	}
}
