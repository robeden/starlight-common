/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */

package com.starlight.ui;

import com.starlight.ExceptionKit;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.MessageFormat;


/**
 * static class to handle exceptions
 */
public class ExceptionHandler {
	private final static String TITLE_MSG = "Error";
	private final static String SHOW_DETAILS_MSG = "Show Technical Info";
	private final static String HIDE_DETAILS_MSG = "Hide Technical Info";
	private final static String DEFAULT_MSG =
		"An unknown error has occurred.\nPlease try again or notify technical support.";
	private final static String DEFAULT_MSG_WITH_EXCEPTION =
		"{0} ({1})\nPlease try again or notify technical support.";

	private final static int DEFAULT_MAX_ERROR_HEIGHT = 350;
	private final static int DEFAULT_MAX_ERROR_WIDTH = 500;


	private static final ThreadLocal<Boolean> AWT_LOOP_DETECT = new ThreadLocal<Boolean>();

	private static JFrame default_frame = null;


	/**
	 * Constructor allows using the system property "sun.awt.exception.handler" to
	 * handle EventQueue exception.
	 */
	public ExceptionHandler() {}


	/**
	 * Set the default frame that will be used for the parent if none is specified.
	 */
	public static void initDefaultFrame( JFrame frame ) {
		default_frame = frame;
	}


	public static void handle( final Settings settings ) {
		if ( !SwingUtilities.isEventDispatchThread() ) {
			SwingUtilities.invokeLater( new Runnable() {
				@Override
				public void run() {
					handle( settings );
				}
			} );
			return;
		}

		if ( settings.dialog == null && settings.frame == null ) {
			settings.frame( default_frame );
		}

		if ( settings.message == null ) {
			if ( settings.throwable != null && settings.throwable.getMessage() != null ) {
				settings.message( MessageFormat.format( DEFAULT_MSG_WITH_EXCEPTION,
					settings.throwable.getMessage(),
					settings.throwable.getClass().getSimpleName() ) );
			}
			else settings.message( DEFAULT_MSG );
		}

		if ( settings.throwable != null ) {
			try {
				settings.throwable.printStackTrace();
			}
			catch( Throwable t ) {
				// ignore
			}
		}

		try {
			JDialog dialog;
			if ( settings.frame != null ) {
				dialog = createDialog( settings.frame, settings.throwable,
					settings.message );
			}
			else {
				dialog = createDialog( settings.dialog, settings.throwable,
					settings.message );
			}

			dialog.setVisible( true );
			dialog.toFront();
		}
		catch( Throwable t ) {
			// ignore
		}

		if ( settings.exit_when_closed ) System.exit( -1 );
	}


	/**
	 * Handle just a message
	 */
	public static void handle( String msg ) {
		handle( new Settings().message( msg ) );
	}

	/**
	 * Handle using the MainFrame and the default error message.
	 *
	 * NOTE: this siganture must remain unchanged for the "sun.awt.exception.handler"
	 *       handler to work.
	 */
	public static void handle( Throwable e ) {
		if ( AWT_LOOP_DETECT.get() != null ) {
			System.err.println( "Loop detected while trying to handle AWT error:" );
			e.printStackTrace( System.err );
			return;
		}

		AWT_LOOP_DETECT.set( Boolean.TRUE );
		try {
			handle( new Settings().throwable( e ) );
		}
		finally {
			AWT_LOOP_DETECT.remove();
		}
	}


	/**
	 * Handle an exception, using the MainFrame as the owner
	 */
	public static void handle( Throwable e, String msg ) {
		handle( new Settings().throwable( e ).message( msg ) );
	}

	/**
	 * Handle and exception, specifiying the frame
	 */
	public static void handle( JFrame frame, Throwable e, String msg ) {
		handle( new Settings().frame( frame ).throwable( e ).message( msg ) );
	}


	public static void handle( JDialog owner, Throwable e, String msg ) {
		handle( new Settings().dialog( owner ).throwable( e ).message( msg ) );
	}

	/**
	 * Create the Exception dialog
	 */
	public static JDialog createDialog( Frame frame, Throwable e, String msg ) {
		if ( msg == null ) throw new IllegalArgumentException( "Message must not be null" );
		return new EDialog( frame, e, msg );
	}

	public static JDialog createDialog( Dialog owner, Throwable e, String msg ) {
		if ( msg == null ) throw new IllegalArgumentException( "Message must not be null" );
		return new EDialog( owner, e, msg );
	}


	private static class EDialog extends EscapeDialog implements ActionListener {
		private final static String HIDE_DETAILS_COMMAND = "HIDE";
		private final static String SHOW_DETAILS_COMMAND = "SHOW";
		private final static String CLOSE_COMMAND = "CLOSE";

		private Throwable throwable = null;
		private String msg = null;

		private JButton details_button = null;
		private JScrollPane e_scroll = null;

		public EDialog( Frame frame, Throwable e, String message ) {
			super( frame, TITLE_MSG, true );

			init_common( e, message );
		}

		public EDialog( Dialog dialog, Throwable e, String message ) {
			super( dialog, TITLE_MSG, true );

			init_common( e, message );
		}


		private void init_common( Throwable e, String message ) {
			this.throwable = e;
			this.msg = message;
			init();

			this.setLocationRelativeTo( null );
		}

		private void init() {

			if ( JDialog.isDefaultLookAndFeelDecorated() ) {
				boolean supportsWindowDecorations =
					UIManager.getLookAndFeel().getSupportsWindowDecorations();

				if ( supportsWindowDecorations ) {
					this.setUndecorated( true );
					this.getRootPane().setWindowDecorationStyle( JRootPane.ERROR_DIALOG );
				}
			}

			JLabel image = new JLabel( UIManager.getIcon( "OptionPane.errorIcon" ) );
			JLabel message = new MultiLineLabel( msg );
			image.setBorder( new EmptyBorder( 10, 10, 10, 10 ) );

			details_button = new JButton( SHOW_DETAILS_MSG );
			details_button.setActionCommand( SHOW_DETAILS_COMMAND );
			details_button.addActionListener( this );

			final JButton close_button = new JButton( "Close" );
			close_button.setActionCommand( CLOSE_COMMAND );
			close_button.addActionListener( this );

//			send_bug_report_button = new JButton( "Send Bug Report" );
//			send_bug_report_button.setActionCommand( "SEND_BUG_REPORT" );
//			send_bug_report_button.addActionListener( this );

			JTextArea earea = new JTextArea();
			earea.setEditable( false );
			if ( throwable != null ) {
				earea.setText( ExceptionKit.getStackTrace( throwable ) );
			}

			e_scroll = new JScrollPane( earea );
			e_scroll.setVisible( false );

			JPanel top = new JPanel( new BorderLayout( 5, 5 ) );
			top.add( image, BorderLayout.WEST );
			top.add( message, BorderLayout.CENTER );

			JPanel bottom = new JPanel( new FlowLayout( FlowLayout.RIGHT, 5, 0 ) );
			bottom.setBorder( null );

			// Add a couple more buttons if we're given an exception
			if ( throwable != null ) {
				bottom.add( details_button );
			}
			bottom.add( close_button );

//			if ( throwable != null ) {
//				JPanel new_bottom = new JPanel( new BorderLayout() );
//				new_bottom.add( bottom, BorderLayout.LINE_END );
//
//				new_bottom.add( send_bug_report_button, BorderLayout.LINE_START );
//				bottom = new_bottom;
//			}

			JPanel big = new JPanel( new BorderLayout( 5, 5 ) );

			big.add( top, BorderLayout.NORTH );
			if ( throwable != null ) big.add( e_scroll, BorderLayout.CENTER );
			big.add( bottom, BorderLayout.SOUTH );
			big.setBorder( new EmptyBorder( 5, 5, 5, 5 ) );

			getContentPane().setLayout( new BorderLayout( 0, 0 ) );
			getContentPane().add( big, BorderLayout.CENTER );
//			this.getRootPane().setMaximumSize(new Dimension(500, 500));

			getRootPane().setDefaultButton( close_button );
			addWindowListener( new WindowAdapter() {
				public void windowActivated( WindowEvent e ) {
					close_button.requestFocusInWindow();
				}
			} );

			Dimension current_scroller_size = e_scroll.getPreferredSize();
			if ( current_scroller_size.width > DEFAULT_MAX_ERROR_WIDTH ||
				current_scroller_size.height > DEFAULT_MAX_ERROR_HEIGHT ) {

				Dimension new_size = new Dimension(
					Math.min( current_scroller_size.width, DEFAULT_MAX_ERROR_WIDTH ),
					Math.min( current_scroller_size.height, DEFAULT_MAX_ERROR_HEIGHT ) );
				e_scroll.setPreferredSize( new_size );
			}

			pack();
		}

		/**
		 * Invoked when an action occurs.
		 */
		public void actionPerformed( ActionEvent e ) {
			if ( e.getActionCommand().equals( CLOSE_COMMAND ) ) {
				setVisible( false );
			}
			else if ( e.getActionCommand().equals( SHOW_DETAILS_COMMAND ) ) {
				e_scroll.setVisible( true );
				pack();

				details_button.setActionCommand( HIDE_DETAILS_COMMAND );
				details_button.setText( HIDE_DETAILS_MSG );
			}
			else if ( e.getActionCommand().equals( HIDE_DETAILS_COMMAND ) ) {
				e_scroll.setVisible( false );
				pack();

				details_button.setActionCommand( SHOW_DETAILS_COMMAND );
				details_button.setText( SHOW_DETAILS_MSG );
			}
//			else if ( e.getActionCommand().equals( "SEND_BUG_REPORT" ) ) {
//				new Thread() {
//					public void run() {
//						FeedbackDialog.show( new FeedbackDialog.Settings().product(
//							product_info ).event(
//							new ExceptionEventInfo( throwable ) ).dialog( EDialog.this ) );
//					}
//				}.start();
//			}
		}
	}

	public static void main(String[] args) {
		handle((JFrame) null, new Exception(), "This is just a test error!");
	}


	public static class Settings {
		private Throwable throwable;
		private String message;
		private boolean exit_when_closed;

		private JFrame frame;
		private JDialog dialog;

		public Settings() {}

		Settings( Throwable throwable, String message, boolean exit_when_closed ) {
			this.throwable = throwable;
			this.message = message;
			this.exit_when_closed = exit_when_closed;
		}

		public Settings throwable( Throwable throwable ) {
			this.throwable = throwable;
			return this;
		}

		public Settings message( String message ) {
			this.message = message;
			return this;
		}

		public Settings exitWhenClosed( boolean exit_when_closed ) {
			this.exit_when_closed = exit_when_closed;
			return this;
		}

		public Settings dialog( JDialog dialog ) {
			this.dialog = dialog;
			return this;
		}

		public Settings frame( JFrame frame ) {
			this.frame = frame;
			return this;
		}
	}
}
