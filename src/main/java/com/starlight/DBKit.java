/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */
package com.starlight;

import java.sql.*;


public class DBKit {

	/**
	 * Convenience method.
	 */
	public static void close( Connection conn, Statement statement ) {
		close( conn );
		close( statement );
	}

	/**
	 * Convenience method.
	 */
	public static void close( Connection conn, Statement statement, ResultSet result ) {
		close( conn );
		close( statement );
		close( result );
	}


	/**
	 * Close if non-null without throwing and exception.
	 */
	public static void close( Connection conn ) {
		if ( conn != null ) {
			try {
				conn.close();
			}
			catch ( SQLException ex ) {
				// ignore
			}
		}
	}

	/**
	 * Close if non-null without throwing and exception.
	 */
	public static void close( Statement statement ) {
		if ( statement != null ) {
			try {
				statement.close();
			}
			catch ( SQLException ex ) {
				// ignore
			}
		}
	}

	/**
	 * Close if non-null without throwing and exception.
	 */
	public static void close( ResultSet result ) {
		if ( result != null ) {
			try {
				result.close();
			}
			catch ( SQLException ex ) {
				// ignore
			}
		}
	}


	/**
	 * Get a boolean from a number column. "0" means false and anything else means true.
	 */
	public static boolean getBoolean( ResultSet result, int column_index )
		throws SQLException {

		int value = result.getInt( column_index );
		return value != 0;
	}

	/**
	 * Get a boolean from a number column. "0" means false and anything else means true.
	 */
	public static boolean getBoolean( ResultSet result, String column_name )
		throws SQLException {

		int value = result.getInt( column_name );
		return value != 0;
	}

	/**
	 * Set a boolean to a number column.
	 */
	public static void setBoolean( PreparedStatement ps, int column_index, boolean value )
		throws SQLException {

		ps.setInt( column_index, value ? 1 : 0 );
	}

	/**
	 * Set a boolean to a number column.
	 */
	public static void setBoolean( PreparedStatement ps, int column_index, Boolean value )
		throws SQLException {

		setBoolean( ps, column_index, value != null && value.booleanValue() );
	}


	/**
	 * Get a string for the JDBC type and size given.
	 */
	public static String getJDBCTypeName( int type ) {
		return getJDBCTypeName( type, -1 );
	}


	/**
	 * Get a string for the JDBC type and size given.
	 */
	public static String getJDBCTypeName( int type, int size ) {
		StringBuffer buf;
		switch ( type ) {
			case Types.ARRAY:
				buf = new StringBuffer( "ARRAY" );
				break;
			case Types.BIGINT:
				buf = new StringBuffer( "BIGINT" );
				break;
			case Types.BINARY:
				buf = new StringBuffer( "BINARY" );
				break;
			case Types.BIT:
				buf = new StringBuffer( "BIT" );
				break;
			case Types.BLOB:
				buf = new StringBuffer( "BLOB" );
				break;
			case Types.BOOLEAN:
				buf = new StringBuffer( "BOOLEAN" );
				break;
			case Types.CHAR:
				buf = new StringBuffer( "CHAR" );
				break;
			case Types.CLOB:
				buf = new StringBuffer( "CLOB" );
				break;
			case Types.DATALINK:
				buf = new StringBuffer( "DATALINK" );
				break;
			case Types.DATE:
				buf = new StringBuffer( "DATE" );
				break;
			case Types.DECIMAL:
				buf = new StringBuffer( "DECIMAL" );
				break;
			case Types.DISTINCT:
				buf = new StringBuffer( "DISTINCT" );
				break;
			case Types.DOUBLE:
				buf = new StringBuffer( "DOUBLE" );
				break;
			case Types.FLOAT:
				buf = new StringBuffer( "FLOAT" );
				break;
			case Types.INTEGER:
				buf = new StringBuffer( "INTEGER" );
				break;
			case Types.JAVA_OBJECT:
				buf = new StringBuffer( "JAVA_OBJECT" );
				break;
			case Types.LONGVARBINARY:
				buf = new StringBuffer( "LONGVARBINARY" );
				break;
			case Types.LONGVARCHAR:
				buf = new StringBuffer( "LONGVARCHAR" );
				break;
			case Types.NULL:
				buf = new StringBuffer( "NULL" );
				break;
			case Types.NUMERIC:
				buf = new StringBuffer( "NUMERIC" );
				break;
			case Types.OTHER:
				buf = new StringBuffer( "OTHER" );
				break;
			case Types.REAL:
				buf = new StringBuffer( "REAL" );
				break;
			case Types.REF:
				buf = new StringBuffer( "REF" );
				break;
			case Types.SMALLINT:
				buf = new StringBuffer( "SMALLINT" );
				break;
			case Types.STRUCT:
				buf = new StringBuffer( "STRUCT" );
				break;
			case Types.TIME:
				buf = new StringBuffer( "TIME" );
				break;
			case Types.TIMESTAMP:
				buf = new StringBuffer( "TIMESTAMP" );
				break;
			case Types.TINYINT:
				buf = new StringBuffer( "TINYINT" );
				break;
			case Types.VARBINARY:
				buf = new StringBuffer( "VARBINARY" );
				break;
			case Types.VARCHAR:
				buf = new StringBuffer( "VARCHAR" );
				break;
			default:
				buf = new StringBuffer( "<<unknown>>" );
				break;
		}

		if ( size > 0 ) {
			buf.append( "(" );
			buf.append( size );
			buf.append( ")" );
		}

		return buf.toString();
	}


	/**
	 * Simple helper method to retrieve a java.util.Date from a SQL date column.
	 */
	public static java.util.Date getNormalDate( ResultSet result, String column )
		throws SQLException {

		java.sql.Date date = result.getDate( column );
		if ( date == null ) return null;

		return new java.util.Date( date.getTime() );
	}

	/**
	 * Simple helper method to retrieve a java.util.Date from a SQL timestamp column.
	 */
	public static java.util.Date getNormalDateForTimestamp( ResultSet result,
		String column ) throws SQLException {

		java.sql.Timestamp date = result.getTimestamp( column );
		if ( date == null ) return null;

		return new java.util.Date( date.getTime() );
	}


	/**
	 * Simple helper method to set a SQL date columne using a java.util.Date.
	 */
	public static void setNormalDate( PreparedStatement ps, int column_index,
		java.util.Date date ) throws SQLException {

		if ( date == null ) ps.setNull( column_index, Types.DATE );
		else ps.setDate( column_index, new java.sql.Date( date.getTime() ) );
	}


	public static java.util.Date getNormalDateFor2PartTimestamp( ResultSet result,
		String date_column, String time_column ) throws SQLException {

		java.sql.Date date = result.getDate( date_column );

		if ( date == null ) return null;

		Time time = result.getTime( time_column );

		return new java.util.Date(
			new Timestamp( date.getTime() + time.getTime() ).getTime() );
	}
}
