package com.starlight;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;


/**
 * Indicates that a parameter, method return value or variable/field CANNOT be null.
 */
@Documented
@Retention( RUNTIME )
@Target( { METHOD, FIELD, PARAMETER, LOCAL_VARIABLE})
public @interface NotNull {}
