/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */
package com.starlight;

import java.math.BigDecimal;


/**
 * A kit of miscellaneous useful functions.
 */
public final class MiscKit {
	/**
	 * Test to see if two objects are equal, handling nulls gracefully.
	 */
	public static boolean equal( final Object o1, final Object o2 ) {
		// Special case BigDecimal because equals doesn't work properly there.
		if ( o1 instanceof BigDecimal ) {
			return o2 instanceof BigDecimal &&
				( ( BigDecimal ) o1 ).compareTo( ( BigDecimal ) o2 ) == 0;
		}

		if ( o1 == null ) return o2 == null;
		return o1.equals( o2 );
	}


	/**
	 * Returns the {@link Boolean#booleanValue()} of a boolean object or a default value
	 * if the Boolean is null.
	 */
	public static boolean booleanValue( Boolean object, boolean default_value ) {
		if ( object == null ) return default_value;
		else return object.booleanValue();
	}
}
