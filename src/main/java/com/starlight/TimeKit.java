/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */
package com.starlight;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Calendar;


/**
 * Various Time utility functions.
 *
 * @author reden
 */
public final class TimeKit {
	private static final long MINUTE_MS = 60 * 1000;
	private static final long HOUR_MS = MINUTE_MS * 60;
	private static final long DAY_MS = HOUR_MS * 24;
	private static final long MONTH_MS = DAY_MS * 30;
	private static final long YEAR_MS = MONTH_MS * 12;

	private static Date prototype_date = null;


	/**
	 * Get a string that says the time between to time (in milliseconds) in this form:
	 * "1 year 3 months 4 days 16 hours 32 minutes".
	 */
	public static String uptimeString( long from_time, long to_time ) {
		long time_ms = to_time - from_time;

		if ( time_ms < 0 ) throw new IllegalArgumentException( "\"to\" must be greater " +
			"than \"from\"" );

		BigDecimal dec = new BigDecimal( time_ms );

		int num_years = dec.divide( new BigDecimal( YEAR_MS ),
			BigDecimal.ROUND_DOWN ).intValue();

		dec = dec.subtract( new BigDecimal( YEAR_MS * num_years ) );

		int num_months = dec.divide( new BigDecimal( MONTH_MS ),
			BigDecimal.ROUND_DOWN ).intValue();

		dec = dec.subtract( new BigDecimal( MONTH_MS * num_months ) );

		int num_days = dec.divide( new BigDecimal( DAY_MS ),
			BigDecimal.ROUND_DOWN ).intValue();

		dec = dec.subtract( new BigDecimal( DAY_MS * num_days ) );

		int num_hours = dec.divide( new BigDecimal( HOUR_MS ),
			BigDecimal.ROUND_DOWN ).intValue();

		dec = dec.subtract( new BigDecimal( HOUR_MS * num_hours ) );

		int num_minutes = dec.divide( new BigDecimal( MINUTE_MS ),
			BigDecimal.ROUND_DOWN ).intValue();

		dec = dec.subtract( new BigDecimal( MINUTE_MS * num_minutes ) );

		StringBuffer buf = new StringBuffer();

		if ( num_years > 0 )
			buf.append( num_years ).append( " years " );
		if ( num_years > 0 || num_months > 0 )
			buf.append( num_months ).append( " months " );
		if ( num_years > 0 || num_months > 0 || num_days > 0 )
			buf.append( num_days ).append( " days " );
		if ( num_years > 0 || num_months > 0 || num_days > 0 || num_hours > 0 )
			buf.append( num_hours ).append( " hours " );
		buf.append( num_minutes ).append( " minutes " );
		return buf.toString();
	}


	/**
	 * Return the Date object for 12/31/2000. This is useful for setting the preferred
	 * size for time fields because nothing will get wider than that.
	 */
	public synchronized static Date getPrototypeDate() {
		if ( prototype_date == null ) {
			Calendar cal = Calendar.getInstance();
			cal.set( Calendar.MONTH, Calendar.DECEMBER );
			cal.set( Calendar.DAY_OF_MONTH, 31 );
			cal.set( Calendar.YEAR, 2000 );
			prototype_date = cal.getTime();
		}

		return prototype_date;
	}


	/**
	 * Calculate the age in years if a person were born on the given date.
	 *
	 * @param birthdate The date the person was born.
	 */
	public static int calculateAge( Date birthdate ) {
		return calculateAge( birthdate, null );
	}

	/**
	 * Calculate the age in years at a certain if a person were born on the given date.
	 *
	 * @param birthdate The date the person was born.
	 * @param date      The at which we want to know the person's age.
	 */
	public static int calculateAge( Date birthdate, Date date ) {
		if ( birthdate == null ) {
			throw new IllegalArgumentException( "Birthdate cannot be null." );
		}

		Calendar cal = Calendar.getInstance();

		if ( date != null ) cal.setTime( date );

		int current_day = cal.get( Calendar.DAY_OF_MONTH );
		int current_month = cal.get( Calendar.MONTH );
		int current_year = cal.get( Calendar.YEAR );

		cal.setTime( birthdate );

		int day = cal.get( Calendar.DAY_OF_MONTH );
		int month = cal.get( Calendar.MONTH );
		int year = cal.get( Calendar.YEAR );

		int age = current_year - year;
		// but, if we're not to that month/day yet, subtract 1
		if ( current_month < month ||
			( current_month == month && current_day < day ) ) {
			age--;
		}

		return age;
	}


//	public static void main( String args[] ) {
//		long value = Long.parseLong( args[ 0 ] );
//		System.out.println( uptimeString( 0, value ) );
//	}
}
