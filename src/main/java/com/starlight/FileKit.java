/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */

package com.starlight;

import java.io.*;


/**
 * Static utility functions for working with Files.
 */
public class FileKit {
	private FileKit() {
	}


	/**
	 * Copy the source file to the destination, overwriting if the destination exists.
	 *
	 * @throws IllegalArgumentException If the source file does not exist.
	 */
	public static void copyFile( File source, File destination )
		throws IOException, IllegalArgumentException {

		if ( !source.exists() ) {
			throw new IllegalArgumentException( "Source does not exist." );
		}

		InputStream in = null;
		OutputStream out = null;
		try {
			in = new FileInputStream( source );
			out = new FileOutputStream( destination, false );

			IOKit.copy( in, out );
		}
		finally {
			IOKit.close( in );
			IOKit.close( out );
		}
	}
}
