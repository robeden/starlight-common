/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */
package com.starlight;

/**
 * Static functions for validating variable.
 *
 * @deprecated
 */
@Deprecated
public class ValidationKit {
	private ValidationKit() {
	}


	/**
	 * Check that the given field is non-null and throw an IllegalArgumentException if it
	 * is null.
	 *
	 * @param variable The variable to check.
	 * @param name     The name of the variable (will be put in the message of the
	 *                 exception if it is null).
	 *
	 * @deprecated  Use {@link java.util.Objects#requireNonNull(Object)}
	 */
	@Deprecated
	public static void checkNonnull( @NotNull final Object variable,
		@NotNull final String name ) throws IllegalArgumentException {

		//noinspection ConstantConditions
		if ( variable == null ) {
			throw new IllegalArgumentException( name + " cannot be null" );
		}
	}

	/**
	 * Check that the given field is greater than a given value and throw an
	 * IllegalArgumentException if it is not.
	 *
	 * @param variable     The variable to check.
	 * @param greater_than The value the variable must be greater than.
	 * @param name         The name of the variable (will be put in the message of the
	 *                     exception if it is null).
	 */
	public static void checkGreaterThan( final int variable, final int greater_than,
		@NotNull final String name ) throws IllegalArgumentException {

		if ( variable <= greater_than ) {
			throw new IllegalArgumentException( name + " must be greater than " +
				greater_than );
		}
	}

	/**
	 * Check that the given field is greater than a given value and throw an
	 * IllegalArgumentException if it is not.
	 *
	 * @param variable     The variable to check.
	 * @param greater_than The value the variable must be greater than.
	 * @param name         The name of the variable (will be put in the message of the
	 *                     exception if it is null).
	 */
	public static void checkGreaterThan( final long variable, final long greater_than,
		@NotNull final String name ) throws IllegalArgumentException {

		if ( variable <= greater_than ) {
			throw new IllegalArgumentException( name + " must be greater than " +
				greater_than );
		}
	}

	/**
	 * Check that the given field is less than a given value and throw an
	 * IllegalArgumentException if it is not.
	 *
	 * @param variable  The variable to check.
	 * @param less_than The value the variable must be less than.
	 * @param name      The name of the variable (will be put in the message of the
	 *                  exception if it is null).
	 */
	public static void checkLessThan( final int variable, final int less_than,
		@NotNull final String name ) throws IllegalArgumentException {

		if ( variable >= less_than ) {
			throw new IllegalArgumentException( name + " must be less than " +
				less_than );
		}
	}

	/**
	 * Check that the given field is less than a given value and throw an
	 * IllegalArgumentException if it is not.
	 *
	 * @param variable  The variable to check.
	 * @param less_than The value the variable must be less than.
	 * @param name      The name of the variable (will be put in the message of the
	 *                  exception if it is null).
	 */
	public static void checkLessThan( final long variable, final long less_than,
		@NotNull final String name ) throws IllegalArgumentException {

		if ( variable >= less_than ) {
			throw new IllegalArgumentException( name + " must be less than " +
				less_than );
		}
	}
}
