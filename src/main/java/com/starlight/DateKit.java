/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */
package com.starlight;

import java.util.Calendar;
import java.util.Date;


/**
 *
 */
public class DateKit {
	public enum TodayYesterday {
		TODAY,
		YESTERDAY
	}


	/**
	 * Template for date fields to get the right preferred size.
	 */
	public static final Date DATE_TEMPLATE;
	/**
	 * Earliest allowed birthdate
	 */
	public static final Date EARLIEST_BIRTHDATE;

	static {
		Calendar cal = Calendar.getInstance();
		cal.set( Calendar.YEAR, 2000 );
		cal.set( Calendar.MONTH, 12 );
		cal.set( Calendar.DAY_OF_MONTH, 30 );
		DATE_TEMPLATE = cal.getTime();

		cal.set( Calendar.YEAR, 1920 );
		cal.set( Calendar.MONTH, 1 );
		cal.set( Calendar.DAY_OF_MONTH, 1 );
		EARLIEST_BIRTHDATE = cal.getTime();
	}


	/**
	 * Returns the number of days between <i>newer</i> and <i>b</i>. If <i>b</i> is later than
	 * <i>newer</i>, retuns newer negative number i.e. <i>newer</i> - <i>b</i>.
	 * <p/>
	 * this should work with any modern dates.  Could infinitely loop with 'old' dates
	 * because one year had less than 365 days.
	 */
	public static int daysDifference( Date newer, Date order ) {
		int difference = 0;
		Calendar calendarA = Calendar.getInstance();
		Calendar calendarB = Calendar.getInstance();

		calendarA.setTime( newer );
		calendarB.setTime( order );

		/* adds to or subtracts from calendarB in 365 day multiples until the years match   */
		while ( calendarA.get( Calendar.YEAR ) != calendarB.get( Calendar.YEAR ) ) {
			int tempDifference = 365 * ( calendarA.get( Calendar.YEAR )
				- calendarB.get( Calendar.YEAR ) );
			difference += tempDifference;

			calendarB.add( Calendar.DAY_OF_YEAR, tempDifference );
		}


		if ( calendarA.get( Calendar.DAY_OF_YEAR ) !=
			calendarB.get( Calendar.DAY_OF_YEAR ) ) {

			difference += calendarA.get( Calendar.DAY_OF_YEAR )
				- calendarB.get( Calendar.DAY_OF_YEAR );
		}

		return difference;
	}


	/**
	 * Fixes two-digit dates.
	 */
	public static int cleanYear( int year ) throws IllegalArgumentException {
		// See how many digits it has
		String year_str = String.valueOf( year );
		if ( year_str.length() == 2 || year_str.length() == 1 ) {
			if ( year < 40 ) year += 2000;
			else year += 1900;

			return year;
		}
		else if ( year_str.length() != 4 ) {
			throw new IllegalArgumentException( year_str.length() +
				"-digit year is illegal" );
		}
		else if ( year > 2099 )
			throw new IllegalArgumentException( year + " is illegal" );
		else if ( year < 1900 )
			throw new IllegalArgumentException( year + " is illegal" );
		// else leave it alone

		return year;
	}


	/**
	 * Determines if the given date can is "today" or "yesterday".
	 *
	 * @return Null if not today or yesterday.
	 */
	public static TodayYesterday isTodayOrYesterday( Date date ) {
		return isTodayOrYesterday( date, null );
	}

	/**
	 * Determines if the given date can is "today" or "yesterday".
	 *
	 * @return Null if not today or yesterday.
	 */
	public static TodayYesterday isTodayOrYesterday( Date date, Calendar cal ) {
		if ( cal == null ) cal = Calendar.getInstance();

		cal.setTime( date );

		int day = cal.get( Calendar.DAY_OF_MONTH );
		int month = cal.get( Calendar.MONTH );
		int year = cal.get( Calendar.YEAR );

		cal.setTimeInMillis( System.currentTimeMillis() );

		if ( cal.get( Calendar.DAY_OF_MONTH ) == day &&
			cal.get( Calendar.MONTH ) == month && cal.get( Calendar.YEAR ) == year ) {

			return TodayYesterday.TODAY;
		}

		cal.add( Calendar.DAY_OF_YEAR, -1 );

		if ( cal.get( Calendar.DAY_OF_MONTH ) == day &&
			cal.get( Calendar.MONTH ) == month && cal.get( Calendar.YEAR ) == year ) {

			return TodayYesterday.YESTERDAY;
		}

		return null;
	}


	/**
	 * Return a Date object with the same date (i.e., day, month, year) of the given
	 * date, but with all time components removed.
	 */
	public static Date stripTime( Date date ) {
		Calendar cal = Calendar.getInstance();
		cal.setTime( date );
		cal.set( Calendar.HOUR_OF_DAY, 0 );
		cal.set( Calendar.MINUTE, 0 );
		cal.set( Calendar.SECOND, 0 );
		cal.set( Calendar.MILLISECOND, 0 );
		return cal.getTime();
	}


	/**
	 * Return the minimum of the given dates.
	 */
	public static Date min( Date one, Date two ) {
		if ( one.getTime() < two.getTime() ) return one;
		else return two;
	}


	/**
	 * Return the maximum of the given dates.
	 */
	public static Date max( Date one, Date two ) {
		if ( one.getTime() > two.getTime() ) return one;
		else return two;
	}


	public static void main( String[] args ) {
		Calendar cal = Calendar.getInstance();

		cal.set( 2007, 1, 3 );
		Date feb3_07 = cal.getTime();

		cal.set( 2008, 1, 3 );
		Date feb3_08 = cal.getTime();

		cal.set( 2006, 10, 4 );
		Date nov4_06 = cal.getTime();

		System.out.println( "Diff: " + daysDifference( nov4_06, feb3_07 ) );
		System.out.println( "Diff: " + daysDifference( feb3_07, nov4_06 ) );

		System.out.println( "Diff: " + daysDifference( nov4_06, feb3_08 ) );
		System.out.println( "Diff: " + daysDifference( feb3_08, nov4_06 ) );
	}
}
