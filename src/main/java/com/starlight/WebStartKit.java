/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */
package com.starlight;

import java.lang.reflect.Method;
import java.net.URL;


/**
 * Static functions that are useful when running in WebStart.
 */
public class WebStartKit {
	private static final boolean supports_url_launching;
	private static final Method show_document_method;
	private static final Object basic_service;

	static {

		// Note: We're going to do a bunch of reflection here to avoid requiring the JNLP
		// JAR to build the launcher.
		Object basic_service_tmp;
		Method show_document_method_tmp;
		boolean supports_url_launching_tmp;
		try {
			// See if we're running in webstart
			Class service_manager_class = Class.forName( "javax.jnlp.ServiceManager" );
			Method service_names_method =
				service_manager_class.getMethod( "getServiceNames", new Class[]{ } );
			String[] service_names = ( String[] ) service_names_method.invoke(
				service_manager_class );

			// If null or empty, not running in WebStart

			if ( service_names == null || service_names.length == 0 ) {
				supports_url_launching_tmp = false;
				show_document_method_tmp = null;
				basic_service_tmp = null;
			}
			else {
				// Lookup the BasicService (needed to launch URLs)
				Class basic_service_class = Class.forName( "javax.jnlp.BasicService" );

				Method lookup_method =
					service_manager_class
						.getMethod( "lookup", new Class[]{ String.class } );

				basic_service_tmp = lookup_method.invoke( service_manager_class,
					basic_service_class.getName() );

				// See if web browsers are supported
				Method web_browser_supported_method =
					basic_service_class
						.getMethod( "isWebBrowserSupported", new Class[]{ } );
				Boolean bool = ( Boolean ) web_browser_supported_method.invoke(
					basic_service_tmp );
				if ( bool == null || !bool.booleanValue() ) {
					supports_url_launching_tmp = false;
					show_document_method_tmp = null;
				}
				else {
					show_document_method_tmp =
						basic_service_class.getMethod( "showDocument",
							new Class[]{ URL.class } );
					supports_url_launching_tmp = true;
				}
			}
		}
		catch ( Exception ex ) {
			supports_url_launching_tmp = false;
			show_document_method_tmp = null;
			basic_service_tmp = null;
		}

		supports_url_launching = supports_url_launching_tmp;
		basic_service = basic_service_tmp;
		show_document_method = show_document_method_tmp;
	}


	/**
	 * See if we currently support URL launching.
	 */
	public static boolean supportsURLLaunching() {
		return supports_url_launching;
	}

	/**
	 * If possible, launch a URL in the client's web browser.
	 */
	public static boolean launchURL( URL url ) {
		ValidationKit.checkNonnull( url, "URL" );

		if ( !supports_url_launching ) return false;

		try {
			Boolean bool = ( Boolean ) show_document_method.invoke(
				basic_service, url );
			return bool != null && bool.booleanValue();
		}
		catch ( Exception ex ) {
			return false;
		}
	}
}