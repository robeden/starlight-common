package com.starlight.validity2;

import com.starlight.NotNull;
import com.starlight.locale.ResourceKey;

import java.util.function.Consumer;


/**
 * A validity check is a single check of data validity. Checks generally check a single
 * datum and so are very simple, but they can also be combined to be arbitrarily complex.
 */
public interface ValidityCheck {
	/**
	 * Perform the check.
	 *
	 * @param fail_message_consumer     Consumer for indicating a reason for the failure.
	 *
	 * @return  True if the check passed or false if it failed.
	 */
	public boolean check( @NotNull Consumer<ResourceKey<String>> fail_message_consumer );
}
