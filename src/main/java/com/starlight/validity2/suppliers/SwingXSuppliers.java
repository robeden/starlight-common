package com.starlight.validity2.suppliers;

import org.jdesktop.swingx.JXDatePicker;

import java.util.Date;
import java.util.function.Supplier;


/**
 * {@link Supplier} implementations for SwingX sources.
 */
public class SwingXSuppliers {
	/**
	 * Returns a supplier with the text of the given {@link java.awt.TextComponent}
	 */
	public static Supplier<Date> dateSupplier( final JXDatePicker component ) {
		return new SwingSuppliers.ComponentSourceSupplier<Date,JXDatePicker>( component ) {
			@Override
			public Date get() {
				return component.getDate();
			}
		};
	}
}
