package com.starlight.validity2;

import com.starlight.NotNull;


/**
 *
 */
public interface CheckHandler {
	public void checkPass();
	public void checkFail( @NotNull FailureInfo info );
}
