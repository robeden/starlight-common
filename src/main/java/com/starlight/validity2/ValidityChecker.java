package com.starlight.validity2;

import com.starlight.MiscKit;
import com.starlight.NotNull;
import com.starlight.Nullable;
import com.starlight.listeners.ListenerSupport;
import com.starlight.listeners.ListenerSupportFactory;
import com.starlight.locale.ResourceKey;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Consumer;


/**
 * A ValidityChecker is comprised of a number of {@link ValidityCheck ValidityChecks} and
 * validates the state of something (general a UI dialog, or something of that sort).
 */
public class ValidityChecker {
//	private static final Logger LOG = LoggerFactory.getLogger( ValidityChecker.class );


	private enum State {
		NOT_YET_RUN,
		PASS,
		FAIL
	}


	private final boolean only_fire_on_state_change;
	private final boolean stop_checks_on_failure;

	private final ListenerSupport<CheckHandler,?> handlers =
		ListenerSupportFactory.create( CheckHandler.class, false );

	private final Set<CheckInfoHolder> checks_set =
		Collections.synchronizedSet( new HashSet<>() );


	private State previous_state = State.NOT_YET_RUN;



	/**
	 * Create a ValidityChecker that does not stop performing checks when a failure is
	 * encountered.
	 *
	 * @see #ValidityChecker(boolean, boolean, CheckHandler...)
	 */
	public ValidityChecker() {
		this( true, false );
	}

	/**
	 *
	 * @param only_fire_on_state_change     If true, the handlers will only be called
	 *                                      when the pass/fail state changes. If false,
	 *                                      their {@link CheckHandler#checkPass} or
	 *                                      {@link CheckHandler#checkFail} ()} methods
	 *                                      will be called every time a check is performed.
	 *
	 * @param stop_checks_on_failure        If true, once a failure is encountered no
	 *                                      further checks will be performed. If false,
	 *                                      all checks will be performed at each sweep.
	 *
	 * @param handlers                      Handlers to be notified of the overall state.
	 */
	public ValidityChecker( boolean only_fire_on_state_change,
		boolean stop_checks_on_failure, CheckHandler... handlers ) {

		this.only_fire_on_state_change = only_fire_on_state_change;
		this.stop_checks_on_failure = stop_checks_on_failure;

		for( CheckHandler handler : handlers ) {
			if ( handler == null ) continue;

			this.handlers.add( handler );
		}
	}


	public void addHandler( @NotNull CheckHandler handler ) {
		handlers.add( handler );
	}

	public void removeHandler( @NotNull CheckHandler handler ) {
		handlers.remove( handler );
	}



	/**
	 * Add a check that will be run when {@link #check()} is called.
	 *
	 * @param check				The check to be run.
	 */
	public void addCheck( @NotNull ValidityCheck check ) {
		addCheck( check, null );
	}

	/**
	 * Add a check that will be run when {@link #check()} is called.
	 *
	 * @param check				The check to be run.
	 * @param handler           Handler called when the state of the check changes or
	 *                          every time the check is run depending on whether or not
	 *                          <tt>only_fire_on_state_change</tt>.
	 */
	public void addCheck( @NotNull ValidityCheck check, @Nullable CheckHandler handler ) {
		checks_set.add( new CheckInfoHolder( check, handler ) );
	}

	/**
	 * Remove a check.
	 */
	public void removeCheck( @NotNull ValidityCheck check ) {
		//noinspection ConstantConditions
		if ( check == null ) return;

		Iterator<CheckInfoHolder> it = checks_set.iterator();
		while( it.hasNext() ) {
			CheckInfoHolder info = it.next();

			if ( info.check.equals( check ) ) {
				it.remove();
				return;
			}
		}
	}

	/**
	 * Clear all checks from the validator.
	 */
	public void clearChecks() {
		checks_set.clear();
	}



	/**
	 * Perform checks and determine the overall state.
	 *
	 * @return  True if all checks pass.
	 */
	public boolean check() {
		boolean all_valid = true;

		FailureMessageConsumer failure_message = new FailureMessageConsumer();

		FailureInfo failure_info = null;

		for ( CheckInfoHolder info : checks_set ) {
			ValidityCheck check = info.check;

			if ( check.check( failure_message ) ) {
//				if ( LOG.isTraceEnabled() ) {
//					printState( true, true, check );
//				}

				info.stateChanged( State.PASS, null );
			}
			else {
//				if ( LOG.isDebugEnabled() ) {
//					printState( false, false, check );
//				}

				info.stateChanged( State.FAIL, failure_message.message );

				all_valid = false;

				if ( failure_info == null ) {
					failure_info = new FailureInfo( info.check, info.failure_message );
				}
				else failure_info.add( info.check, info.failure_message );

				if ( stop_checks_on_failure ) break;
			}
		}

		if ( all_valid ) {
			// If desired, make sure the state changed
			if ( !only_fire_on_state_change || previous_state != State.PASS ) {
				handlers.dispatch().checkPass();
			}
		}
		else {
			// If desired, make sure the state changed
			if ( !only_fire_on_state_change || previous_state != State.FAIL ) {
				handlers.dispatch().checkFail( failure_info );
			}
		}

		previous_state = all_valid ? State.PASS : State.FAIL;

		return all_valid;
	}




	private class CheckInfoHolder {
		final @NotNull ValidityCheck check;

		final @Nullable CheckHandler handler;

		State state = State.NOT_YET_RUN;
		ResourceKey<String> failure_message = null;


		CheckInfoHolder( @NotNull ValidityCheck check, @Nullable CheckHandler handler ) {
			this.check = check;
			this.handler = handler;
		}



		/**
		 * Set the new state for the check.
		 *
		 * @return      True if the state has changed
		 */
		boolean stateChanged( @NotNull State new_state,
			@Nullable ResourceKey<String> new_failure_message ) {

			if ( new_state != state ||
				( new_state == State.FAIL &&
				!MiscKit.equal( new_failure_message, failure_message ) ) ) {

				this.state = new_state;
				this.failure_message = new_failure_message;

				dispatch( new_state == State.FAIL, failure_message );
				return true;
			}

			if ( !only_fire_on_state_change ) {
				dispatch( new_state == State.FAIL, failure_message );
			}

			return false;
		}


		private void dispatch( boolean fail, ResourceKey<String> failure_message ) {
			if ( handler == null ) return;

			if ( fail ) {
				handler.checkFail( new FailureInfo( check, failure_message ) );
			}
			else handler.checkPass();
		}
	}


	private static class FailureMessageConsumer implements Consumer<ResourceKey<String>> {
		ResourceKey<String> message;

		@Override
		public void accept( ResourceKey<String> message ) {
			this.message = message;
		}
	}
}
