package com.starlight.validity2.checks;

import com.starlight.NotNull;
import com.starlight.Nullable;
import com.starlight.locale.ResourceKey;
import com.starlight.validity2.ValidityCheck;

import java.util.function.Consumer;


/**
 *
 */
public abstract class AbstractValidityCheck implements ValidityCheck {
	private @Nullable ResourceKey<String> failure_message;



	public AbstractValidityCheck( @Nullable ResourceKey<String> failure_message ) {
		this.failure_message = failure_message;
	}



	@Override
	public boolean check( @NotNull Consumer<ResourceKey<String>> fail_message_consumer ) {
		if ( check() ) return true;
		else {
			fail_message_consumer.accept( failure_message );
			return false;
		}
	}



	public void setFailureMessage( @Nullable ResourceKey<String> failure_message ) {
		this.failure_message = failure_message;
	}



	/**
	 *
	 */
	protected abstract boolean check();
}
