package com.starlight.validity2;

import com.starlight.NotNull;
import com.starlight.Nullable;
import com.starlight.locale.ResourceKey;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 *
 */
public class FailureInfo {
	private final Map<ValidityCheck,ResourceKey<String>> failures = new HashMap<>();


	public FailureInfo( @NotNull ValidityCheck check,
		@Nullable ResourceKey<String> failure_message ) {

		add( check, failure_message );
	}


	public void add( @NotNull ValidityCheck check,
		@Nullable ResourceKey<String> failure_message ) {

		failures.put( check, failure_message );
	}



	public Map<ValidityCheck,ResourceKey<String>> failures() {
		return Collections.unmodifiableMap( failures );
	}



	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( o == null || getClass() != o.getClass() ) return false;

		FailureInfo that = ( FailureInfo ) o;

		if ( !failures.equals( that.failures ) ) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return failures.hashCode();
	}

	@Override
	public String   toString() {
		return "FailureInfo{" +
			"failures=" + failures +
			'}';
	}
}
