/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */

package com.starlight;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.Arrays;


/**
 * Utilities related to exceptions.
 */
public class ExceptionKit {
	/**
	 * Was <tt>throwable</tt> caused by an exception of the class
	 * <tt>causeClass</tt>?.  Equivalent to {@link #isCause(Throwable, Class,
	 * boolean)}<tt>(throwable, causeClass, true)</tt> -- that is, looking for
	 * an exact match for <tt>causeClass</tt>.
	 */
	public static boolean isCause( Throwable throwable, Class causeClass ) {
		return isCause( throwable, causeClass, true );
	}

	/**
	 * Was <tt>throwable</tt> caused by an exception of class
	 * <tt>cause_class</tt>?  That is, tracing back through <tt>throwable</tt>'s
	 * causes, including <tt>throwable</tt> itself, are any of them instances
	 * of <tt>cause_class</tt>?
	 *
	 * @param exact_only if true, only exact matches of <tt>cause_class</tt> will be
	 *                   detected; otherwise, subclasses will also cause a result
	 *                   of <tt>true</tt>.
	 */
	public static boolean isCause( Throwable throwable, Class cause_class,
		boolean exact_only ) {

		boolean result;
		if ( exact_only ) result = throwable.getClass().equals( cause_class );
		else result = cause_class.isInstance( throwable );

		// found a match?
		if ( result ) return true;
			// otherwise, recurse
		else {
			Throwable cause = throwable.getCause();
			// didn't find a match
			if ( cause == null ) return false;
			else return isCause( cause, cause_class, exact_only );
		}
	}

	/**
	 * This returns true if the given exception or any of it's causes are
	 * an instance of the class specified, or if any
	 */
	public static boolean isInstanceOrCauses( Class eclass, Throwable e ) {
		if ( e == null ) return false;
		else if ( eclass.isInstance( e ) ) return true;
		else return isInstanceOrCauses( eclass, e.getCause() );
	}

	/**
	 * Check whether a Throwable's stack trace contains a matching frame in the given
	 * range. For example, to check whether the function "util.NodeList.getNode" is
	 * anywhere the trace, <tt>containsFrame(t, "util.NoteList.getNode", 0, -1)</tt>.
	 *
	 * @param frame_string       A String to look for in a stack trace frame
	 * @param min_dist_from_root Where to start searching -- 0 means the frame where
	 *                           the Throwable was created and originally thrown from.
	 *                           1 would mean one step up from the root cause, etc.
	 * @param max_dist_from_root Which frame to stop searching at. -1 means search
	 *                           all frames.
	 * @return true if a frame matches, false otherwise.
	 */
	public static boolean containsFrame( Throwable t, String frame_string,
		int min_dist_from_root, int max_dist_from_root ) {

		StackTraceElement[] frames = t.getStackTrace();
		if ( max_dist_from_root < 0 || max_dist_from_root > frames.length ) {
			max_dist_from_root = frames.length - 1;
		}
		if ( min_dist_from_root < 0 || min_dist_from_root > max_dist_from_root ) {
			throw new IllegalArgumentException(
				"min_dist_from_root out of bounds (" + min_dist_from_root + ")" );
		}

		for ( int i = min_dist_from_root; i <= max_dist_from_root; i++ ) {
			// have we found a match?
			if ( frames[ i ].toString().indexOf( frame_string ) >= 0 ) return true;
		}

		// no match
		return false;
	}


	/**
	 * Helper method to get the StackTrace of a throwable.
	 */
	public static String getStackTrace( Throwable t ) {
		ValidationKit.checkNonnull( t, "t" );

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter( sw );
		t.printStackTrace( pw );
		pw.close();

		return sw.toString();
	}


	/**
	 * Returns true if the given method can throw the given Throwable without wrapping it
	 * in an UndeclaredThowableException.
	 */
	public static boolean canThrow( Method method, Class<? extends Throwable> t ) {
		if ( RuntimeException.class.isAssignableFrom( t ) ) return true;
		if ( Error.class.isAssignableFrom( t ) ) return true;
		
		Class[] exception_types = method.getExceptionTypes();
		for( Class exception : exception_types ) {
			if ( exception.isAssignableFrom( t ) ) return true;
		}

		return false;
	}


	/**
	 * Returns the message of an exception, unless it has non, in which case it returns
	 * the exception class name.
	 */
	public static String simpleToString( Throwable t ) {
		if ( t == null ) return null;

		// Special case: show full toString for index problems
		if ( t instanceof IndexOutOfBoundsException ) return t.toString();

		String message = t.getLocalizedMessage();
		if ( StringKit.isEmpty( message, false ) ) {
			return t.getClass().getName();
		}
		else return message;
	}


	/**
	 * Determines whether or not two Throwable objects are equal.
	 */
	public static boolean equals( Throwable t1, Throwable t2 ) {
		if ( t1 == t2 ) return true;

		if ( t1 == null ) return false;
		if ( t2 == null ) return false;

		if ( !t1.getClass().equals( t2.getClass() ) ) return false;
		if ( !MiscKit.equal( t1.getMessage(), t2.getMessage() ) ) return false;

		StackTraceElement[] s1 = t1.getStackTrace();
		StackTraceElement[] s2 = t2.getStackTrace();

		return Arrays.equals( s1, s2 );
	}
}
