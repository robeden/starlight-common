/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */
package com.starlight;

import org.slf4j.helpers.MessageFormatter;

import java.util.Arrays;
import java.util.Collection;


/**
 * Static string functions.
 *
 * @author reden
 */
public class StringKit {
	private static final String[] INSIGNIFICANT_WORDS = { "of", "a", "an" };

	static {
		Arrays.sort( INSIGNIFICANT_WORDS );
	}


	/**
	 * Return the given string in title case, for example: This Is Title Case.
	 */
	public static String toTitleCase( String string, boolean ignore_insignificant ) {
		if ( string == null || string.equals( "" ) ) return string;

		StringBuffer buf = new StringBuffer();
		String[] toks = string.split( " " );
		for ( int i = 0; i < toks.length; i++ ) {
			if ( i != 0 ) buf.append( " " );

			String token = toks[ i ];
			if ( token.length() == 0 ) continue;

			if ( ignore_insignificant ) {
				if ( Arrays.binarySearch( INSIGNIFICANT_WORDS, token ) >= 0 ) {
					buf.append( token );
					continue;
				}
			}

			// first see if it's all upper-case. If so, leave it alone. This catches acronyms
			if ( token.equals( token.toUpperCase() ) ) {
				buf.append( token );
				continue;
			}

			char first_char = token.charAt( 0 );
			if ( Character.isLetter( first_char ) &&
				!Character.isUpperCase( first_char ) ) {
				buf.append( Character.toTitleCase( first_char ) );
			}
			else {
				buf.append( first_char );
			}

			// Make sure the second character is lower case if the third is not
			if ( token.length() >= 3 ) {
				if ( Character.isUpperCase( token.charAt( 1 ) ) &&
					Character.isLowerCase( token.charAt( 2 ) ) ) {

					buf.append( Character.toLowerCase( token.charAt( 1 ) ) );
				}
				else {
					buf.append( token.charAt( 1 ) );
				}

				buf.append( token.substring( 2 ) );
			}
			else {
				buf.append( token.substring( 1 ) );
			}
		}

		return buf.toString();
	}


	/**
	 * Tells whether or not a string is null or empty.
	 *
	 * @param str The string to check.
	 */
	public static boolean isEmpty( String str ) {
		return isEmpty( str, true );
	}

	/**
	 * Tells whether or not a string is null or empty.
	 *
	 * @param str           The string to check.
	 * @param consider_trim If true, the string will also be trimmer to see if
	 *                      it only contains white space.
	 */
	public static boolean isEmpty( String str, boolean consider_trim ) {
		if ( str == null ) return true;
		else if ( str.length() == 0 ) return true;
		else if ( consider_trim && str.trim().length() == 0 ) return true;
		else return false;
	}


	/**
	 * Ensure the returned string is a particular size. If it's bigger it will be clipped.
	 * If it's too small it will be padded.
	 */
	public static String ensureSize( String string, int length, char pad_char ) {
		if ( string == null ) string = "";

		if ( string.length() == length ) return string;
		else if ( string.length() > length ) return clip( string, length );
		else {
			StringBuffer buf = new StringBuffer( string );
			while ( buf.length() < length ) {
				buf.append( pad_char );
			}

			return buf.toString();
		}
	}

	/**
	 * Ensure the returned string is a particular size. If it's bigger it will be clipped.
	 * If it's too small it will be padded with a space.
	 */
	public static String ensureSize( String string, int length ) {
		return ensureSize( string, length, ' ' );
	}


	/**
	 * Make sure a String isn't any longer than the given size. If it is, it's clipped off
	 * to that size.
	 */
	public static String clip( String string, final int size ) {
		if ( string == null ) return string;

		if ( size < 1 ) throw new IllegalArgumentException( "Size must be positive." );

		if ( string.length() > size ) {
			string = string.substring( 0, size );
		}

		return string;
	}


	/**
	 * Returns null if the object if null, otherwise the toString() value of the object.
	 */
	public static String toString( final Object value ) {
		return toString( value, null );
	}

	/**
	 * Returns the given value if the object if null, otherwise the toString() value of
	 * the object.
	 */
	public static String toString( final Object value, final String null_value ) {
		if ( value == null ) return null_value;
		else return value.toString();
	}


	/**
	 * Returns a name in sortable format.
	 */
	public static String buildSortableName( String first, Character middle,
		String last ) {
		return buildSortableName( first, middle == null ? null : middle.toString(),
			last );
	}

	/**
	 * Returns a name in sortable format.
	 */
	public static String buildSortableName( String first, String middle, String last ) {
		StringBuilder buf = new StringBuilder( last );
		buf.append( ", " );
		buf.append( first );
		if ( middle != null ) {
			buf.append( " " );
			buf.append( middle );

			if ( middle.length() == 1 ) buf.append( "." );
		}

		return buf.toString();
	}

	/**
	 * Return a pretty string of the list items. For example, passing in "a", "b", "c"
	 * would return "a, b and c".
	 */
	public static <T> String buildPrettyList( Collection<T> collection ) {
		if ( collection == null ) return "";
		return buildPrettyList( collection.toArray() );
	}

	/**
	 * Return a pretty string of the list items. For example, passing in "a", "b", "c"
	 * would return "a, b and c".
	 */
	public static <T> String buildPrettyList( T... args ) {
		if ( args == null || args.length == 0 ) return "";
		if ( args.length == 1 ) return String.valueOf( args[ 0 ] );

		// TODO: not sure how to properly localize this
		StringBuilder buf = new StringBuilder();
		for( int i = 0; i < args.length; i++ ) {
			if ( i != 0 ) {
				if ( i == args.length - 1 ) buf.append( " and " );
				else buf.append( ", " );
			}

			buf.append( String.valueOf( args[ i ] ) );
		}

		return buf.toString();
	}


	/**
	 * Split a string by a given character. This is equivalent to String.split, but is
	 * significantly faster.
	 */
	public static String[] split( String string, char c ) {
		// Count the number of characters
		int length = string.length();
		int count = 0;
		for( int i = 0; i < length; i++ ) {
			char string_c = string.charAt( i );
			if ( c == string_c ) {
				if ( i < length - 1 ) {
					count++;
				}
			}
		}

		// Shortcut for no matches
		if ( count == 0 ) return new String[] { string };

		int next_token_index = 0;
		String[] to_return = new String[ count + 1 ];

		// Now pull out the tokens
		int c_index = string.indexOf( c );
		if ( c_index >= 0 ) {
			int start = 0;
			while( c_index >= 0 ) {
				to_return[ next_token_index ] = string.substring( start, c_index );
				next_token_index++;

				start = c_index + 1;
				c_index = string.indexOf( c, start );
			}
			if ( start != 0 && start < string.length() ) {
				to_return[ next_token_index ] = string.substring( start );
				next_token_index++;
			}
		}

		return to_return;
	}


	/**
	 * This will trim a string and return null if the result is empty.
	 */
	public static @Nullable String trimToNull( @Nullable String string ) {
		if ( string == null ) return null;

		string = string.trim();
		if ( string.isEmpty() ) return null;
		return string;
	}

	/**
	 * Test to see if two string are equal, ignoring case and handling nulls gracefully.
	 */
	public static boolean equalIgnoreCase( final String s1, final String s2 ) {
		if ( s1 == null ) return s2 == null;
		else return s1.equalsIgnoreCase( s2 );
	}


	/**
	 * Returns a String formatted using SLF4J syntax. That is, where the string
	 * "This {} a {}" with the arguments "is" and "test" would return "This is a test".
	 */
	public static String format( String message_pattern, Object... arguments ) {
		return MessageFormatter.arrayFormat( message_pattern, arguments ).getMessage();
	}
}
