package com.starlight.thread;

/**
 * A simple class that runs multiple Runnables when this Runnable is run.
 */
public class CompoundRunnable implements Runnable {
	private final Runnable[] parts;

	public CompoundRunnable( Runnable... parts ) {
		this.parts = parts;
	}

	
	@Override
	public void run() {
		for( Runnable part : parts ) {
			part.run();
		}
	}
}
