/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */
package com.starlight.thread;

/**
 *
 */
public interface Haltable {
	public void halt();
}
