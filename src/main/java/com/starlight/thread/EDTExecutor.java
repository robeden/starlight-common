/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */
package com.starlight.thread;

import javax.swing.*;
import java.util.concurrent.Executor;


/**
 * An Executor instance to always run a command in the EDT.
 */
public class EDTExecutor implements Executor {
	public static final Executor INSTANCE = new EDTExecutor();


	private EDTExecutor() {
	}


	public void execute( Runnable command ) {
		if ( SwingUtilities.isEventDispatchThread() ) {
			command.run();
		}
		else SwingUtilities.invokeLater( command );
	}
}
