/*
 * Copyright(c) 2002-2010, Rob Eden
 * All rights reserved.
 */

package com.starlight.thread;

import java.util.concurrent.Executor;


/**
 * A very, very simple Executor implementation that runs the task in the current thread.
 * Normally this would be very silly, but it allows us to work with the Executor interface
 * as an abstraction layer.
 */
public class SynchronousExecutor implements Executor {
	/**
	 * Shared instance that is safe for concurrent usage.
	 */
	public static final Executor INSTANCE = new SynchronousExecutor();


	public void execute( Runnable command ) {
		command.run();
	}
}
