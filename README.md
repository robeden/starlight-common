StarLight-Common
================

A set of Java utility classes


Deprecated
----------

This project has been broken into smaller pieces. Please switch to the following:

* [common-core](https://bitbucket.org/logicartisan/common-core)
* [common-locale](https://bitbucket.org/logicartisan/common-locale)
* [common-validation](https://bitbucket.org/logicartisan/common-validation)